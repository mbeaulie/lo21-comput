/****************************************************************************
** Meta object code from reading C++ file 'dialogprgm.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "../../VF_RPNCalculator/dialogprgm.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'dialogprgm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_DialogPrgm_t {
    QByteArrayData data[14];
    char stringdata0[316];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_DialogPrgm_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_DialogPrgm_t qt_meta_stringdata_DialogPrgm = {
    {
QT_MOC_LITERAL(0, 0, 10), // "DialogPrgm"
QT_MOC_LITERAL(1, 11, 13), // "prgmOkPressed"
QT_MOC_LITERAL(2, 25, 0), // ""
QT_MOC_LITERAL(3, 26, 21), // "on_buttonBox_accepted"
QT_MOC_LITERAL(4, 48, 29), // "on_prgmList_itemDoubleClicked"
QT_MOC_LITERAL(5, 78, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(6, 95, 4), // "item"
QT_MOC_LITERAL(7, 100, 32), // "on_pushButtonEnregistrer_clicked"
QT_MOC_LITERAL(8, 133, 28), // "on_pushButtonAnnuler_clicked"
QT_MOC_LITERAL(9, 162, 28), // "on_pushButtonNouveau_clicked"
QT_MOC_LITERAL(10, 191, 30), // "on_pushButtonSupprimer_clicked"
QT_MOC_LITERAL(11, 222, 32), // "on_prgmList_itemSelectionChanged"
QT_MOC_LITERAL(12, 255, 29), // "on_pushButtonModifier_clicked"
QT_MOC_LITERAL(13, 285, 30) // "on_pushButtonDupliquer_clicked"

    },
    "DialogPrgm\0prgmOkPressed\0\0"
    "on_buttonBox_accepted\0"
    "on_prgmList_itemDoubleClicked\0"
    "QListWidgetItem*\0item\0"
    "on_pushButtonEnregistrer_clicked\0"
    "on_pushButtonAnnuler_clicked\0"
    "on_pushButtonNouveau_clicked\0"
    "on_pushButtonSupprimer_clicked\0"
    "on_prgmList_itemSelectionChanged\0"
    "on_pushButtonModifier_clicked\0"
    "on_pushButtonDupliquer_clicked"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_DialogPrgm[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      10,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   64,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       3,    0,   65,    2, 0x08 /* Private */,
       4,    1,   66,    2, 0x08 /* Private */,
       7,    0,   69,    2, 0x08 /* Private */,
       8,    0,   70,    2, 0x08 /* Private */,
       9,    0,   71,    2, 0x08 /* Private */,
      10,    0,   72,    2, 0x08 /* Private */,
      11,    0,   73,    2, 0x08 /* Private */,
      12,    0,   74,    2, 0x08 /* Private */,
      13,    0,   75,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 5,    6,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void DialogPrgm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<DialogPrgm *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->prgmOkPressed(); break;
        case 1: _t->on_buttonBox_accepted(); break;
        case 2: _t->on_prgmList_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        case 3: _t->on_pushButtonEnregistrer_clicked(); break;
        case 4: _t->on_pushButtonAnnuler_clicked(); break;
        case 5: _t->on_pushButtonNouveau_clicked(); break;
        case 6: _t->on_pushButtonSupprimer_clicked(); break;
        case 7: _t->on_prgmList_itemSelectionChanged(); break;
        case 8: _t->on_pushButtonModifier_clicked(); break;
        case 9: _t->on_pushButtonDupliquer_clicked(); break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            using _t = void (DialogPrgm::*)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&DialogPrgm::prgmOkPressed)) {
                *result = 0;
                return;
            }
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject DialogPrgm::staticMetaObject = { {
    QMetaObject::SuperData::link<QDialog::staticMetaObject>(),
    qt_meta_stringdata_DialogPrgm.data,
    qt_meta_data_DialogPrgm,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *DialogPrgm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *DialogPrgm::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_DialogPrgm.stringdata0))
        return static_cast<void*>(this);
    return QDialog::qt_metacast(_clname);
}

int DialogPrgm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 10)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 10;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 10)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 10;
    }
    return _id;
}

// SIGNAL 0
void DialogPrgm::prgmOkPressed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
