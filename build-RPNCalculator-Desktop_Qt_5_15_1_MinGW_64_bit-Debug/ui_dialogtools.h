/********************************************************************************
** Form generated from reading UI file 'dialogtools.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGTOOLS_H
#define UI_DIALOGTOOLS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogTools
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QSpinBox *SpinNbAffiche;

    void setupUi(QDialog *DialogTools)
    {
        if (DialogTools->objectName().isEmpty())
            DialogTools->setObjectName(QString::fromUtf8("DialogTools"));
        DialogTools->resize(453, 359);
        buttonBox = new QDialogButtonBox(DialogTools);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(90, 320, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(DialogTools);
        formLayoutWidget->setObjectName(QString::fromUtf8("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(9, 29, 421, 281));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QString::fromUtf8("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        SpinNbAffiche = new QSpinBox(formLayoutWidget);
        SpinNbAffiche->setObjectName(QString::fromUtf8("SpinNbAffiche"));

        formLayout->setWidget(0, QFormLayout::FieldRole, SpinNbAffiche);


        retranslateUi(DialogTools);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogTools, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogTools, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogTools);
    } // setupUi

    void retranslateUi(QDialog *DialogTools)
    {
        DialogTools->setWindowTitle(QCoreApplication::translate("DialogTools", "Dialog", nullptr));
        label->setText(QCoreApplication::translate("DialogTools", "Nombre d'elements a afficher dans la pile", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogTools: public Ui_DialogTools {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGTOOLS_H
