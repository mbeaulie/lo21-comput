/********************************************************************************
** Form generated from reading UI file 'dialogvar.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGVAR_H
#define UI_DIALOGVAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_DialogVar
{
public:
    QDialogButtonBox *buttonBox;
    QListWidget *listWidget;
    QTextEdit *textEdit;

    void setupUi(QDialog *DialogVar)
    {
        if (DialogVar->objectName().isEmpty())
            DialogVar->setObjectName(QString::fromUtf8("DialogVar"));
        DialogVar->resize(408, 540);
        buttonBox = new QDialogButtonBox(DialogVar);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(50, 490, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        listWidget = new QListWidget(DialogVar);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(20, 20, 361, 151));
        textEdit = new QTextEdit(DialogVar);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(20, 200, 361, 271));

        retranslateUi(DialogVar);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogVar, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogVar, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogVar);
    } // setupUi

    void retranslateUi(QDialog *DialogVar)
    {
        DialogVar->setWindowTitle(QCoreApplication::translate("DialogVar", "Dialog", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogVar: public Ui_DialogVar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGVAR_H
