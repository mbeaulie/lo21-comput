/********************************************************************************
** Form generated from reading UI file 'dialogprgm.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGPRGM_H
#define UI_DIALOGPRGM_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogPrgm
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QTextEdit *prgmText;
    QSpacerItem *horizontalSpacer;
    QLabel *lbl_list;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButtonNouveau;
    QPushButton *pushButtonSupprimer;
    QPushButton *pushButtonModifier;
    QPushButton *pushButtonDupliquer;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButtonEnregistrer;
    QSpacerItem *verticalSpacer;
    QPushButton *pushButtonAnnuler;
    QLabel *lbl_name;
    QLabel *lbl_content;
    QLineEdit *prgmNom;
    QListWidget *prgmList;
    QVBoxLayout *verticalLayout_2;
    QLineEdit *varText;

    void setupUi(QDialog *DialogPrgm)
    {
        if (DialogPrgm->objectName().isEmpty())
            DialogPrgm->setObjectName(QString::fromUtf8("DialogPrgm"));
        DialogPrgm->resize(590, 574);
        buttonBox = new QDialogButtonBox(DialogPrgm);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setGeometry(QRect(230, 530, 341, 32));
        buttonBox->setMinimumSize(QSize(341, 0));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        gridLayoutWidget = new QWidget(DialogPrgm);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(20, 20, 551, 481));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        prgmText = new QTextEdit(gridLayoutWidget);
        prgmText->setObjectName(QString::fromUtf8("prgmText"));
        prgmText->setInputMethodHints(Qt::ImhNone);
        prgmText->setAcceptRichText(false);

        gridLayout->addWidget(prgmText, 7, 0, 1, 1);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

        lbl_list = new QLabel(gridLayoutWidget);
        lbl_list->setObjectName(QString::fromUtf8("lbl_list"));

        gridLayout->addWidget(lbl_list, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        pushButtonNouveau = new QPushButton(gridLayoutWidget);
        pushButtonNouveau->setObjectName(QString::fromUtf8("pushButtonNouveau"));

        verticalLayout->addWidget(pushButtonNouveau);

        pushButtonSupprimer = new QPushButton(gridLayoutWidget);
        pushButtonSupprimer->setObjectName(QString::fromUtf8("pushButtonSupprimer"));

        verticalLayout->addWidget(pushButtonSupprimer);

        pushButtonModifier = new QPushButton(gridLayoutWidget);
        pushButtonModifier->setObjectName(QString::fromUtf8("pushButtonModifier"));

        verticalLayout->addWidget(pushButtonModifier);

        pushButtonDupliquer = new QPushButton(gridLayoutWidget);
        pushButtonDupliquer->setObjectName(QString::fromUtf8("pushButtonDupliquer"));

        verticalLayout->addWidget(pushButtonDupliquer);


        gridLayout->addLayout(verticalLayout, 1, 1, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        pushButtonEnregistrer = new QPushButton(gridLayoutWidget);
        pushButtonEnregistrer->setObjectName(QString::fromUtf8("pushButtonEnregistrer"));

        horizontalLayout->addWidget(pushButtonEnregistrer);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        horizontalLayout->addItem(verticalSpacer);

        pushButtonAnnuler = new QPushButton(gridLayoutWidget);
        pushButtonAnnuler->setObjectName(QString::fromUtf8("pushButtonAnnuler"));

        horizontalLayout->addWidget(pushButtonAnnuler);


        gridLayout->addLayout(horizontalLayout, 11, 0, 1, 1);

        lbl_name = new QLabel(gridLayoutWidget);
        lbl_name->setObjectName(QString::fromUtf8("lbl_name"));

        gridLayout->addWidget(lbl_name, 3, 0, 1, 1);

        lbl_content = new QLabel(gridLayoutWidget);
        lbl_content->setObjectName(QString::fromUtf8("lbl_content"));

        gridLayout->addWidget(lbl_content, 5, 0, 1, 1);

        prgmNom = new QLineEdit(gridLayoutWidget);
        prgmNom->setObjectName(QString::fromUtf8("prgmNom"));
        prgmNom->setInputMethodHints(Qt::ImhUppercaseOnly);
        prgmNom->setMaxLength(20);

        gridLayout->addWidget(prgmNom, 4, 0, 1, 1);

        prgmList = new QListWidget(gridLayoutWidget);
        prgmList->setObjectName(QString::fromUtf8("prgmList"));

        gridLayout->addWidget(prgmList, 1, 0, 1, 1);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));

        gridLayout->addLayout(verticalLayout_2, 7, 1, 1, 1);

        varText = new QLineEdit(gridLayoutWidget);
        varText->setObjectName(QString::fromUtf8("varText"));

        gridLayout->addWidget(varText, 6, 0, 1, 1);


        retranslateUi(DialogPrgm);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogPrgm, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogPrgm, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogPrgm);
    } // setupUi

    void retranslateUi(QDialog *DialogPrgm)
    {
        DialogPrgm->setWindowTitle(QCoreApplication::translate("DialogPrgm", "Dialog", nullptr));
        lbl_list->setText(QCoreApplication::translate("DialogPrgm", "Liste des programmes", nullptr));
        pushButtonNouveau->setText(QCoreApplication::translate("DialogPrgm", "Nouveau", nullptr));
        pushButtonSupprimer->setText(QCoreApplication::translate("DialogPrgm", "Supprimer", nullptr));
        pushButtonModifier->setText(QCoreApplication::translate("DialogPrgm", "Modifier", nullptr));
        pushButtonDupliquer->setText(QCoreApplication::translate("DialogPrgm", "Dupliquer", nullptr));
        pushButtonEnregistrer->setText(QCoreApplication::translate("DialogPrgm", "&Enregistrer", nullptr));
        pushButtonAnnuler->setText(QCoreApplication::translate("DialogPrgm", "&Annuler", nullptr));
        lbl_name->setText(QCoreApplication::translate("DialogPrgm", "Nom du programme", nullptr));
        lbl_content->setText(QCoreApplication::translate("DialogPrgm", "Contenu", nullptr));
    } // retranslateUi

};

namespace Ui {
    class DialogPrgm: public Ui_DialogPrgm {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGPRGM_H
