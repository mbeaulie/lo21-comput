/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionQuitter;
    QAction *actionEditeur;
    QAction *actionEditeur_2;
    QAction *actionParam_tres;
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *touche_inferieur;
    QListWidget *listPrgm;
    QLineEdit *affichage;
    QPushButton *touche_sup;
    QPushButton *cacher_var;
    QPushButton *touche_prgm;
    QPushButton *touche_mod;
    QPushButton *touche_entree;
    QPushButton *touche_6;
    QPushButton *touche_back;
    QPushButton *touche_2;
    QPushButton *touche_clear;
    QPushButton *touche_fois;
    QPushButton *cacher_prgm;
    QPushButton *touche_divent;
    QPushButton *touche_moins;
    QPushButton *touche_point;
    QPushButton *touche_egal;
    QPushButton *touche_9;
    QPushButton *touche_div;
    QPushButton *touche_plus;
    QPushButton *touche_3;
    QPushButton *touche_var;
    QPushButton *touche_4;
    QPushButton *touche_neg;
    QPushButton *touche_0;
    QPushButton *touche_inferegal;
    QPushButton *touche_8;
    QLabel *label;
    QPushButton *cacher_clavier;
    QPushButton *touche_eval;
    QListWidget *listVariables;
    QPushButton *touche_supegal;
    QPushButton *touche_7;
    QPushButton *touche_5;
    QPushButton *touche_1;
    QPushButton *touche_dup;
    QPushButton *touche_drop;
    QPushButton *touche_swap;
    QPushButton *touche_ift;
    QPushButton *touche_clear_pile;
    QTextEdit *EtatPile;
    QLineEdit *msg_util;
    QPushButton *touche_non;
    QPushButton *touche_or;
    QPushButton *touche_and;
    QPushButton *touche_different;
    QMenuBar *menubar;
    QMenu *menu_Fichier;
    QMenu *menu_Programmes;
    QMenu *menu_Variables;
    QMenu *menu_Outils;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1346, 600);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(11);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        MainWindow->setFont(font);
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QString::fromUtf8("actionQuitter"));
        actionEditeur = new QAction(MainWindow);
        actionEditeur->setObjectName(QString::fromUtf8("actionEditeur"));
        actionEditeur_2 = new QAction(MainWindow);
        actionEditeur_2->setObjectName(QString::fromUtf8("actionEditeur_2"));
        actionParam_tres = new QAction(MainWindow);
        actionParam_tres->setObjectName(QString::fromUtf8("actionParam_tres"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QString::fromUtf8("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 20, 1311, 481));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setHorizontalSpacing(4);
        gridLayout->setVerticalSpacing(6);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        touche_inferieur = new QPushButton(gridLayoutWidget);
        touche_inferieur->setObjectName(QString::fromUtf8("touche_inferieur"));

        gridLayout->addWidget(touche_inferieur, 6, 2, 1, 1);

        listPrgm = new QListWidget(gridLayoutWidget);
        listPrgm->setObjectName(QString::fromUtf8("listPrgm"));

        gridLayout->addWidget(listPrgm, 1, 12, 6, 2);

        affichage = new QLineEdit(gridLayoutWidget);
        affichage->setObjectName(QString::fromUtf8("affichage"));
        affichage->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(affichage, 0, 1, 1, 4);

        touche_sup = new QPushButton(gridLayoutWidget);
        touche_sup->setObjectName(QString::fromUtf8("touche_sup"));

        gridLayout->addWidget(touche_sup, 6, 4, 1, 1);

        cacher_var = new QPushButton(gridLayoutWidget);
        cacher_var->setObjectName(QString::fromUtf8("cacher_var"));

        gridLayout->addWidget(cacher_var, 0, 15, 1, 1);

        touche_prgm = new QPushButton(gridLayoutWidget);
        touche_prgm->setObjectName(QString::fromUtf8("touche_prgm"));

        gridLayout->addWidget(touche_prgm, 0, 12, 1, 1);

        touche_mod = new QPushButton(gridLayoutWidget);
        touche_mod->setObjectName(QString::fromUtf8("touche_mod"));

        gridLayout->addWidget(touche_mod, 4, 5, 1, 1);

        touche_entree = new QPushButton(gridLayoutWidget);
        touche_entree->setObjectName(QString::fromUtf8("touche_entree"));

        gridLayout->addWidget(touche_entree, 5, 5, 1, 1);

        touche_6 = new QPushButton(gridLayoutWidget);
        touche_6->setObjectName(QString::fromUtf8("touche_6"));

        gridLayout->addWidget(touche_6, 3, 3, 1, 1);

        touche_back = new QPushButton(gridLayoutWidget);
        touche_back->setObjectName(QString::fromUtf8("touche_back"));

        gridLayout->addWidget(touche_back, 1, 1, 1, 2);

        touche_2 = new QPushButton(gridLayoutWidget);
        touche_2->setObjectName(QString::fromUtf8("touche_2"));

        gridLayout->addWidget(touche_2, 4, 2, 1, 1);

        touche_clear = new QPushButton(gridLayoutWidget);
        touche_clear->setObjectName(QString::fromUtf8("touche_clear"));

        gridLayout->addWidget(touche_clear, 1, 3, 1, 2);

        touche_fois = new QPushButton(gridLayoutWidget);
        touche_fois->setObjectName(QString::fromUtf8("touche_fois"));

        gridLayout->addWidget(touche_fois, 3, 4, 1, 1);

        cacher_prgm = new QPushButton(gridLayoutWidget);
        cacher_prgm->setObjectName(QString::fromUtf8("cacher_prgm"));

        gridLayout->addWidget(cacher_prgm, 0, 13, 1, 1);

        touche_divent = new QPushButton(gridLayoutWidget);
        touche_divent->setObjectName(QString::fromUtf8("touche_divent"));

        gridLayout->addWidget(touche_divent, 3, 5, 1, 1);

        touche_moins = new QPushButton(gridLayoutWidget);
        touche_moins->setObjectName(QString::fromUtf8("touche_moins"));

        gridLayout->addWidget(touche_moins, 4, 4, 1, 1);

        touche_point = new QPushButton(gridLayoutWidget);
        touche_point->setObjectName(QString::fromUtf8("touche_point"));

        gridLayout->addWidget(touche_point, 5, 3, 1, 1);

        touche_egal = new QPushButton(gridLayoutWidget);
        touche_egal->setObjectName(QString::fromUtf8("touche_egal"));

        gridLayout->addWidget(touche_egal, 6, 5, 1, 1);

        touche_9 = new QPushButton(gridLayoutWidget);
        touche_9->setObjectName(QString::fromUtf8("touche_9"));

        gridLayout->addWidget(touche_9, 2, 3, 1, 1);

        touche_div = new QPushButton(gridLayoutWidget);
        touche_div->setObjectName(QString::fromUtf8("touche_div"));

        gridLayout->addWidget(touche_div, 2, 4, 1, 1);

        touche_plus = new QPushButton(gridLayoutWidget);
        touche_plus->setObjectName(QString::fromUtf8("touche_plus"));

        gridLayout->addWidget(touche_plus, 5, 4, 1, 1);

        touche_3 = new QPushButton(gridLayoutWidget);
        touche_3->setObjectName(QString::fromUtf8("touche_3"));

        gridLayout->addWidget(touche_3, 4, 3, 1, 1);

        touche_var = new QPushButton(gridLayoutWidget);
        touche_var->setObjectName(QString::fromUtf8("touche_var"));

        gridLayout->addWidget(touche_var, 0, 14, 1, 1);

        touche_4 = new QPushButton(gridLayoutWidget);
        touche_4->setObjectName(QString::fromUtf8("touche_4"));

        gridLayout->addWidget(touche_4, 3, 1, 1, 1);

        touche_neg = new QPushButton(gridLayoutWidget);
        touche_neg->setObjectName(QString::fromUtf8("touche_neg"));

        gridLayout->addWidget(touche_neg, 5, 1, 1, 1);

        touche_0 = new QPushButton(gridLayoutWidget);
        touche_0->setObjectName(QString::fromUtf8("touche_0"));

        gridLayout->addWidget(touche_0, 5, 2, 1, 1);

        touche_inferegal = new QPushButton(gridLayoutWidget);
        touche_inferegal->setObjectName(QString::fromUtf8("touche_inferegal"));

        gridLayout->addWidget(touche_inferegal, 6, 1, 1, 1);

        touche_8 = new QPushButton(gridLayoutWidget);
        touche_8->setObjectName(QString::fromUtf8("touche_8"));

        gridLayout->addWidget(touche_8, 2, 2, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 6, 1, 5);

        cacher_clavier = new QPushButton(gridLayoutWidget);
        cacher_clavier->setObjectName(QString::fromUtf8("cacher_clavier"));

        gridLayout->addWidget(cacher_clavier, 0, 5, 1, 1);

        touche_eval = new QPushButton(gridLayoutWidget);
        touche_eval->setObjectName(QString::fromUtf8("touche_eval"));

        gridLayout->addWidget(touche_eval, 2, 5, 1, 1);

        listVariables = new QListWidget(gridLayoutWidget);
        listVariables->setObjectName(QString::fromUtf8("listVariables"));

        gridLayout->addWidget(listVariables, 1, 14, 6, 2);

        touche_supegal = new QPushButton(gridLayoutWidget);
        touche_supegal->setObjectName(QString::fromUtf8("touche_supegal"));

        gridLayout->addWidget(touche_supegal, 6, 3, 1, 1);

        touche_7 = new QPushButton(gridLayoutWidget);
        touche_7->setObjectName(QString::fromUtf8("touche_7"));

        gridLayout->addWidget(touche_7, 2, 1, 1, 1);

        touche_5 = new QPushButton(gridLayoutWidget);
        touche_5->setObjectName(QString::fromUtf8("touche_5"));

        gridLayout->addWidget(touche_5, 3, 2, 1, 1);

        touche_1 = new QPushButton(gridLayoutWidget);
        touche_1->setObjectName(QString::fromUtf8("touche_1"));

        gridLayout->addWidget(touche_1, 4, 1, 1, 1);

        touche_dup = new QPushButton(gridLayoutWidget);
        touche_dup->setObjectName(QString::fromUtf8("touche_dup"));

        gridLayout->addWidget(touche_dup, 7, 6, 1, 1);

        touche_drop = new QPushButton(gridLayoutWidget);
        touche_drop->setObjectName(QString::fromUtf8("touche_drop"));

        gridLayout->addWidget(touche_drop, 7, 7, 1, 1);

        touche_swap = new QPushButton(gridLayoutWidget);
        touche_swap->setObjectName(QString::fromUtf8("touche_swap"));

        gridLayout->addWidget(touche_swap, 7, 8, 1, 1);

        touche_ift = new QPushButton(gridLayoutWidget);
        touche_ift->setObjectName(QString::fromUtf8("touche_ift"));

        gridLayout->addWidget(touche_ift, 7, 9, 1, 1);

        touche_clear_pile = new QPushButton(gridLayoutWidget);
        touche_clear_pile->setObjectName(QString::fromUtf8("touche_clear_pile"));

        gridLayout->addWidget(touche_clear_pile, 7, 11, 1, 1);

        EtatPile = new QTextEdit(gridLayoutWidget);
        EtatPile->setObjectName(QString::fromUtf8("EtatPile"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(EtatPile->sizePolicy().hasHeightForWidth());
        EtatPile->setSizePolicy(sizePolicy1);
        EtatPile->setReadOnly(true);

        gridLayout->addWidget(EtatPile, 1, 6, 6, 6);

        msg_util = new QLineEdit(gridLayoutWidget);
        msg_util->setObjectName(QString::fromUtf8("msg_util"));
        msg_util->setEnabled(true);
        msg_util->setReadOnly(true);

        gridLayout->addWidget(msg_util, 8, 1, 1, 5);

        touche_non = new QPushButton(gridLayoutWidget);
        touche_non->setObjectName(QString::fromUtf8("touche_non"));

        gridLayout->addWidget(touche_non, 7, 4, 1, 1);

        touche_or = new QPushButton(gridLayoutWidget);
        touche_or->setObjectName(QString::fromUtf8("touche_or"));

        gridLayout->addWidget(touche_or, 7, 3, 1, 1);

        touche_and = new QPushButton(gridLayoutWidget);
        touche_and->setObjectName(QString::fromUtf8("touche_and"));

        gridLayout->addWidget(touche_and, 7, 2, 1, 1);

        touche_different = new QPushButton(gridLayoutWidget);
        touche_different->setObjectName(QString::fromUtf8("touche_different"));

        gridLayout->addWidget(touche_different, 7, 1, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1346, 27));
        menu_Fichier = new QMenu(menubar);
        menu_Fichier->setObjectName(QString::fromUtf8("menu_Fichier"));
        menu_Programmes = new QMenu(menubar);
        menu_Programmes->setObjectName(QString::fromUtf8("menu_Programmes"));
        menu_Variables = new QMenu(menubar);
        menu_Variables->setObjectName(QString::fromUtf8("menu_Variables"));
        menu_Outils = new QMenu(menubar);
        menu_Outils->setObjectName(QString::fromUtf8("menu_Outils"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_Fichier->menuAction());
        menubar->addAction(menu_Programmes->menuAction());
        menubar->addAction(menu_Variables->menuAction());
        menubar->addAction(menu_Outils->menuAction());
        menu_Fichier->addAction(actionQuitter);
        menu_Programmes->addAction(actionEditeur);
        menu_Variables->addAction(actionEditeur_2);
        menu_Outils->addAction(actionParam_tres);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionQuitter->setText(QCoreApplication::translate("MainWindow", "Quitter", nullptr));
        actionEditeur->setText(QCoreApplication::translate("MainWindow", "Editeur", nullptr));
        actionEditeur_2->setText(QCoreApplication::translate("MainWindow", "Editeur", nullptr));
        actionParam_tres->setText(QCoreApplication::translate("MainWindow", "Parametres", nullptr));
        touche_inferieur->setText(QCoreApplication::translate("MainWindow", "<", nullptr));
        touche_sup->setText(QCoreApplication::translate("MainWindow", ">", nullptr));
        cacher_var->setText(QCoreApplication::translate("MainWindow", "Cacher", nullptr));
        touche_prgm->setText(QCoreApplication::translate("MainWindow", "Programmes", nullptr));
        touche_mod->setText(QCoreApplication::translate("MainWindow", "MOD", nullptr));
        touche_entree->setText(QCoreApplication::translate("MainWindow", "ENTREE", nullptr));
        touche_6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        touche_back->setText(QCoreApplication::translate("MainWindow", "Backspace", nullptr));
        touche_2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        touche_clear->setText(QCoreApplication::translate("MainWindow", "CLEAR", nullptr));
        touche_fois->setText(QCoreApplication::translate("MainWindow", "x", nullptr));
        cacher_prgm->setText(QCoreApplication::translate("MainWindow", "Cacher", nullptr));
        touche_divent->setText(QCoreApplication::translate("MainWindow", "DIV", nullptr));
        touche_moins->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        touche_point->setText(QCoreApplication::translate("MainWindow", ".", nullptr));
        touche_egal->setText(QCoreApplication::translate("MainWindow", "=", nullptr));
        touche_9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        touche_div->setText(QCoreApplication::translate("MainWindow", "/", nullptr));
        touche_plus->setText(QCoreApplication::translate("MainWindow", "+", nullptr));
        touche_3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        touche_var->setText(QCoreApplication::translate("MainWindow", "Variables", nullptr));
        touche_4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        touche_neg->setText(QCoreApplication::translate("MainWindow", "+/-", nullptr));
        touche_0->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
        touche_inferegal->setText(QCoreApplication::translate("MainWindow", "<=", nullptr));
        touche_8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Etat de la pile", nullptr));
        cacher_clavier->setText(QCoreApplication::translate("MainWindow", "Cacher", nullptr));
        touche_eval->setText(QCoreApplication::translate("MainWindow", "EVAL", nullptr));
        touche_supegal->setText(QCoreApplication::translate("MainWindow", ">=", nullptr));
        touche_7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        touche_5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        touche_1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        touche_dup->setText(QCoreApplication::translate("MainWindow", "DUP", nullptr));
        touche_drop->setText(QCoreApplication::translate("MainWindow", "DROP", nullptr));
        touche_swap->setText(QCoreApplication::translate("MainWindow", "SWAP", nullptr));
        touche_ift->setText(QCoreApplication::translate("MainWindow", "IFT", nullptr));
        touche_clear_pile->setText(QCoreApplication::translate("MainWindow", "CLEAR", nullptr));
        touche_non->setText(QCoreApplication::translate("MainWindow", "NON", nullptr));
        touche_or->setText(QCoreApplication::translate("MainWindow", "OR", nullptr));
        touche_and->setText(QCoreApplication::translate("MainWindow", "AND", nullptr));
        touche_different->setText(QCoreApplication::translate("MainWindow", "!=", nullptr));
        menu_Fichier->setTitle(QCoreApplication::translate("MainWindow", "&Fichier", nullptr));
        menu_Programmes->setTitle(QCoreApplication::translate("MainWindow", "&Programmes", nullptr));
        menu_Variables->setTitle(QCoreApplication::translate("MainWindow", "&Variables", nullptr));
        menu_Outils->setTitle(QCoreApplication::translate("MainWindow", "&Outils", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
