#include "pile.h"

using namespace std;




//----------------------------Pile----------------------

Pile& Pile::donneInstancePile()
{
    static Pile uniqueinstance;
    return uniqueinstance;
}

size_t Pile::nbAffiche=4;

void Pile::affiche() const {
//    system("cls"); // system("cls") sous windows
    cout << message << endl;
    for(size_t i = nbAffiche; i>0; i--) {
        cout << i << " : ";
        if(i <= items.size()) //il y en a moins que nbAffiche, on affiche tout
        {
          cout<<items[items.size()-i]->GetId();
        }
        cout << endl;
    }
}

void Pile::PUSH(Item * it)
{
    items.push_back(it);
    setMessage("El�ment ajout� � la pile");
}

void Pile::DROP()
{
    if(estVide()) {
        //throw ComputException("Pile vide");
        setMessage("Pile vide !");
        return;

    }
    items.pop_back();// le popback va supprim� le pointeur mais pas l'element point� qui sera elemine avec le destructeur en fin de prg
}

Item* Pile::TOP() {

    if(estVide()) {
        setMessage("Pile vide !");
        throw ComputException("Pile vide");
    }
    //enum_id id_temp =items[items.size()-1]->GetId();
    //Item* it = new Item(id_temp);
    //it = items[items.size()-1];
    Item* top = items.back();
    return top;
}

void Pile::CLEAR()
{
    items.clear();// appel popback qui supprime tous les pointeurs mais paes les objets
    setMessage("Pile effac�e !");
}

void Pile::SWAP()
{
    if (items.size()<2)
    {
        setMessage("Pile trop petite !");
        throw ComputException("Pile trop petite");
    }

    swap(items[getTaille()-1],items[getTaille()-2]);

    setMessage("Swap effectu� !");

    /*it1 = TOP();
    DROP();
    it2 = TOP();
    DROP();
    PUSH(it1);
    PUSH(it2);*/

}

void Pile::DUP()
{
    if(estVide()) {
        //throw ComputException("Pile vide");
        return;
    }
    fflush(stdout);
    Item* copie = new Item(TOP()->GetId());
    copie=TOP();
    PUSH(copie);
    /*it = TOP();
    PUSH(it);*/
    setMessage("Duplication effectu�e !");
}


void Pile::IFT(){

//� L�op�rateur binaire IFT d�pile 2 arguments. Le 1er (i.e. le dernier d�pil�) est un test logique.
//Si la valeur de ce test est vrai, le 2e argument est �valu� sinon il est abandonn�.
    if (items.size()<2)
    {
        setMessage("Pile trop petite !");
        throw ComputException("Pile trop petite");
    }
    Item* it1 = new Item(TOP()->GetId());
    it1=TOP();
    DROP();
    Item* it2 = new Item(TOP()->GetId());
    it2=TOP();
    DROP();
    if ((it1->GetId() == Ent && it1->getValue()!=0) || it1->GetId() != Ent )
    {
            cout<<"test logique positif"<<endl;
            Entier* e1= new Entier (1);
            PUSH(e1);

          //evaluer arg2
          if(it2->GetId() == Ent ||it2->GetId() == Frac || it2->GetId() == Re)
            PUSH(it2);
          else if(it2->GetId() == At)
          {
              if(dynamic_cast<Atome*>(it2)->GetSs_Id() == Var)
              { cout<<"var"<<endl;
                PUSH(it2);}
              else if(dynamic_cast<Atome*>(it2)->GetSs_Id() == Prog)
                //dynamic_cast<Programme&>(it2).executer();
                cout<<"prog"<<endl;
          }
          else if(it2->GetId() == Exp)
            EVAL(ItemManager::donneInstance(), dynamic_cast<Expression&>(*it2));
    }

    else cout<<"test logique faux"<<endl;
}

void Pile::CALCUL(const string op)
{
    // Si moins de 2 �l�ments dans la pile --> Pas de calcul

    if ( getTaille() < 2 )
    {
        //  cout << "Pile::CALCUL - getTaille()=" << getTaille() << endl ; fflush(stdout);
        setMessage("Pas assez d'�lements dans la pile !");
        return;
    }
    Item* tab_item[2];
    Entier* tab_ent[2];
    Fraction* tab_frac[2];
    Reel* tab_re[2];
    // Indice 0 contient le 1er de la pile
    tab_item[0]=new Item(TOP()->GetId());
    tab_item[0]=TOP();
    DROP();
    // Indice 1 contient le 2�me de la pile
    tab_item[1]=new Item(TOP()->GetId());
    tab_item[1]=TOP();
    DROP();

    for (int i=0;i<2;i++)
    {
        switch(tab_item[i]->GetId())
        {
            case Ent:
                {tab_ent[i]=new Entier(dynamic_cast<Entier*>(tab_item[i])->getEnt());

                break;}
            case Frac:
                {tab_frac[i]=new Fraction(dynamic_cast<Fraction*>(tab_item[i])->getNum(),dynamic_cast<Fraction*>(tab_item[i])->getDeno());
                break;}
            case Re:
                {tab_re[i]=new Reel(dynamic_cast<Reel*>(tab_item[i])->getEnt(),dynamic_cast<Reel*>(tab_item[i])->getMant(),dynamic_cast<Reel*>(tab_item[i])->getNbZero(),dynamic_cast<Reel*>(tab_item[i])->getNeg());
                break;}
            default:
                {}
        }
    }
    /**
     ** Addition
     **/
    if (op=="+")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0] + *tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Fraction* newFrac=new Fraction(*tab_ent[0] + *tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Reel* newRe=new Reel(*tab_ent[0]+*tab_re[1]);
                        //cout<<*tab_ent[0]<<endl;
                        //cout<<tab_re[1]->getNeg()<<endl;
                        //cout<<*newRe<<endl;
                        fflush(stdout);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Fraction* newFrac=new Fraction(*tab_frac[0]+*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Fraction* newFrac= new Fraction(*tab_frac[0]+*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Reel* newRe=new Reel(*tab_frac[0]+*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Reel* newRe=new Reel(*tab_re[0]+*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Reel* newRe=new Reel(*tab_re[0]+*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Reel* newRe= new Reel(*tab_re[0]+*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }
    if (op=="-")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[1] - *tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Fraction* newFrac=new Fraction(*tab_frac[1]-*tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Reel* newRe=new Reel(*tab_re[1]-*tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Fraction* newFrac=new Fraction(*tab_ent[1]-*tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Fraction* newFrac= new Fraction(*tab_frac[1]-*tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Reel* newRe=new Reel(*tab_re[1]-*tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Reel* newRe=new Reel(*tab_ent[1]-*tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Reel* newRe=new Reel(*tab_frac[1]-*tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Reel* newRe= new Reel(*tab_re[1]-*tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}
        }
    return;
    }
    if (op=="*")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0] * *tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Fraction* newFrac=new Fraction(*tab_ent[0]**tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Reel* newRe=new Reel(*tab_ent[0]**tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Fraction* newFrac=new Fraction(*tab_frac[0]**tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Fraction* newFrac= new Fraction(*tab_frac[0]**tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Reel* newRe=new Reel(*tab_frac[0]**tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Reel* newRe=new Reel(*tab_re[0]**tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                    }
                    case Frac: //r�el + fraction
                    {
                        Reel* newRe=new Reel(*tab_re[0]**tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Reel* newRe= new Reel(*tab_re[0]**tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }

    if (op=="/")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Fraction* newFrac= new Fraction(*tab_ent[1]/ *tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Fraction* newFrac=new Fraction(*tab_frac[1]/ *tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Reel* newRe=new Reel(*tab_re[1]/ *tab_ent[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Fraction* newFrac=new Fraction(*tab_ent[1]/ *tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Fraction* newFrac= new Fraction(*tab_frac[1]/ *tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newFrac));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Reel* newRe=new Reel(*tab_re[1]/ *tab_frac[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Reel* newRe=new Reel(*tab_ent[1]/ *tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                    }
                    case Frac: //r�el + fraction
                    {
                        Reel* newRe=new Reel(*tab_frac[1]/ *tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Reel* newRe= new Reel(*tab_re[1]/ *tab_re[0]);
                        PUSH(dynamic_cast<Item*>(newRe));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}
        }
        return;
    }
    if (op=="DIV")
    {
        if (tab_item[0]->GetId()!=Ent || tab_item[1]->GetId()!=Ent)
        {
            setMessage("DIV disponible seulement entre deux entiers !");
            // on remet les deux �l�ments dans la pile
            PUSH(tab_item[1]);
            PUSH(tab_item[0]);
        }
        else
        {
            Entier* newEnt=new Entier(tab_ent[1]->DIV(*tab_ent[0]));
            PUSH(dynamic_cast<Item*>(newEnt));
            setMessage("Op�ration DIV effectu�e");
        }
    }
    if (op=="MOD")
    {
        if (tab_item[0]->GetId()!=Ent || tab_item[1]->GetId()!=Ent)
        {
            setMessage("MOD disponible seulement entre deux entiers !");
            // on remet les deux items dans la pile
            PUSH(tab_item[1]);
            PUSH(tab_item[0]);
        }
        else
        {
            Entier* newEnt=new Entier(tab_ent[1]->MOD(*tab_ent[0]));
            PUSH(dynamic_cast<Item*>(newEnt));
            setMessage("Op�ration MOD effectu�e");
        }
    }
}


void Pile::TESTS(const string op)
{
    // Si moins de 2 �l�ments dans la pile --> Pas de calcul

    if ( getTaille() < 2 )
    {
        setMessage("Pas assez d'�lements dans la pile !");
        return;
    }
    Item* tab_item[2];
    Entier* tab_ent[2];
    Fraction* tab_frac[2];
    Reel* tab_re[2];
    // Indice 0 contient le 1er de la pile
    tab_item[0]=new Item(TOP()->GetId());
    tab_item[0]=TOP();
    DROP();
    // Indice 1 contient le 2�me de la pile
    tab_item[1]=new Item(TOP()->GetId());
    tab_item[1]=TOP();
    DROP();

    for (int i=0;i<2;i++)
    {
        switch(tab_item[i]->GetId())
        {
            case Ent:
                {tab_ent[i]=new Entier(dynamic_cast<Entier*>(tab_item[i])->getEnt());

                break;}
            case Frac:
                {tab_frac[i]=new Fraction(dynamic_cast<Fraction*>(tab_item[i])->getNum(),dynamic_cast<Fraction*>(tab_item[i])->getDeno());
                break;}
            case Re:
                {tab_re[i]=new Reel(dynamic_cast<Reel*>(tab_item[i])->getEnt(),dynamic_cast<Reel*>(tab_item[i])->getMant(),dynamic_cast<Reel*>(tab_item[i])->getNbZero(),dynamic_cast<Reel*>(tab_item[i])->getNeg());
                break;}
            default:
                {}
        }
    }
    /**
     ** <=
     **/
    if (op=="<=")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }
    /**
     ** <
     **/
    if (op=="<")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]<*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]<*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Entier* newEnt= new Entier(*tab_re[0]<*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}
        }
        return;
    }

    /**
     ** >=
     **/
    if (op==">=")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }
    /**
     ** >
     **/
    if (op==">")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]>*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]>*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Entier* newEnt= new Entier(*tab_re[0]>*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }
    /**
     ** =
     **/
    if (op=="=")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                Entier* newEnt= new Entier(*tab_ent[0]==*tab_ent[1]);
                PUSH(dynamic_cast<Item*>(newEnt));
                break;
            }
            case Frac:
            {
                Entier* newEnt= new Entier(*tab_frac[0]==*tab_frac[1]);
                PUSH(dynamic_cast<Item*>(newEnt));
                break;
            }
            case Re:
            {
                Entier* newEnt= new Entier(*tab_re[0]==*tab_re[1]);
                PUSH(dynamic_cast<Item*>(newEnt));
                break;
            }
            default:
                break;
        }
        return;
    }
    /**
     ** !=
     **/
    if (op=="!=")
    {
        switch(tab_item[0]->GetId())
        {
            case Ent:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //entier + entier
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]!=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //entier + fraction
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]!=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //entier + r�el
                    {
                        Entier* newEnt= new Entier(*tab_ent[0]!=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Frac:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //fraction + entier
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]!=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //fraction + fraction
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]!=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //fraction+r�el
                    {
                        Entier* newEnt= new Entier(*tab_frac[0]!=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
                break;
            }
            case Re:
            {
                switch(tab_item[1]->GetId())
                {
                    case Ent: //r�el + entier
                    {
                        Entier* newEnt= new Entier(*tab_re[0]!=*tab_ent[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Frac: //r�el + fraction
                    {
                        Entier* newEnt= new Entier(*tab_re[0]!=*tab_frac[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    case Re: //r�el + r�el
                    {
                        Entier* newEnt= new Entier(*tab_re[0]!=*tab_re[1]);
                        PUSH(dynamic_cast<Item*>(newEnt));
                        break;
                    }
                    default:
                        {break;}
                }
               break;
            }
            default:
                {break;}

        }
        return;
    }
}


string Pile::r_affiche() const {

    stringstream res;

    for(size_t i = nbAffiche; i>0; i--) {

        if(i <= items.size()) //il y en a moins que nbAffiche, on affiche tout
        {

            res << "(" << i << ") ";

            int id=items[items.size()-i]->GetId();
            Item* it=items[items.size()-i];
            switch (id)
            {
            // Ent,Frac,Re,Prog,Var,Op,Exp,At
            case Ent:
            case Frac :
            case Re:
              //cout<< "(" << id << ") Entier----->" << dynamic_cast<Entier*>(it)->getEnt();
              //res << "(Ent) " << dynamic_cast<Entier*>(it)->getEnt();
              res << dynamic_cast<LitNumerique*>(it)->toString();
              break;
            case Prog:
              //cout<< "(" << id << ") Programme-->" << dynamic_cast<Programme*>(it)->getNom();
              res << dynamic_cast<Programme*>(it)->getNom();
              break;
            case Var:
              //cout<< "(" << id << ") Variable--->" << dynamic_cast<Variable*>(it)->getNom();;
              res << dynamic_cast<Variable*>(it)->getNom();;
              break;
            case Op:
              //cout<< "(" << id << ") Operateur-->" << dynamic_cast<Operateur*>(it)->getNom();
              res << dynamic_cast<Operateur*>(it)->getNom();
              break;
            case Exp:
              //cout<< "(" << id << ") Expression->" << dynamic_cast<Expression*>(it)->getNom();
              res << dynamic_cast<Expression*>(it)->getNom();
              break;
            case At:
              //cout<< "(" << id << ") Atome------>" << dynamic_cast<Atome*>(it)->getNom();
              res << dynamic_cast<Atome*>(it)->getNom();
              break;
            }
            res << endl;
        }
    }
    return res.str();
}
