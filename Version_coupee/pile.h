#ifndef PILE_H_INCLUDED
#define PILE_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>
#include "item.h"
#include "litnum.h"
#include "litterales.h"
//#include "itemmanag.h"

using namespace std;



class Pile {
    //Item* items = nullptr; // Tableau contenant la saisie utilisateur
    vector<Item*> items;
    //size_t nb = 0;
    //size_t nbMax = 0;
    static size_t nbAffiche; // Nombre d'items � afficher au max (pour ne pas afficher toute la pile, seuls les nbAffiche au sommet sont affich�es)
    string message = ""; // Message � afficher � l'utilisateur quand il appelle affiche()
    //void agrandissementCapacite();
    Pile() = default;
public:
    static Pile& donneInstancePile();
    //~Pile() { delete[] items;}
    //void agrandissementCapacite();
    // Affiche un message � l'utilisateur ainsi que l'�tat courant de la pile (les nbAffiche �l�ments au sommet)
    void affiche() const;
    string r_affiche() const;
    // Empiler
    void PUSH(Item* it);
    // d�pile la litt�rale au sommet de la pile
    void DROP();
    // Indique si la pile est vide
    bool estVide() const { return items.empty();}
    // Renvoie la taille de la pile
    size_t getTaille() const { return items.size();}
    //Renvoie nb affiche
    static size_t getNbAffiche(){return nbAffiche;};
    // Renvoie l'�l�ment au sommet de la pile
    Item* TOP();
    void setMessage(const string& m) { message = m;}
    string getMessage() const { return message;}
    void setNbItemsToAffiche(size_t n) { nbAffiche = n;}
    //Duplique le sommet de la pile
    void DUP(); //L'item pris en param�tre permet le passage par valeur. Un passage par adresse ne marche pas
    //Intervertit les deux derniers �l�ments empil�s dans la pile
    void SWAP();//Les item pris en param�tres permettent le passage par valeur. Un passage par adresse ne marche pas
    //vide tous les �l�ments de la pile.
    void CLEAR();
    //L�op�rateur binaireIFTd�pile 2 arguments. Le 1er (i.e.le dernier d�pil�) est un test logique.
    //Si la valeur de ce test est vrai, le2eargument est �valu� sinon il est abandonn�.
    void IFT();
    void CALCUL(const string op);
    void TESTS(const string op);
};

#endif // PILE_H_INCLUDED
