#ifndef DIALOGPRGM_H
#define DIALOGPRGM_H

#include <QDialog>
#include <QListWidget>
#include "pile.h"
#include "item.h"
#include "litterales.h"
#include "litnum.h"
#include "controller.h"

namespace Ui {
class DialogPrgm;
}
/**
 * @brief The DialogPrgm class
 */
class DialogPrgm : public QDialog
{
    Q_OBJECT

public:
    explicit DialogPrgm(QWidget *parent = nullptr);
    ~DialogPrgm();

    QList<QString> itemName;
    QList<QString> itemText;

    void initialiser(); // initialiser les elements de l'interface utilisateur

    void setFonction(const char fct) {fonction=fct;}
    char getFonction() {return fonction;}

signals:
    void prgmOkPressed();

private slots:

    void on_buttonBox_accepted();

    void on_prgmList_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButtonEnregistrer_clicked();

    void on_pushButtonAnnuler_clicked();

    void on_pushButtonNouveau_clicked();

    void on_pushButtonSupprimer_clicked();

    void on_prgmList_itemSelectionChanged();

    void on_pushButtonModifier_clicked();

    void on_pushButtonDupliquer_clicked();

private:
    Ui::DialogPrgm *ui;

    void setControles(bool etat);

    char fonction; // 'V' pour variables, 'P' pour programmes

    Pile* pile;

    ItemManager* itmMgr;

};

void prgmEVAL(string instructions);

#endif // DIALOGPRGM_H
