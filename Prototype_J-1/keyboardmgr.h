#ifndef KEYBOARDMGR_H
#define KEYBOARDMGR_H

#include <QObject>

class KeyboardMgr : public QObject
{
    Q_OBJECT
public:
    KeyboardMgr();

protected:
    bool eventFilter(QObject* obj, QEvent* event);
};

#endif // KEYBOARDMGR_H
