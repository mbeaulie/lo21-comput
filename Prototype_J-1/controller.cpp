#include "controller.h"

using namespace std;

void Controller::commande(const string& c){
    //analyse du 1er caractere
    //if isOperator(c) {
        //switch(c){
            //'+':
    // si LITNUMERIQUE
    if (isdigit(c[0])) {
        size_t pos = c.find(".");
        if (pos != std::string::npos) {
            Reel rtemp(stoi(c.substr(0,pos)), stoi(c.substr(pos+1)));
            Reel* prtemp = new Reel(rtemp.getEnt(), rtemp.getMant());
            Pile::donneInstancePile().Pile::PUSH(prtemp);
            cout<<"Objet REEL cree : "<<prtemp<<endl;
        } else {
            pos = c.find("/");
            if (pos != std::string::npos){
            Fraction ftemp(stoi(c.substr(0,pos)), stoi(c.substr(pos+1)));
            Fraction* pftemp = new Fraction(ftemp.getNum(), ftemp.getDeno());
            Pile::donneInstancePile().Pile::PUSH(pftemp);
            cout<<"Objet FRACTION cree : "<<pftemp<<endl;
            } else {
                Entier etemp(stoi(c));
                // modification SJD
                //Entier* petemp = &etemp;
                Entier* petemp = new Entier(etemp);
                Pile::donneInstancePile().Pile::PUSH(petemp);
                cout<<"Objet ENTIER cree : "<<petemp<<endl;
            }
        }
    } else {
        if ((64 < int(c[0])) && (int(c[0]) < 91)) {
            if (islowerall(c)) { /* TODO -->
                for(ItemManager::Iterator iter=ItemManager::donneInstance().begin(); iter!=ItemManager::donneInstance().end();++iter) {
                    cout<<*iter<<endl;
                    if (c == (*iter).getNom()){
                        if ((*iter.getId() == 3) { //si c programme, exe
                            (*iter).executer();
                            return;
                        }
                        if ((*iter.getId() == 4) { // si variable, push
                            Pile::PUSH(iter);
                            return;
                        }
                    }
                } 		<-- TODO */
                //sinon (inconnu?), on cree une expression ("nom")
                Expression exptemp('"'+c+'"');
                Expression* pexptemp = &exptemp;
                Pile::donneInstancePile().Pile::PUSH(pexptemp);
                cout<<"Objet EXPRESSION cree : "<<endl;
            } else {
                ComputException("Minuscules interdites. Utiliser des majuscules dans la saisie d'atome et d'expressions."); //via Qt
                return;
            }
        }
        if (c[0] == '"'){ // et finissant par " ?
            if (islowerall(c)) {
                Expression exptemp(c.substr(1, std::string::npos-1));
                Expression* pexptemp = &exptemp;
                Pile::donneInstancePile().Pile::PUSH(pexptemp);
                cout<<"Objet EXPRESSION cree : "<<exptemp<<endl;
            } else {
                ComputException("Minuscules interdites. Utiliser des majuscules dans la saisie d'atome et d'expressions.");//via Qt
                return;
            }
        }
        if (c[0] == '['){ //et finissant par ] ?
            //Programme progtemp(c.substr(1, std::string::npos-1));
            //Programme* pprogtemp = &progtemp;
            //Pile::donneInstancePile().Pile::PUSH(pprogtemp);
            //cout<<"Objet PROGRAMME cree : "<<progtemp<<endl;
        }
    }
}



void::Controller::execute(const string& l)
{

    string lit="";
    string cmd="";
    bool litEnCours=false;
    bool cmdEnCours=false;
    char c=0;;

    string instructions=l;

    instructions +=" "; // pour toujours avec un espace a la fin

    int insSize=instructions.size();

    for (int i=0 ; i < insSize ; i++)
    {
        c=instructions.at(i);

        if (c=='\n' || c=='\t')
            c=' ';  // pour traitrer les \n et les \t comme des espaces

        //std::cout << "pgrmEVAL(), Debut tests ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);

        if ( c == ' ' && litEnCours)
        {
            if (litEnCours && lit != "-" )
            {
                litEnCours=false;
                //std::cout << "pgrmEVAL(), lit Termine ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);
                // ici traiter le literal
                size_t pos = lit.find(".");
                if (pos != std::string::npos) {
                    int e=stoi(lit.substr(0,pos));
                    bool neg=false;
                    if ( (e==0) && (lit.substr(0,1) == "-"))
                        neg=true;
                    Reel rtemp(e, stoi(lit.substr(pos+1)));
                    Reel* prtemp = new Reel(rtemp.getEnt(), rtemp.getMant());
                    prtemp->setNeg(neg);
                    Pile::donneInstancePile().Pile::PUSH(prtemp);
                    //cout<<"Objet REEL cree : "<<prtemp<<endl;
                } else {
                    pos = lit.find("/");
                    if (pos != std::string::npos){
                    Fraction ftemp(stoi(lit.substr(0,pos)), stoi(lit.substr(pos+1)));
                    Fraction* pftemp = new Fraction(ftemp.getNum(), ftemp.getDeno());
                    Pile::donneInstancePile().Pile::PUSH(pftemp);
                    //cout<<"Objet FRACTION cree : "<<pftemp<<endl;
                    } else {
                        Entier etemp(stoi(lit));
                        // modification SJD
                        //Entier* petemp = &etemp;
                        Entier* petemp = new Entier(etemp);
                        Pile::donneInstancePile().Pile::PUSH(petemp);
                        //cout<<"Objet ENTIER cree : "<<petemp<<endl;
                    }
                 }
                lit.clear();
            }
            else
            {
                if (lit == "-") // il n'y a qu'un moins
                {
                    litEnCours=false; // en fait c'etait une commande --> on repasse le meme caractere en indiquant qu'une commande en est cours
                    cmdEnCours=true;
                    cmd=lit;
                    lit.clear();
                    i--;
                }
                else
                {
                    litEnCours=true;
                    lit.clear();
                    //std::cout << "pgrmEVAL(), lit Debute  ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);
                }
            }
        }

        else // ce n'est pas un espace
        {
            if ( (( c >= '0' && c <='9') || c == '-') && litEnCours==false)
            {
                litEnCours=true;
            }

            if ( ( (c>='0')&&(c<='9')) ||  (c=='-') || (c=='/') || (c=='.') ) // le caractere est il valide ?
            {
                lit += c;
                //std::cout << "pgrmEVAL(), lit Ajoute  ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);
            }
            else
            {
                if ( c == ' ' ) // separateur de commande
                {
                    if ( cmdEnCours )
                    {
                        cmdEnCours=false;
                        //std::cout << "pgrmEVAL(), cmd Termine ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);

                        // ici traiter la commande

                        if ( ( cmd == "+") || ( cmd == "-") ||( cmd == "/") ||( cmd == "*")||( cmd == "DIV")||( cmd == "MOD") )
                            try {
                                Pile::donneInstancePile().Pile::CALCUL(cmd);
                        }  catch (...) {
                        }
                        else if ( ( cmd == "<" ) || (cmd == ">") || (cmd == ">=") || (cmd == "<=") || (cmd == "!=") || (cmd == "==" ) )
                            try {
                                Pile::donneInstancePile().Pile::TESTS(cmd);
                        }  catch (...) {
                        }
                        else if ( cmd == "AND")
                            try {
                            OpAND();
                        }  catch (...) {
                        }
                        else if ( cmd == "OR")
                            try {
                            OpOR();
                        }  catch (...) {
                        }
                        else if ( (cmd == "NOT") || (cmd == "NON"))
                            try {
                            OpNON();
                        }  catch (...) {
                        }
                        else if ( cmd == "SWAP")
                            try {
                            Pile::donneInstancePile().Pile::SWAP();
                        }  catch (...) {
                        }
                        else if ( cmd == "CLEAR")
                            try {
                            Pile::donneInstancePile().Pile::CLEAR();
                        }  catch (...) {
                        }
                        else if ( cmd == "DROP")
                            try {
                            Pile::donneInstancePile().Pile::DROP();
                        }  catch (...) {
                        }
                        else if ( cmd == "IFT")
                            try {
                            Pile::donneInstancePile().Pile::IFT();
                        }  catch (...) {
                        }
                        else if ( cmd == "PUSH")
                            {
                            // Pile::donneInstancePile().Pile::PUSH(); // commande par defaut
                            }
                        else
                        {
                            Pile::donneInstancePile().Pile::setMessage("Commande inconnue : " + cmd);
                        }
                        cmd.clear();
                    }
                }
                if ( ((c>='A')&&(c<='Z')) || ((c>='0')&&(c<='9')) || (c=='+') || (c=='-') || (c=='/') || (c=='*') || (c=='<') || (c=='>') || (c=='.') ) // le caractere est il valide ?
                {
                    cmd+=c;
                    cmdEnCours=true;
                    //std::cout << "pgrmEVAL(), cmd Ajoute ; c=" << c << ", lit=" << lit << ", cmd=" << cmd << ", litEnCours=" << litEnCours << ", cmdEnCours=" << cmdEnCours << std::endl; fflush(stdout);
                }
            }
         }
    }
}

bool islowerall(const string s)
{
    for (unsigned int i = 0; i <= s.size(); ++i)
        if (islower(int(s[i]))) return false;
    return true;
}
