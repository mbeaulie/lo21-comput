#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>

using namespace std;

enum enum_id {Ent,Frac,Re,Prog,Var,Op,Exp,At};

class ComputException{
string info;
public:
    ComputException(const string& s): info(s){}
    string GetInfo() const {return info;}
};


class Item{
enum_id id;
public:
    virtual const int getValue()const {return 0;}
    Item(enum_id _id):id(_id){}
    virtual ~Item()=default;
    const enum_id GetId() const {return id;}
    Item& operator=(const Item& it){id = it.GetId(); return *this;}
};

ostream & operator<<(ostream &f, Item &i);


class Pile {
    //Item* items = nullptr; // Tableau contenant la saisie utilisateur
    vector<Item*> items;
    //size_t nb = 0;
    //size_t nbMax = 0;
    static size_t nbAffiche; // Nombre d'items à afficher au max (pour ne pas afficher toute la pile, seuls les nbAffiche au sommet sont affichées)
    string message = ""; // Message à afficher à l'utilisateur quand il appelle affiche()
    //void agrandissementCapacite();
    Pile() = default;
public:
    static Pile& donneInstancePile();
    //~Pile() { delete[] items;}
    //void agrandissementCapacite();
    // Affiche un message à l'utilisateur ainsi que l'état courant de la pile (les nbAffiche éléments au sommet)
    void affiche() const;
    string r_affiche() const;
    // Empiler
    void PUSH(Item* it);
    // dépile la littérale au sommet de la pile
    void DROP();
    // Indique si la pile est vide
    bool estVide() const { return items.empty();}
    // Renvoie la taille de la pile
    size_t getTaille() const { return items.size();}
    //Renvoie nb affiche
    static size_t getNbAffiche(){return nbAffiche;};
    // Renvoie l'élément au sommet de la pile
    Item* TOP();
    void setMessage(const string& m) { message = m;}
    string getMessage() const { return message;}
    void setNbItemsToAffiche(size_t n) { nbAffiche = n;}
    //Duplique le sommet de la pile
    void DUP(); //L'item pris en paramètre permet le passage par valeur. Un passage par adresse ne marche pas
    //Intervertit les deux derniers éléments empilés dans la pile
    void SWAP();//Les item pris en paramètres permettent le passage par valeur. Un passage par adresse ne marche pas
    //vide tous les éléments de la pile.
    void CLEAR();
    //L’opérateur binaireIFTdépile 2 arguments. Le 1er (i.e.le dernier dépilé) est un test logique.
    //Si la valeur de ce test est vrai, le2eargument est évalué sinon il est abandonné.
    void IFT();
    void CALCUL(const string op);
    void TESTS(const string op);
};


class LitNumerique: public Item { // classe d'interface

public:
    LitNumerique(enum_id _id): Item(_id){}
    ~LitNumerique() = default;
    virtual const LitNumerique& operator+(const LitNumerique& l)const = 0;
    virtual const LitNumerique& operator-(const LitNumerique& l)const = 0;
    virtual const LitNumerique& operator*(const LitNumerique& l)const = 0;
    //virtual const LitNumerique& operator/(const LitNumerique* l)const = 0;
    virtual const LitNumerique& NEG() const = 0;
    string toString();
};




class Entier: public LitNumerique{
private:
    int ent;
    enum_id id=Ent;
public:
    Entier(int i=0): LitNumerique(Ent), ent(i){}
    Entier(const Entier& e): LitNumerique(Ent), ent(e.getEnt()){}
    ~Entier() = default;
    const int getEnt() const {return ent;}

    const Entier& operator+(const LitNumerique& e)const {
        Entier* p = new Entier(ent + dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier& operator-(const LitNumerique& e)const {
        Entier* p = new Entier(ent - dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier& operator*(const LitNumerique& e)const{
        Entier* p = new Entier(ent * dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier DIV(const Entier& e) const{ 		// soit const Entier& qui renvoie *p
        Entier i(ent/e.ent);
        return i;
    }
    const Entier MOD(const Entier& e) const {		// soit const Entier qui renvoie e QUELLE DIFFERENCE
        Entier i(ent%e.ent);
        return i;
    }
    const Entier& NEG() const{
        Entier* p = new Entier(-dynamic_cast<const Entier&>(*this).ent);
        return (*p);
    }
    bool operator<(const Entier& e) const {return ent<e.ent;}
    bool operator>(const Entier& e) const {return ent>e.ent;}
    bool operator<=(const Entier& e) const {return ent<=e.ent;}
    bool operator>=(const Entier& e) const {return ent>=e.ent;}
    bool operator==(const Entier& e) const {return ent==e.ent;}
    bool operator!=(const Entier& e) const {return ent!=e.ent;}
    void operator++(){ent++;}
    void operator--(){ent--;}
    const int getValue() const {getEnt();}
};

ostream & operator<<(ostream &f, Entier &e);


class Fraction: public LitNumerique
{
    public:
    Fraction(Entier n, Entier d, int test): LitNumerique(Frac), numerateur(n),denominateur(d){}
    Fraction(Entier n, Entier d): LitNumerique(Frac), numerateur(n),denominateur(d){simplification();};
    const Entier getNum() const {return numerateur;}
    const Entier getDeno() const {return denominateur;}
    void setFraction(Entier n, Entier d);
    const Fraction& operator+(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur + f1.numerateur*denominateur , denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator-(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur - f1.numerateur*denominateur , denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator*(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.numerateur,denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator/(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur,denominateur*f1.numerateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& NEG() const {
        Fraction* p = new Fraction(dynamic_cast<const Fraction&>(*this).numerateur.NEG(),denominateur);
        return (*p);
    }

    void
    simplification();
    //double const getValue()const;

    bool operator<(const Fraction& f) const {
        Fraction f1(numerateur*f.denominateur, denominateur*f.denominateur, 0);
        Fraction f2(f.numerateur*denominateur, f.denominateur*denominateur, 0);
        return f1.getNum()<f2.getNum();
    }
    bool operator>(const Fraction& f) const {
        Fraction f1(numerateur*f.denominateur, denominateur*f.denominateur, 0);
        Fraction f2(f.numerateur*denominateur, f.denominateur*denominateur, 0);
        return f1.getNum()>f2.getNum();
    }
    bool operator==(const Fraction& f) const {return ((numerateur==f.numerateur)&&(denominateur==f.denominateur));}
    bool operator!=(const Fraction& f) const {return ((numerateur!=f.numerateur)||(denominateur!=f.denominateur));}
    bool operator<=(const Fraction& f) const{return (*this<f)||(*this==f);}
    bool operator>=(const Fraction& f) const{return (*this>f)||(*this==f);}

    private:
        Entier numerateur;
        Entier denominateur;
        enum_id id= Frac;
};

ostream & operator<<(ostream& f, Fraction& frac);
int taille(int n);

class Reel : public LitNumerique
{
    public:
        Reel(Entier e): LitNumerique(Re), ent(e), mant(0), nb_zero(0){}
        Reel(int e=0, int m=0, int n=0): LitNumerique(Re), ent(Entier(e)), mant(Entier(m)), nb_zero(n){if (mant.getEnt()==0) simplification();}
        Reel(Entier e, Entier m, int n = 0, bool neg = false): LitNumerique(Re), ent(e), mant(m), nb_zero(n), negatif(neg){if (mant.getEnt()==0) simplification();}
        const Entier  getEnt() const {return ent;}
        const Entier  getMant()const {return mant;}
        const bool getNeg()const{return negatif;}
        void setNeg(bool neg){negatif=neg;}
        const int getNbZero()const {return nb_zero;}
        virtual const Reel& operator+(const LitNumerique& r1) const;
        virtual const Reel& operator-(const LitNumerique& r1) const;
        virtual const Reel& operator*(const LitNumerique& r1) const;
        virtual const Reel& operator/(const LitNumerique& r1) const;
        virtual const Reel& NEG() const {
            Reel* p = new Reel(dynamic_cast<const Reel&>(*this).ent.NEG(),mant);
            return (*p);
            }
    //double const getValue()const {return ent.getValue()+((double) mant.getValue())*pow(10, -taille(mant.getValue()));}
        Entier simplification();

        bool operator<(const Reel& r) const {return ((ent<r.ent)||((ent==r.ent)&&(nb_zero>r.nb_zero))||((ent==r.ent)&&(nb_zero==r.nb_zero)&&(mant<r.mant)));}
        bool operator>(const Reel& r) const {return ((ent>r.ent)||((ent==r.ent)&&(nb_zero<r.nb_zero))||((ent==r.ent)&&(nb_zero==r.nb_zero)&&(mant>r.mant)));}
        bool operator==(const Reel& r) const {return ((ent==r.ent)&&(mant==r.mant)&&(nb_zero==r.nb_zero));}
        bool operator!=(const Reel& r) const {return ((ent!=r.ent)||(mant!=r.mant)||(nb_zero!=r.nb_zero));}
        bool operator<=(const Reel& r) const {return (*this<r)||(*this ==r);}
        bool operator>=(const Reel& r) const {return (*this>r)||(*this ==r);}

    private:
        Entier ent;
        Entier mant;
        int nb_zero; //nombre de 0 après la virgule
        bool negatif; //si la partie réelle est nulle et le nombre négatif.
        enum_id id= Re;
};
ostream & operator<<(ostream& f, const Reel& r);
int taille(int n);
int puissance_dix(int n);
Reel r_cast(const Fraction& f);

/*-------------------Operateurs-numeriques---------------------*/

const Fraction operator/(const Entier& n, const Entier& d);

const Fraction operator+(const Entier& e, const Fraction& f);
const Fraction operator+(const Fraction& f, const Entier& e);
const Reel operator+(const Entier& e, const Reel& r);
const Reel operator+(const Reel& r, const Entier& e);
const Reel operator+(const Fraction& f, const Reel& r);
const Reel operator+(const Reel& r, const Fraction& f);

const Fraction operator-(const Entier& e, const Fraction& f);
const Fraction operator-(const Fraction& f, const Entier& e);
const Reel operator-(const Entier& e, const Reel& r);
const Reel operator-(const Reel& r, const Entier& e);
const Reel operator-(const Fraction& f, const Reel& r);
const Reel operator-(const Reel& r, const Fraction& f);

const Fraction operator*(const Entier& e, const Fraction& f);
const Fraction operator*(const Fraction& f, const Entier& e);
const Reel operator*(const Entier& e, const Reel& r);
const Reel operator*(const Reel& r, const Entier& e);
const Reel operator*(const Fraction& f, const Reel& r);
const Reel operator*(const Reel& r, const Fraction& f);

const Fraction operator/(const Entier& e, const Fraction& f);
const Fraction operator/(const Fraction& f, const Entier& e);
const Reel operator/(const Entier& e, const Reel& r);
const Reel operator/(const Reel& r, const Entier& e);
const Reel operator/(const Fraction& f, const Reel& r);
const Reel operator/(const Reel& r, const Fraction& f);

/*---------------------Operateurs-logiques-----------------------*/

const bool operator<(const Entier& e, const Fraction& f);
const bool operator<(const Fraction& f, const Entier& e);
const bool operator<(const Entier& e, const Reel& r);
const bool operator<(const Reel& r, const Entier& e);
const bool operator<(const Fraction& f, const Reel& r);
const bool operator<(const Reel& r, const Fraction& f);

const bool operator>(const Entier& e, const Fraction& f);
const bool operator>(const Fraction& f, const Entier& e);
const bool operator>(const Entier& e, const Reel& r);
const bool operator>(const Reel& r, const Entier& e);
const bool operator>(const Fraction& f, const Reel& r);
const bool operator>(const Reel& r, const Fraction& f);

const bool operator==(const Entier& e, const Fraction& f);
const bool operator==(const Fraction& f, const Entier& e);
const bool operator==(const Entier& e, const Reel& r);
const bool operator==(const Reel& r, const Entier& e);
const bool operator==(const Fraction& f, const Reel& r);
const bool operator==(const Reel& r, const Fraction& f);

const bool operator!=(const Entier& e, const Fraction& f);
const bool operator!=(const Fraction& f, const Entier& e);
const bool operator!=(const Entier& e, const Reel& r);
const bool operator!=(const Reel& r, const Entier& e);
const bool operator!=(const Fraction& f, const Reel& r);
const bool operator!=(const Reel& r, const Fraction& f);

const bool operator>=(const Entier& e, const Fraction& f);
const bool operator>=(const Fraction& f, const Entier& e);
const bool operator>=(const Entier& e, const Reel& r);
const bool operator>=(const Reel& r, const Entier& e);
const bool operator>=(const Fraction& f, const Reel& r);
const bool operator>=(const Reel& r, const Fraction& f);

const bool operator<=(const Entier& e, const Fraction& f);
const bool operator<=(const Fraction& f, const Entier& e);
const bool operator<=(const Entier& e, const Reel& r);
const bool operator<=(const Reel& r, const Entier& e);
const bool operator<=(const Fraction& f, const Reel& r);
const bool operator<=(const Reel& r, const Fraction& f);



/*
L’opérateurEVALpermet d’évaluer numériquement une littérale expression.
Si la littérale atome associée à l’expression correspond à l’identificateur d’une variable,
l’évaluation provoque l’empilement de la littérale associée cette variable.
Si la littérale atome associé à l’expression correspond à l’identificateurd’un programme,
l’évaluation provoque l’évaluation du programme associé (i.e.l’exécution de la suite d’opérandes du programme).
Si la littérale expression est un atome qui ne correspond pas au nom d’unevariable ou d’un programme,
l’évaluation n’a aucune effet et un message en informe l’utilisateur.
*/

class Expression: public Item
{
    string nom;
    enum_id id= Exp;
public:
    Expression(const string& n):Item(Exp), nom(n){};
    const string& getNom() const {return nom;}

};


class Atome: public Item{
string nom;
enum_id ss_id;
public:
    Atome(const string& n): Item(At), nom(n){
    char firstcar = n.at(0);
    if (firstcar<'A'|| firstcar>'Z')
        throw ComputException("la premi�re lettre doit etre une majuscule ");
    }

    const string& getNom() const {return nom;}
    virtual const enum_id GetSs_Id() const {return ss_id;}

};

//stocke une variable/prog lors de l'appel de STO
//stocke les oeprateurs au moment de leur creation pour STO
//les atomes sont ils stockés? Expressions ?


class ItemManager {
    Item** it = nullptr;
    size_t nb = 0;
    size_t nbMax = 0;
    void agrandissementCapacite();
    ItemManager() = default;
    ~ItemManager();
    ItemManager(const ItemManager & m);
    ItemManager & operator=(const ItemManager & m);
public:
    static ItemManager& donneInstance();
    Item& addItem(Item* item);
    void removeItem(Item& e);
    void cleartab(){for (unsigned int i=0;i<nb;i++) delete it[i];nb=0;}// Ajout de nb=0 par SJD pour dire qu'il n'y a plus d'élément
    void STO(Item& it, Expression& exp);
    void FORGET(Atome& a);
    friend void EVAL(ItemManager& ItMan, Expression& e);

    /*class Iterator{
            friend class ItemManager;
            Item** courant;
            Iterator(Item** iter):courant(iter){}
        public:
            enum_id operator*() const {
                if((*courant)->GetId()!= At)
                    return (*courant)->GetId();
                else
                    return dynamic_cast<Atome*>(*courant)->GetSs_Id();
                }

            Iterator& operator++(){++courant; return *this;}
            Iterator operator++(int){Iterator old=*this; ++courant; return old;}
            bool operator==(Iterator iter){return courant==iter.courant;}
            bool operator!=(Iterator iter){return courant!=iter.courant;}
   };

    Iterator begin(){return Iterator(it);}
    Iterator end(){return Iterator(it+nb);}
    */

  class Iterator{
          friend class ItemManager;
          Item** courant;
          Iterator(Item** iter):courant(iter){}
      public:
          Item* operator*() const {
                  return (*courant);
              }
          Iterator& operator++(){++courant; return *this;}
          Iterator operator++(int){Iterator old=*this; ++courant; return old;}
          bool operator==(Iterator iter){return courant==iter.courant;}
          bool operator!=(Iterator iter){return courant!=iter.courant;}
 };

  Iterator begin(){return Iterator(it);}
  Iterator end(){return Iterator(it+nb);}

};
class Variable: public Atome
{
    Item* it;
    enum_id ss_id = Var;
public:
   Variable(const string& s, Item& val): Atome(s), it(&val), ss_id(Var){}
    const enum_id GetSs_Id() const {return ss_id;}
    Item* getItem() const {return it;}
};

class Programme: public Atome {

string instructions;
enum_id ss_id;

public:
    Programme(const string& n, const string& i): Atome(n), instructions(i), ss_id(Prog){
    /*if (instructions.at(0) != '[' || instructions.at(instructions.size()) !=']')
        throw ComputException("mauvaise syntaxe, instructions entre crochets");*/
    }
    const string& GetInstructions() const {return instructions;}
    void executer();
    const enum_id GetSs_Id() const {return ss_id;}

};



class Operateur: public Item{
string nom;
int arite;
enum_id id;
public:
    Operateur(int ar,const string& s):Item(Op),nom(s),arite(ar){}
    const int getArite() const{return arite;}
    const string& getNom(){return nom;}

};


void OpAND();
void OpOR();
void OpNON();

class Controller{
    ItemManager& ItMng;
    Pile& pile;
public:
    Controller(ItemManager& man, Pile& p):ItMng(man), pile(p){} //donne instance ?
    void commande(const string& c); // On pourrait déplacer cette méthode dans la partie privée ?
    void execute(const string& l);
};

bool islowerall(const string s);





#endif // COMPUTER_H_INCLUDED
