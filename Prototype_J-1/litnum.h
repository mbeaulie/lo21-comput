#ifndef LITNUM_H_INCLUDED
#define LITNUM_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>
#include <math.h>

#include "item.h"

using namespace std;

class LitNumerique: public Item { // classe d'interface

public:
    LitNumerique(enum_id _id): Item(_id){}
    ~LitNumerique() = default;
    virtual const LitNumerique& operator+(const LitNumerique& l)const = 0;
    virtual const LitNumerique& operator-(const LitNumerique& l)const = 0;
    virtual const LitNumerique& operator*(const LitNumerique& l)const = 0;
    virtual const LitNumerique& NEG() const = 0;
    string toString();
};




class Entier: public LitNumerique{
private:
    int ent;
    enum_id id=Ent;
public:
    Entier(int i=0): LitNumerique(Ent), ent(i){}
    Entier(const Entier& e): LitNumerique(Ent), ent(e.getEnt()){}
    ~Entier() = default;
    int getEnt() const {return ent;}

    const Entier& operator+(const LitNumerique& e)const {
        Entier* p = new Entier(ent + dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier& operator-(const LitNumerique& e)const {
        Entier* p = new Entier(ent - dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier& operator*(const LitNumerique& e)const{
        Entier* p = new Entier(ent * dynamic_cast<const Entier&>(e).ent);
        return (*p);
    }
    const Entier DIV(const Entier& e) const{ 		// soit const Entier& qui renvoie *p
        Entier i(ent/e.ent);
        return i;
    }
    const Entier MOD(const Entier& e) const {		// soit const Entier qui renvoie e QUELLE DIFFERENCE
        Entier i(ent%e.ent);
        return i;
    }
    const Entier& NEG() const{
        Entier* p = new Entier(-dynamic_cast<const Entier&>(*this).ent);
        return (*p);
    }
    bool operator<(const Entier& e) const {return ent<e.ent;}
    bool operator>(const Entier& e) const {return ent>e.ent;}
    bool operator<=(const Entier& e) const {return ent<=e.ent;}
    bool operator>=(const Entier& e) const {return ent>=e.ent;}
    bool operator==(const Entier& e) const {return ent==e.ent;}
    bool operator!=(const Entier& e) const {return ent!=e.ent;}
    void operator++(){ent++;}
    void operator--(){ent--;}
    int getValue() const {return getEnt();}
};

ostream & operator<<(ostream &f, Entier &e);


class Fraction: public LitNumerique
{
    public:
    Fraction(Entier n, Entier d, int test=0): LitNumerique(Frac), numerateur(n),denominateur(d){if(test==0) simplification();};
    const Entier getNum() const {return numerateur;}
    const Entier getDeno() const {return denominateur;}
    void setFraction(Entier n, Entier d);
    const Fraction& operator+(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur + f1.numerateur*denominateur , denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator-(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur - f1.numerateur*denominateur , denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator*(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.numerateur,denominateur*f1.denominateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& operator/(const LitNumerique& f) const {
        Fraction f1 = dynamic_cast<const Fraction&>(f);
        Fraction* p = new Fraction(numerateur*f1.denominateur,denominateur*f1.numerateur);
        (*p).simplification();
        return (*p);
    }
    const Fraction& NEG() const {
        Fraction* p = new Fraction(dynamic_cast<const Fraction&>(*this).numerateur.NEG(),denominateur);
        return (*p);
    }

    void
    simplification();
    //double const getValue()const;

    bool operator<(const Fraction& f) const {
        Fraction f1(numerateur*f.denominateur, denominateur*f.denominateur, 1);
        Fraction f2(f.numerateur*denominateur, f.denominateur*denominateur, 1);
        return f1.getNum()<f2.getNum();
    }
    bool operator>(const Fraction& f) const {
        Fraction f1(numerateur*f.denominateur, denominateur*f.denominateur, 1);
        Fraction f2(f.numerateur*denominateur, f.denominateur*denominateur, 1);
        return f1.getNum()>f2.getNum();
    }
    bool operator==(const Fraction& f) const {return ((numerateur==f.numerateur)&&(denominateur==f.denominateur));}
    bool operator!=(const Fraction& f) const {return ((numerateur!=f.numerateur)||(denominateur!=f.denominateur));}
    bool operator<=(const Fraction& f) const{return (*this<f)||(*this==f);}
    bool operator>=(const Fraction& f) const{return (*this>f)||(*this==f);}

    private:
        Entier numerateur;
        Entier denominateur;
        enum_id id= Frac;
};

ostream & operator<<(ostream& f, Fraction& frac);
int taille(int n);

class Reel : public LitNumerique
{
    public:
        Reel(Entier e): LitNumerique(Re), ent(e), mant(0), nb_zero(0){}
        Reel(int e=0, int m=0, int n=0): LitNumerique(Re), ent(Entier(e)), mant(Entier(m)), nb_zero(n){if (mant.getEnt()==0) simplification();}
        Reel(Entier e, Entier m, int n = 0, bool neg = false): LitNumerique(Re), ent(e), mant(m), nb_zero(n), negatif(neg){if (mant.getEnt()==0) simplification();}
        const Entier  getEnt() const {return ent;}
        const Entier  getMant()const {return mant;}
        bool getNeg()const{return negatif;}
        void setNeg(bool neg){negatif=neg;}
        int getNbZero()const {return nb_zero;}
        virtual const Reel& operator+(const LitNumerique& r1) const;
        virtual const Reel& operator-(const LitNumerique& r1) const;
        virtual const Reel& operator*(const LitNumerique& r1) const;
        virtual const Reel& operator/(const LitNumerique& r1) const;
        virtual const Reel& NEG() const {
            Reel* p = new Reel(dynamic_cast<const Reel&>(*this).ent.NEG(),mant);
            return (*p);
            }
    //double const getValue()const {return ent.getValue()+((double) mant.getValue())*pow(10, -taille(mant.getValue()));}
        Entier simplification();

        bool operator<(const Reel& r) const {return ((ent<r.ent)||((ent==r.ent)&&(nb_zero>r.nb_zero))||((ent==r.ent)&&(nb_zero==r.nb_zero)&&(mant<r.mant)));}
        bool operator>(const Reel& r) const {return ((ent>r.ent)||((ent==r.ent)&&(nb_zero<r.nb_zero))||((ent==r.ent)&&(nb_zero==r.nb_zero)&&(mant>r.mant)));}
        bool operator==(const Reel& r) const {return ((ent==r.ent)&&(mant==r.mant)&&(nb_zero==r.nb_zero)&&(negatif==r.negatif));}
        bool operator!=(const Reel& r) const {return ((ent!=r.ent)||(mant!=r.mant)||(nb_zero!=r.nb_zero)||(negatif!=r.negatif));}
        bool operator<=(const Reel& r) const {return (*this<r)||(*this ==r);}
        bool operator>=(const Reel& r) const {return (*this>r)||(*this ==r);}

    private:
        Entier ent;
        Entier mant;
        int nb_zero; //nombre de 0 apres la virgule
        bool negatif; //si la partie reelle est nulle et le nombre negatif.
        enum_id id= Re;
};
ostream & operator<<(ostream& f, const Reel& r);
int taille(int n);
int puissance_dix(int n);
Reel r_cast(const Fraction& f);

/*-------------------Operateurs-numeriques---------------------*/

const Fraction operator/(const Entier& n, const Entier& d);

const Fraction operator+(const Entier& e, const Fraction& f);
const Fraction operator+(const Fraction& f, const Entier& e);
const Reel operator+(const Entier& e, const Reel& r);
const Reel operator+(const Reel& r, const Entier& e);
const Reel operator+(const Fraction& f, const Reel& r);
const Reel operator+(const Reel& r, const Fraction& f);

const Fraction operator-(const Entier& e, const Fraction& f);
const Fraction operator-(const Fraction& f, const Entier& e);
const Reel operator-(const Entier& e, const Reel& r);
const Reel operator-(const Reel& r, const Entier& e);
const Reel operator-(const Fraction& f, const Reel& r);
const Reel operator-(const Reel& r, const Fraction& f);

const Fraction operator*(const Entier& e, const Fraction& f);
const Fraction operator*(const Fraction& f, const Entier& e);
const Reel operator*(const Entier& e, const Reel& r);
const Reel operator*(const Reel& r, const Entier& e);
const Reel operator*(const Fraction& f, const Reel& r);
const Reel operator*(const Reel& r, const Fraction& f);

const Fraction operator/(const Entier& e, const Fraction& f);
const Fraction operator/(const Fraction& f, const Entier& e);
const Reel operator/(const Entier& e, const Reel& r);
const Reel operator/(const Reel& r, const Entier& e);
const Reel operator/(const Fraction& f, const Reel& r);
const Reel operator/(const Reel& r, const Fraction& f);

/*---------------------Operateurs-logiques-----------------------*/

bool operator<(const Entier& e, const Fraction& f);
bool operator<(const Fraction& f, const Entier& e);
bool operator<(const Entier& e, const Reel& r);
bool operator<(const Reel& r, const Entier& e);
bool operator<(const Fraction& f, const Reel& r);
bool operator<(const Reel& r, const Fraction& f);

bool operator>(const Entier& e, const Fraction& f);
bool operator>(const Fraction& f, const Entier& e);
bool operator>(const Entier& e, const Reel& r);
bool operator>(const Reel& r, const Entier& e);
bool operator>(const Fraction& f, const Reel& r);
bool operator>(const Reel& r, const Fraction& f);

bool operator==(const Entier& e, const Fraction& f);
bool operator==(const Fraction& f, const Entier& e);
bool operator==(const Entier& e, const Reel& r);
bool operator==(const Reel& r, const Entier& e);
bool operator==(const Fraction& f, const Reel& r);
bool operator==(const Reel& r, const Fraction& f);

bool operator!=(const Entier& e, const Fraction& f);
bool operator!=(const Fraction& f, const Entier& e);
bool operator!=(const Entier& e, const Reel& r);
bool operator!=(const Reel& r, const Entier& e);
bool operator!=(const Fraction& f, const Reel& r);
bool operator!=(const Reel& r, const Fraction& f);

bool operator>=(const Entier& e, const Fraction& f);
bool operator>=(const Fraction& f, const Entier& e);
bool operator>=(const Entier& e, const Reel& r);
bool operator>=(const Reel& r, const Entier& e);
bool operator>=(const Fraction& f, const Reel& r);
bool operator>=(const Reel& r, const Fraction& f);

bool operator<=(const Entier& e, const Fraction& f);
bool operator<=(const Fraction& f, const Entier& e);
bool operator<=(const Entier& e, const Reel& r);
bool operator<=(const Reel& r, const Entier& e);
bool operator<=(const Fraction& f, const Reel& r);
bool operator<=(const Reel& r, const Fraction& f);



#endif // LITNUM_H_INCLUDED
