#ifndef CONTROLLER_H_INCLUDED
#define CONTROLLER_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>


#include "item.h"
#include "pile.h"
#include "litterales.h"

using namespace std;

class Controller{
    ItemManager& ItMng;
    Pile& pile;
public:
    Controller(ItemManager& man, Pile& p):ItMng(man), pile(p){}
    void commande(const string& c);
    void execute(const string& l);
};

bool islowerall(const string s);

#endif // CONTROLLER_H_INCLUDED
