#ifndef LITTERALES_H_INCLUDED
#define LITTERALES_H_INCLUDED

#include "item.h"
#include "pile.h"

using namespace std;

class Expression: public Item
{
    string nom;
    enum_id id= Exp;
public:
    Expression(const string& n):Item(Exp), nom(n){};
    const string& getNom() const {return nom;}

};

class Atome: public Item{
string nom;
enum_id ss_id;
public:
    Atome(const string& n): Item(At), nom(n){
    char firstcar = n.at(0);
    if (firstcar<'A'|| firstcar>'Z')
        throw ComputException("la premiere lettre doit etre une majuscule ");
    }

    const string& getNom() const {return nom;}
    virtual enum_id GetSs_Id() const {return ss_id;}

};


class Variable: public Atome
{
    Item* it;
    enum_id ss_id = Var;
public:
   Variable(const string& s, Item& val): Atome(s), it(&val), ss_id(Var){}
    enum_id GetSs_Id() const {return ss_id;}
    Item* getItem() const {return it;}
};

class Programme: public Atome {

string instructions;
enum_id ss_id;

public:
    Programme(const string& n, const string& i): Atome(n), instructions(i), ss_id(Prog){}
    const string& GetInstructions() const {return instructions;}
    enum_id GetSs_Id() const {return ss_id;}

};

class Operateur: public Item{
string nom;
int arite;
enum_id id;
public:
    Operateur(int ar,const string& s):Item(Op),nom(s),arite(ar){}
    int getArite() const{return arite;}
    const string& getNom(){return nom;}

};

void OpAND();
void OpOR();
void OpNON();

class ItemManager {
    Item** it = nullptr;
    size_t nb = 0;
    size_t nbMax = 0;
    void agrandissementCapacite();
    ItemManager() = default;
    ~ItemManager();
    ItemManager(const ItemManager & m);
    ItemManager & operator=(const ItemManager & m);
public:
    static ItemManager& donneInstance();
    Item& addItem(Item* item);
    void removeItem(Item& e);
    void cleartab(){for (unsigned int i=0;i<nb;i++) delete it[i];nb=0;}// Ajout de nb=0 par SJD pour dire qu'il n'y a plus d'element
    void STO(Item& it, Expression& exp);
    void FORGET(Atome& a);
    friend void EVAL(ItemManager& ItMan, Expression& e);

    /*class Iterator{
            friend class ItemManager;
            Item** courant;
            Iterator(Item** iter):courant(iter){}
        public:
            enum_id operator*() const {
                if((*courant)->GetId()!= At)
                    return (*courant)->GetId();
                else
                    return dynamic_cast<Atome*>(*courant)->GetSs_Id();
                }

            Iterator& operator++(){++courant; return *this;}
            Iterator operator++(int){Iterator old=*this; ++courant; return old;}
            bool operator==(Iterator iter){return courant==iter.courant;}
            bool operator!=(Iterator iter){return courant!=iter.courant;}
   };

    Iterator begin(){return Iterator(it);}
    Iterator end(){return Iterator(it+nb);}
    */

  class Iterator{
          friend class ItemManager;
          Item** courant;
          Iterator(Item** iter):courant(iter){}
      public:
          Item* operator*() const {
                  return (*courant);
              }
          Iterator& operator++(){++courant; return *this;}
          Iterator operator++(int){Iterator old=*this; ++courant; return old;}
          bool operator==(Iterator iter){return courant==iter.courant;}
          bool operator!=(Iterator iter){return courant!=iter.courant;}
 };

  Iterator begin(){return Iterator(it);}
  Iterator end(){return Iterator(it+nb);}

};

#endif // LITTERALES_H_INCLUDED
