#ifndef DIALOGVAR_H
#define DIALOGVAR_H

#include <QDialog>

namespace Ui {
class DialogVar;
}

class DialogVar : public QDialog
{
    Q_OBJECT

public:
    explicit DialogVar(QWidget *parent = nullptr);
    ~DialogVar();

private:
    Ui::DialogVar *ui;
};

#endif // DIALOGVAR_H
