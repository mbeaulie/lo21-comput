#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "dialogtools.h"
#include "dialogprgm.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void setResultatAffiche(const bool res) { resultatAffiche = res;}
    bool getResultatAffiche() const { return resultatAffiche;}
    Item* stringToitem(QString texte);
    string itemToString (Item* item);

    // elements de cacul
public slots:

    void OptionsChanged();
    void prgmChanged();

private slots:

    void on_cacher_clavier_clicked();

    void on_touche_prgm_clicked();

    void on_touche_var_clicked();

    void on_touche_clear_clicked();

    void on_touche_back_clicked();

    void on_touche_drop_clicked();

    void on_touche_dup_clicked();

    void on_touche_clear_pile_clicked();

    void on_cacher_prgm_clicked();

    void on_touche_0_clicked();

    void on_touche_1_clicked();

    void on_touche_2_clicked();

    void on_touche_3_clicked();

    void on_touche_4_clicked();

    void on_touche_5_clicked();

    void on_touche_6_clicked();

    void on_touche_7_clicked();

    void on_touche_8_clicked();

    void on_touche_9_clicked();

    void on_touche_point_clicked();

    void on_cacher_var_clicked();

    void on_touche_neg_clicked();

    void on_touche_entree_clicked();

    void on_affichage_returnPressed();

    void on_touche_div_clicked();

    void on_touche_plus_clicked();

    void on_touche_moins_clicked();

    void on_touche_fois_clicked();

    void on_touche_divent_clicked();

    void on_touche_mod_clicked();

    void on_actionQuitter_triggered();

    void on_actionEditeur_triggered();

    void on_actionEditeur_2_triggered();

    void on_actionParam_tres_triggered();

    void on_touche_swap_clicked();

    void on_touche_ift_clicked();

    void on_affichage_textChanged(const QString &arg1);

    void on_touche_inferegal_clicked();

    void on_touche_inferieur_clicked();

    void on_touche_supegal_clicked();

    void on_touche_sup_clicked();

    void on_touche_egal_clicked();

    void on_touche_different_clicked();

    void on_listVariables_itemDoubleClicked(QListWidgetItem *item);

    void on_touche_and_clicked();

    void on_touche_or_clicked();

    void on_touche_non_clicked();

    void on_listPrgm_itemDoubleClicked(QListWidgetItem *item);

    void on_touche_eval_clicked();

private:

    void afficher_msg(const QString msg);
    void reset_affiche();
    void on_valeur_clicked(QString valeur);
    void affiche_resultat();

    Ui::MainWindow *ui;

    void affichePile();
    void affichePrgmVar();

    DialogTools* tools;
    DialogPrgm* prgmDialog;

    Pile* pile;

    ItemManager* itmMgr;

    bool resultatAffiche;

};
#endif // MAINWINDOW_H
