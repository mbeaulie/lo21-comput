#ifndef ITEM_H_INCLUDED
#define ITEM_H_INCLUDED

#include <iostream>
#include <string.h>
#include <vector>
#include <sstream>

//#include "litterales.h"

using namespace std;

enum enum_id {Ent,Frac,Re,Prog,Var,Op,Exp,At};

class ComputException{
string info;
public:
    ComputException(const string& s): info(s){}
    string GetInfo() const {return info;}
};


class Item{
enum_id id;
public:
    virtual int getValue() const {return 0;}
    Item(enum_id _id):id(_id){}
    virtual ~Item()=default;
    enum_id GetId() const {return id;}
    Item& operator=(const Item& it){id = it.GetId(); return *this;}
};

ostream & operator<<(ostream &f, Item &i);


#endif // ITEM_H_INCLUDED
