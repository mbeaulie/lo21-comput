#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED

#include <stdlib.h>
#include <iostream>
#include <stdio.h>


using namespace std;


class Entier{
private:
    int ent;

public:
    Entier(int i=0):ent(i){}
    const int getEnt() const {return ent;}
    Entier operator+(const Entier& e) {Entier i(ent+e.ent); return i;}
    Entier operator-(const Entier& e) {Entier i(ent-e.ent); return i;}
    Entier operator*(const Entier& e) {Entier i(ent*e.ent); return i;}
    Entier DIV(const Entier& e){Entier i(ent/e.ent); return i;} //Attention, pas des operator -> e.DIV(e1)
    Entier MOD(const Entier& e) {Entier i(ent%e.ent); return i;}
    Entier NEG(){Entier i(-ent); return i;}
    bool operator<(const Entier& e){return ent<e.ent;}
    bool operator>(const Entier& e){return ent>e.ent;}
    bool operator==(const Entier& e){return ent==e.ent;}
    bool operator!=(const Entier& e){return ent!=e.ent;}
    void operator++(){ent++;}
    void operator--(){ent--;}


};

ostream & operator<<(ostream &f, Entier &e);


class Fraction
{
    public:
        Fraction(Entier n, Entier d): numerateur(n),denominateur(d){simplification();};
        Entier const getNum(){return numerateur;}
        Entier const getDeno(){return denominateur;}
        void setFraction(Entier n, Entier d);
        Fraction operator+(Fraction& f1) {Fraction f(numerateur*f1.denominateur+f1.numerateur*denominateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator-(Fraction& f1) {Fraction f(numerateur*f1.denominateur-f1.numerateur*denominateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator*(Fraction& f1) {Fraction f(numerateur*f1.numerateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator/(Fraction& f1) {Fraction f(numerateur*f1.denominateur,denominateur*f1.numerateur);simplification();return f;};
        Fraction NEG() {Fraction f(numerateur.NEG(),denominateur);return f;};
        void simplification();
    private:
        Entier numerateur;
        Entier denominateur;

};

	Fraction operator/(Entier n, Entier d) {
		Fraction f(n, d); 
		return f;
		}

ostream & operator<<(ostream& f, Fraction& frac);


class Reel
{
    public:
        Reel(Entier e=0, Entier m=0): ent(e), mant(m){simplification();}
        Entier const getEnt(){return ent;}
        Entier const getMant(){return mant;}
        Reel const operator+(Reel& r1);
        Reel const operator-(Reel& r1);
        Reel const operator*(Reel& r1);
        Reel const operator/(Reel& r1);
        Reel NEG(){Reel r(ent.NEG(),mant);return r;}
        Entier simplification();
    private:
        Entier ent;
        Entier mant;
};

ostream & operator<<(ostream& f, Reel& r);


#endif // COMPUTER_H_INCLUDED
