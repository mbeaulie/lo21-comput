#include "dialogprgm.h"
#include "ui_dialogprgm.h"
#include "computer.h"

#include <iostream>
#include <QMessageBox>

/**
 * @brief DialogPrgm::DialogPrgm
 * @param parent
 *
 * initialisation de l'ui
 *
 */
DialogPrgm::DialogPrgm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogPrgm)
{
    ui->setupUi(this);

    // les saisies des programmes ne sont pas enabled

    setControles(false);

    ui->prgmList->setSelectionMode(QAbstractItemView::SingleSelection); // Un seul item sélectionné à la fois
}

DialogPrgm::~DialogPrgm()
{
    delete ui;
}
/**
 * @brief DialogPrgm::on_buttonBox_accepted
 * click sur le bouton OK
 *  on met à jour les tableau itemName et ItemText (attributs publics)
 */
void DialogPrgm::on_buttonBox_accepted()
{

    // sauvegarde des items dans le tableau programmes
    // les QList sont des attributs publics

    // std::cout << "on_buttonBox_accepted():" << ui->prgmList->count() << std::endl ;  fflush(stdout);

    // on vide les listes pour les remplir avec les nouvelles valeurs

    itemName.clear();
    itemText.clear();

    for(int i = 0; i < ui->prgmList->count(); ++i)
    {
        QListWidgetItem* item = ui->prgmList->item(i);
        itemName.append(item->data(0).toString());
        itemText.append(item->data(1).toString());
    }

    emit prgmOkPressed(); // Signal écouté par mainwindow
}

/**
 * @brief DialogPrgm::on_prgmList_itemDoubleClicked
 * @param item
 * On entre dans l'edition d'un item par le double click sur l'item
 */
void DialogPrgm::on_prgmList_itemDoubleClicked(QListWidgetItem *item)
{

    setControles(true);

    if (getFonction() == 'P')
        ui->prgmText->setText(item->data(1).toString());
    else
        ui->varText->setText(item->data(1).toString());

    ui->prgmNom->setText(item->data(0).toString());

    //std::cout << "ui->prgmList->selectedItems().count()=" << ui->prgmList->selectedItems().count() << std::endl; fflush(stdout);
}
/**
 * @brief DialogPrgm::on_pushButtonEnregistrer_clicked
 * sauvegarde de la saisie --> mise à jour du sélectionné ou création d'un nouveau
 */
void DialogPrgm::on_pushButtonEnregistrer_clicked()
{

    if ( ui->prgmList->selectedItems().count() > 0) // Si un seul est sélectionné
    {
        // A faire les vérifications de la saisie

        QListWidgetItem *item=ui->prgmList->selectedItems().at(0);
        // on met à jour les données
        item->setData(0,ui->prgmNom->text().toUpper());

        if (getFonction()=='P')
            item->setData(1,ui->prgmText->toPlainText());
        else
            item->setData(1,ui->varText->text());

    }
    else // si aucun n'est sélectionné --> on ajoute
    {
        // A faire les vérifications de la saisie

        // on crée un nouvel item dans la liste

        QListWidgetItem *item=new QListWidgetItem(ui->prgmNom->text());
        item->setData(0,ui->prgmNom->text().toUpper());

        if (getFonction()=='P')
            item->setData(1,ui->prgmText->toPlainText());
        else
            item->setData(1,ui->varText->text());

        ui->prgmList->addItem(item);
    }

    setControles(false);

    ui->prgmList->clearSelection();
}
/**
 * @brief DialogPrgm::on_pushButtonAnnuler_clicked
 */
void DialogPrgm::on_pushButtonAnnuler_clicked()
{
    setControles(false);
}
/**
 * @brief DialogPrgm::setControles
 * @param etat
 * controle de l'état des boutons
 * etat=true --> edition d'un élément
 * etat=false --> choix d'un élément
 * si les boutons d'édition sont disponibles, alors les boutons de sélection / création ne le sont pas et inversement
 */
void DialogPrgm::setControles(bool etat)
{


    // effacement du nom et du texte
    ui->prgmText->clear();
    ui->varText->clear();
    ui->prgmNom->clear();

    ui->prgmText->setReadOnly(! etat);
    ui->varText->setReadOnly(! etat);
    ui->prgmNom->setReadOnly(! etat);

    ui->pushButtonEnregistrer->setEnabled(etat);
    ui->pushButtonAnnuler->setEnabled(etat);
    ui->buttonBox->setEnabled(! etat);
    ui->prgmList->setEnabled(! etat);
    ui->pushButtonNouveau->setEnabled(! etat);
    ui->pushButtonSupprimer->setEnabled(! etat);
    ui->pushButtonModifier->setEnabled(! etat);
    ui->pushButtonDupliquer->setEnabled(! etat);

    if (etat)
    {
        ui->prgmNom->setFocus();
        ui->pushButtonNouveau->setDefault(true);
    }
    else
    {
        ui->prgmList->setFocus();
        ui->pushButtonEnregistrer->setDefault(true);
    }

}
/**
 * @brief DialogPrgm::on_pushButtonNouveau_clicked
 * Création d'un nouveau programme
 * on active les controles de saisie
 */
void DialogPrgm::on_pushButtonNouveau_clicked()
{
    ui->prgmList->clearSelection(); // on déselectionne tout

    setControles(true);

}
/**
 * @brief DialogPrgm::on_pushButtonSupprimer_clicked
 * l'item sélectionné est supprimé (pas de confirmation)
 */
void DialogPrgm::on_pushButtonSupprimer_clicked()
{
    if ( ui->prgmList->selectedItems().count() == 0 )
        return;

    QString msg;

    QListWidgetItem* item=ui->prgmList->selectedItems().at(0); // on prend le sélectionné (il ne peut y en avoir qu'un)

    msg="\nConfirmez-vous la suppression du programme ?\n" + item->data(0).toString();

    QMessageBox *confirmer = new QMessageBox(QMessageBox::Question,
                                        "Confirmation de la suppression",
                                        msg,
                                        QMessageBox::Yes | QMessageBox::No);

    confirmer->button(QMessageBox::Yes)->setText("Oui");
    confirmer->button(QMessageBox::No)->setText("Non");
    confirmer->setDefaultButton(QMessageBox::Yes);

    int n = confirmer->exec();
    delete confirmer;

    if (n == QMessageBox::No)
        return;

    ui->prgmList->takeItem( ui->prgmList->currentRow());

}
/**
 * @brief DialogPrgm::afficheListe
 * methode publique
 * affiche la liste des items à partir des attributs publics itemName[] et itemText[]
 * affiche les libellés en fonction de la fonction (V ou P)
 */
void DialogPrgm::initialiser()
{

    Pile& pile= pile.donneInstancePile();

    std::cout << "DialogPrgm::initialiser()-Nombre d'éléments dans la pile : " << pile.getTaille() << std::endl;


    for (int i=0 ; i < itemName.count() ; i++)
    {
        QListWidgetItem *item=new QListWidgetItem(itemName[i]);
        item->setText(QString::number(i));
        item->setData(0,itemName[i] );
        item->setData(1,itemText[i] );
        ui->prgmList->addItem(item);
    }

    if (getFonction()== 'V')
    {
        ui->lbl_content->setText("Valeur de la variable");
        ui->lbl_list->setText("Liste des variables");
        ui->lbl_name->setText("Nom de la variable");
        this->setWindowTitle("Gestion des variables");
        ui->prgmText->setInputMethodHints(Qt::ImhNone);
        ui->prgmNom->setInputMethodHints(Qt::ImhUppercaseOnly);
        ui->prgmText->setVisible(false);
        ui->varText->setVisible(true);
    }
    else if (getFonction() == 'P')
    {
        ui->lbl_content->setText("Contenu du programme");
        ui->lbl_list->setText("Liste des programmes");
        ui->lbl_name->setText("Nom du programme");
        this->setWindowTitle("Gestion des programmes");
        ui->prgmText->setInputMethodHints(Qt::ImhMultiLine);
        ui->prgmNom->setInputMethodHints(Qt::ImhUppercaseOnly);
        ui->prgmText->setVisible(true);
        ui->varText->setVisible(false);
    }
    else
    {
        ui->lbl_content->setText("Fonction inconnue");
        ui->lbl_list->setText("Fonction inconnue");
        ui->lbl_name->setText("Fonction inconnue");
        this->setWindowTitle("Fonction inconnue");
    }


}

void DialogPrgm::on_prgmList_itemSelectionChanged()
{
    if (ui->prgmList->selectedItems().count() == 0)
        return;

    QListWidgetItem *item=ui->prgmList->selectedItems().at(0);
    ui->prgmNom->setText(item->data(0).toString());
    if (getFonction() == 'P')
        ui->prgmText->setText(item->data(1).toString());
    else
        ui->varText->setText(item->data(1).toString());

}

void DialogPrgm::on_pushButtonModifier_clicked()
{
    if (ui->prgmList->selectedItems().count() == 0)
        return;
    QListWidgetItem *item=ui->prgmList->selectedItems().at(0);
    on_prgmList_itemDoubleClicked(item);
}

void DialogPrgm::on_pushButtonDupliquer_clicked()
{
    if (ui->prgmList->selectedItems().count() == 0)
        return;

    QListWidgetItem *item=ui->prgmList->selectedItems().at(0);

    QListWidgetItem *newitem = new QListWidgetItem;

    newitem->setData(0, "copie de " + item->data(0).toString());
    newitem->setData(1, item->data(0).toString());

    ui->prgmList->addItem(newitem);

    ui->prgmList->setCurrentItem(newitem);
}
