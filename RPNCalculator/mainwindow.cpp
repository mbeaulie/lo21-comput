#include <QChar>
#include <QString>
#include <QStringList>
#include <sstream>

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialogprgm.h"
#include "ui_dialogprgm.h"
#include "dialogtools.h"
#include "keyboardmgr.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    Pile& pile= pile.donneInstancePile();

    affichePile();

    pile.affiche();
    affichePile();
    // validation de l'affichage
    // un signe - ou rien et des chiffres de 0à9
    // ou un signe - ou rien et des chiffres de 0à9.0à9
    // ou un signe - ou rien et des chiffres 0à9/0à9
    // ou = ou < ou > ou >= ou <=
    QRegExp re("-?[0-9]*|-?[0-9]*/[0-9]*|-?[0-9]*[.][0-9]*|=|<|>|<=|>=");
    QRegExpValidator *validator = new QRegExpValidator(re, this);
    ui->affichage->setValidator(validator);

    // gestion des événements du clavier
    KeyboardMgr* key = new KeyboardMgr();
    ui->affichage->installEventFilter(key);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_cacher_clavier_clicked()
{
    if (ui->touche_0->isHidden())
    {
        ui->touche_0->show();
        ui->touche_1->show();
        ui->touche_2->show();
        ui->touche_3->show();
        ui->touche_4->show();
        ui->touche_5->show();
        ui->touche_6->show();
        ui->touche_7->show();
        ui->touche_8->show();
        ui->touche_9->show();
        ui->touche_plus->show();
        ui->touche_moins->show();
        ui->touche_fois->show();
        ui->touche_div->show();
        ui->touche_divent->show();
        ui->touche_mod->show();
        ui->touche_entree->show();
        ui->touche_point->show();
        ui->touche_neg->show();
        ui->touche_clear->show();
        ui->touche_back->show();
        ui->touche_and->show();
        ui->touche_or->show();
        ui->touche_not->show();
        ui->touche_sup->show();
        ui->touche_supegal->show();
        ui->touche_inferegal->show();
        ui->touche_inferieur->show();
        ui->touche_egal->show();
        ui->touche_different->show();
        ui->touche_eval->show();
        ui->cacher_clavier->setText("Cacher");
    }
    else
    {
        ui->touche_0->hide();
        ui->touche_1->hide();
        ui->touche_2->hide();
        ui->touche_3->hide();
        ui->touche_4->hide();
        ui->touche_5->hide();
        ui->touche_6->hide();
        ui->touche_7->hide();
        ui->touche_8->hide();
        ui->touche_9->hide();
        ui->touche_plus->hide();
        ui->touche_moins->hide();
        ui->touche_fois->hide();
        ui->touche_div->hide();
        ui->touche_divent->hide();
        ui->touche_mod->hide();
        ui->touche_entree->hide();
        ui->touche_point->hide();
        ui->touche_neg->hide();
        ui->touche_clear->hide();
        ui->touche_back->hide();
        ui->touche_and->hide();
        ui->touche_or->hide();
        ui->touche_not->hide();
        ui->touche_sup->hide();
        ui->touche_supegal->hide();
        ui->touche_inferegal->hide();
        ui->touche_inferieur->hide();
        ui->touche_egal->hide();
        ui->touche_different->hide();
        ui->touche_eval->hide();
        ui->cacher_clavier->setText("Montrer");
    }
}

/**
 * @brief MainWindow::on_touche_prgm_clicked
 */
void MainWindow::on_touche_prgm_clicked()
{

    Pile& pile= pile.donneInstancePile();

    std::cout << "DialogPrgm::initialiser()-Nombre d'éléments dans la pile : " << pile.getTaille() << std::endl;

    // Affiche la fenêtre de gestion des programmes

    prgmDialog = new DialogPrgm;

    prgmDialog->setFonction('P'); // on gère les programmes

    // ici remplir la liste des programmes de prgmDialog
    // les attributs prgmDialog->itemName et prgmDialog->itemText sont publiques

    ItemManager& itmMgr= itmMgr.donneInstance();

    stringstream res;

    for( ItemManager::Iterator it = itmMgr.begin(); it != itmMgr.end() ; ++it)
    {
        res.clear();
        if ( (*it)->GetId() == At ) // je ne traite que les atomes
        {
            if (dynamic_cast<Atome*>(*it)->GetSs_Id() == Prog) // si l'atome est un programme
            {
                std::cout << "dynamic_cast<Programme*>(*it)->getNom())=" << dynamic_cast<Programme*>(*it)->getNom() << std::endl; fflush (stdout);
                prgmDialog->itemName.append(QString::fromStdString(dynamic_cast<Programme*>(*it)->getNom()));
                std::cout << "dynamic_cast<Programme*>(*it)->GetInstructions())=" << dynamic_cast<Programme*>(*it)->GetInstructions() << std::endl; fflush (stdout);
                prgmDialog->itemText.append(QString::fromStdString(dynamic_cast<Programme*>(*it)->GetInstructions()));
            }
        }
    }

    prgmDialog->initialiser();

    // ajout de la gestion du signal
    QObject::connect(prgmDialog, SIGNAL(prgmOkPressed()), this, SLOT(prgmChanged()));  // C'est le slot OptionsChanged qui sera appelé

    // On affiche la fenêtre

    prgmDialog->show();

}
/**
 * @brief MainWindow::on_touche_var_clicked
 */
void MainWindow::on_touche_var_clicked()
{

    // Affiche la fenêtre de gestion des programmes / variables

    prgmDialog = new DialogPrgm;

    prgmDialog->setFonction('V'); // on gère les variables

    // ici remplir la liste des variables de prgmDialog
    // les attributs prgmDialog->itemName et prgmDialog->itemText sont publiques

    ItemManager& itmMgr= itmMgr.donneInstance();

    stringstream res;

    for( ItemManager::Iterator it = itmMgr.begin(); it != itmMgr.end() ; ++it)
    {

        if ( (*it)->GetId() == At ) // je ne traite que les atomes
        {
            if (dynamic_cast<Atome*>(*it)->GetSs_Id() == Var) // si l'atome est une variable
            {
                res.clear();

                prgmDialog->itemName.append(QString::fromStdString(dynamic_cast<Variable*>(*it)->getNom()));

                int id=dynamic_cast<Variable*>(*it)->getItem()->GetId();

                switch ( id )
                {
                    // Ent,Frac,Re,Prog,Var,Op,Exp,At
                    case Ent:
                    {
                        Entier* e=dynamic_cast<Entier*>(dynamic_cast<Variable*>(*it)->getItem());
                        res << e->getEnt();
                        break;
                    }
                    case Frac:
                    {
                        Fraction* f=dynamic_cast<Fraction*>(dynamic_cast<Variable*>(*it)->getItem());
                        res << f->getNum() << "/" << f->getDeno();
                        break;
                    }
                    case Re:
                    {
                        Reel* r=dynamic_cast<Reel*>(dynamic_cast<Variable*>(*it)->getItem());
                        Entier nul(0);
                        if((r->getEnt()==nul)&&(r->getNeg()))
                            res <<"-";
                        res << r->getEnt() << "." ;
                        for(int i =0; i < r->getNbZero(); i++)
                           res << "0";
                        res << r->getMant();
                        break;
                    }
                    default:
                    {
                        res << "N/A";
                        break;
                    }
                }
                prgmDialog->itemText.append(QString::fromStdString(res.str()));
            }
        }
    }

    prgmDialog->initialiser();

    // ajout de la gestion du signal
    QObject::connect(prgmDialog, SIGNAL(prgmOkPressed()), this, SLOT(prgmChanged()));  // C'est le slot OptionsChanged qui sera appelé

    // On affiche la fenêtre

    prgmDialog->show();
}
/**
 * @brief MainWindow::prgmChanged
 * slot appelé à la fermeture de la fenêtre prgmDialog
 **/
void MainWindow::prgmChanged()
{
   //std::cout << "PrgmChanged() - " << std::endl ;fflush(stdout);

   ItemManager& itmMgr = itmMgr.donneInstance();

   // Récupérer les programmes et les variables pour les stocker

   for(int i = 0; i < prgmDialog->itemName.count(); ++i)
   {
       //std::cout << "prgmChanged(), itemName(" << i << ")=" << prgmDialog->itemName[i].toStdString() << std::endl ;
       //std::cout << "prgmChanged(), itemText(" << i << ")=" << prgmDialog->itemText[i].toStdString() << std::endl ;

       if (prgmDialog->getFonction() == 'P')
       {
           Programme* p = new Programme(prgmDialog->itemName[i].toStdString(), prgmDialog->itemText[i].toStdString() );
           Expression* exp = new Expression(prgmDialog->itemName[i].toStdString());
           itmMgr.STO(dynamic_cast<Item&>(*p), dynamic_cast<Expression&>(*exp));
       }
       else
       {
           Item* it = stringToitem(prgmDialog->itemText[i]);
           std::cout << "MainWindow::prgmChanged()-it.getid()=" << it->GetId() << std::endl ; fflush(stdout);
           Expression* exp = new Expression(prgmDialog->itemName[i].toStdString());
           itmMgr.STO(dynamic_cast<Item&>(*it), dynamic_cast<Expression&>(*exp));
       }

   }

   delete prgmDialog;

   reset_affiche();

}

void MainWindow::afficher_msg(const QString msg)
{
    ui->msg_util->setText(msg);
}

void MainWindow::on_touche_clear_clicked()
{
    ui->affichage->clear();
}

void MainWindow::on_touche_back_clicked()
{
    ui->affichage->backspace();
}

void MainWindow::on_touche_drop_clicked()
{
    Pile& pile= pile.donneInstancePile();
    //printf ("avant drop pointeur sur pile : %p, taille=%d\n", pile, pile.getTaille()); fflush(stdout);
    pile.DROP();
    //printf ("après drop pointeur sur pile : %p, taille=%d\n", pile, pile.getTaille()); fflush(stdout);
    reset_affiche();
}
void MainWindow::affichePile()
{
    Pile& pile= pile.donneInstancePile();
    ui->EtatPile->setText(QString::fromStdString(pile.r_affiche()));
    afficher_msg(QString::fromStdString(pile.getMessage()));
}

void MainWindow::affichePrgmVar()
{
    // on vide les listes

    ui->listPrgm->clear();
    ui->listVariables->clear();

    ItemManager& itmMgr= itmMgr.donneInstance();

    // on itère dans itemManager pour afficher les éléments

    for( ItemManager::Iterator it = itmMgr.begin(); it != itmMgr.end() ; ++it)
    {
        //std::cout << "MainWindow::affichePrgmVar() *it " << *it << endl;
        //std::cout << "MainWindow::affichePrgmVar() *it-getId() " << dynamic_cast<Item*>(*it)->GetId() << endl;

        if ( (*it)->GetId() == At ) // je ne traite que les atomes
        {
            //std::cout << "MainWindow::affichePrgmVar() *it-getSs_Id() " << dynamic_cast<Atome*>(*it)->GetSs_Id() << endl;

            if (dynamic_cast<Atome*>(*it)->GetSs_Id() == Prog) // si l'atome est un programme
            {
                QListWidgetItem* item = new QListWidgetItem;
                //std::cout << "MainWindow::affichePrgmVar() (*it)->getNom() " << dynamic_cast<Programme*>(*it)->getNom() << endl;
                item->setData(0, QString::fromStdString(dynamic_cast<Programme*>(*it)->getNom()));
                ui->listPrgm->addItem(item);
            }
            else if (dynamic_cast<Atome*>(*it)->GetSs_Id() == Var) // si l'atome est une variable
            {
                QListWidgetItem* item = new QListWidgetItem;
                //std::cout << "MainWindow::affichePrgmVar() (*it)->getNom() " << dynamic_cast<Variable*>(*it)->getNom() << endl; fflush(stdout);
                item->setData(0, QString::fromStdString(dynamic_cast<Variable*>(*it)->getNom()));

                stringstream res;

                int id=dynamic_cast<Variable*>(*it)->getItem()->GetId();
                std::cout << "MainWindow::affichePrgmVar() id=" << id << endl; fflush(stdout);

                switch ( id )
                {
                    // Ent,Frac,Re,Prog,Var,Op,Exp,At
                    case Ent:
                    {
                        Entier* e=dynamic_cast<Entier*>(dynamic_cast<Variable*>(*it)->getItem());
                        res << e->getEnt();
                        break;
                    }
                    case Frac:
                    {
                        Fraction* f=dynamic_cast<Fraction*>(dynamic_cast<Variable*>(*it)->getItem());
                        res << f->getNum() << "/" << f->getDeno();
                        break;
                    }
                    case Re:
                    {
                        Reel* r=dynamic_cast<Reel*>(dynamic_cast<Variable*>(*it)->getItem());
                        res << r->getEnt() << "." ;
                        for(int i =0; i < r->getNbZero(); i++)
                           res << "0";
                        res << r->getMant();
                        break;
                    }
                    default:
                    {
                        res << "N/A";
                        break;
                    }
                }
                item->setData(1, QString::fromStdString(res.str()));

                ui->listVariables->addItem(item);
            }
        }
    }
}


void MainWindow::on_touche_dup_clicked()
{
    Pile& pile= pile.donneInstancePile();
    pile.DUP();
    reset_affiche();
}

void MainWindow::on_touche_clear_pile_clicked()
{
    Pile& pile= pile.donneInstancePile();
    pile.CLEAR();
    reset_affiche();
}

void MainWindow::on_cacher_prgm_clicked()
{
    //std::cout << "on_cacher_prgm_clicked()" << "-" << "ui->listPrgm->isHidden()=" << ui->listPrgm->isHidden() << std::endl;
    if (ui->listPrgm->isHidden())
    {
        ui->cacher_prgm->setText("Cacher");
        ui->listPrgm->show();
    }
    else
    {
        ui->cacher_prgm->setText("Montrer");
        ui->listPrgm->hide();
    }
}

void MainWindow::on_valeur_clicked(QString valeur)
{
    if (resultatAffiche){
            ui->affichage->clear();
            setResultatAffiche(false);}

    ui->affichage->setText(ui->affichage->text() + valeur );
}

void MainWindow::on_touche_0_clicked()
{
    on_valeur_clicked("0");
}

void MainWindow::on_touche_1_clicked()
{
    on_valeur_clicked("1");
}

void MainWindow::on_touche_2_clicked()
{
    on_valeur_clicked("2");
}

void MainWindow::on_touche_3_clicked()
{
    on_valeur_clicked("3");
}

void MainWindow::on_touche_4_clicked()
{
    on_valeur_clicked("4");
}

void MainWindow::on_touche_5_clicked()
{
    on_valeur_clicked("5");
}

void MainWindow::on_touche_6_clicked()
{
    on_valeur_clicked("6");
}

void MainWindow::on_touche_7_clicked()
{
    on_valeur_clicked("7");
}

void MainWindow::on_touche_8_clicked()
{
    on_valeur_clicked("8");
}

void MainWindow::on_touche_9_clicked()
{
    on_valeur_clicked("9");
}

void MainWindow::on_touche_point_clicked()
{
    if (!(ui->affichage->text().contains("."))) //s'il n'y a pas encore de point
        on_valeur_clicked(".");
}

void MainWindow::on_cacher_var_clicked()
{
    //std::cout << "on_cacher_var_clicked()" << "-" << "ui->listVariables->isHidden()=" << ui->listVariables->isHidden() << std::endl;
    if (ui->listVariables->isHidden())
    {
        ui->cacher_var->setText("Cacher");
        ui->listVariables->show();
    }
    else
    {
        ui->cacher_var->setText("Montrer");
        ui->listVariables->hide();
    }
}

void MainWindow::on_touche_neg_clicked()
{
    if (ui->affichage->text().length()==0)
        return;
    if (ui->affichage->text().contains("-")) //s'il y a déjà un moins
         ui->affichage->setText(ui->affichage->text().right(ui->affichage->text().length()-1));
    else
        ui->affichage->setText("-"+ui->affichage->text());
}

void MainWindow::on_touche_entree_clicked()
{
    // si rien dans la zone affichage, on sort
    if (ui->affichage->text().isEmpty())
    {
        return;
    }
    Pile& pile= pile.donneInstancePile();

    if (ui->affichage->text() == "=") // opérateur =
    {
        on_touche_egal_clicked();
        return;
    }
    else if (ui->affichage->text() == ">") // opérateur >
    {
        on_touche_sup_clicked();
        return;
    }
    else if (ui->affichage->text() == "<") // opérateur >
    {
        on_touche_inferieur_clicked();
        return;
    }
    else if (ui->affichage->text() == ">=") // opérateur >
    {
        on_touche_supegal_clicked();
        return;
    }

    else if (ui->affichage->text() == "<=") // opérateur >
    {
        on_touche_inferegal_clicked();
        return;
    }
    else
    {
        pile.PUSH(dynamic_cast<Item*>(stringToitem(ui->affichage->text())));
    }
    reset_affiche();
}

void MainWindow::on_affichage_returnPressed()
{
    on_touche_entree_clicked();
}

void MainWindow::on_touche_div_clicked()
{   Pile& pile= pile.donneInstancePile();
    if (ui->affichage->text().length()==0)
        //on empile l'opérateur
    {
        pile.CALCUL("/");
        reset_affiche();
        return;
    }
    if (!(ui->affichage->text().contains("/")))
    {
        on_valeur_clicked("/");
    }
}

void MainWindow::on_touche_plus_clicked()
{   Pile& pile= pile.donneInstancePile();
    pile.CALCUL("+");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_moins_clicked()
{   Pile& pile= pile.donneInstancePile();
    pile.CALCUL("-");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_fois_clicked()
{   Pile& pile= pile.donneInstancePile();
    pile.CALCUL("*");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_divent_clicked()
{   Pile& pile= pile.donneInstancePile();
    pile.CALCUL("DIV");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_mod_clicked()
{   Pile& pile= pile.donneInstancePile();
    pile.CALCUL("MOD");
    reset_affiche();
    affiche_resultat();
}

//Opérateurs logiques

void MainWindow::on_touche_inferegal_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS("<=");;
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_inferieur_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS("<");;
    reset_affiche();
    affiche_resultat();
}


void MainWindow::on_actionQuitter_triggered()
{
    exit(0);
}

void MainWindow::on_actionEditeur_triggered()
{
    on_touche_prgm_clicked();
}

void MainWindow::on_actionEditeur_2_triggered()
{
    on_touche_var_clicked();
}

void MainWindow::on_actionParam_tres_triggered()
{   Pile& pile= pile.donneInstancePile();
    tools = new DialogTools();

    tools->setWindowTitle("Options");
    QObject::connect(tools, SIGNAL(OkPressed()), this, SLOT(OptionsChanged()));  // C'est le slot OptionsChanged qui sera appelé
    tools->setNbAffiche(pile.getNbAffiche());
    tools->show();
    pile.setNbItemsToAffiche(tools->getNbAffiche());
}

void MainWindow::OptionsChanged()
{   Pile& pile= pile.donneInstancePile();
//   cout<<"test "<<tools->getNbAffiche();
//   fflush(stdout);
   pile.setNbItemsToAffiche(tools->getNbAffiche());
   delete tools;
}

void MainWindow::reset_affiche()
{
    ui->affichage->clear();
    affichePile();
    affichePrgmVar();
    setResultatAffiche(false);
    ui->affichage->setFocus();
}

void MainWindow::affiche_resultat()
{

    // std::cout << "affiche_resultat()" << std::endl ; fflush(stdout);

    setResultatAffiche(true);

    Pile& pile=pile.donneInstancePile();

    try {

        Item* it=pile.TOP();

        //std::cout << "affiche_resultat()id->GetId" << it->GetId() << std::endl ;
        //fflush(stdout);

        int id=it->GetId();

        QString res;

        switch (id)
        {
            case Ent:
              res = QString::number(dynamic_cast<Entier*>(it)->getEnt());
              break;
            case Frac:
              res = QString::number(dynamic_cast<Fraction*>(it)->getNum().getEnt()) + "/" + QString::number(dynamic_cast<Fraction*>(it)->getDeno().getEnt());
              break;
            case Re:{
               Entier nul(0);
               if((dynamic_cast<Reel*>(it)->getEnt()==nul)&&(dynamic_cast<Reel*>(it)->getNeg()))
                   res += "-";
               res += QString::number(dynamic_cast<Reel*>(it)->getEnt().getEnt()) + "." ;
               for(int i =0; i<dynamic_cast<Reel*>(it)->getNbZero(); i++)
                   res += "0";
               res += QString::number(dynamic_cast<Reel*>(it)->getMant().getEnt());
               break;}
            default:
              res = "";
              break;
        }
        ui->affichage->setText(res);

    }  catch (...) {
        return;
    }

}


void MainWindow::on_touche_swap_clicked()
{
    try {
        Pile& pile=pile.donneInstancePile();
        pile.SWAP();

    }  catch (...) //
    {

    }

    affichePile();

}

void MainWindow::on_touche_ift_clicked()
{
    Pile& pile=pile.donneInstancePile();
    pile.IFT();
    affichePile();
}
/**
 * @brief MainWindow::on_affichage_textChanged
 * Gestion des contenus saisis --> keyboardmanager remplit le champ en fonction de la touche saisie
 * @param arg1 le contenu du controle affichage
 */
void MainWindow::on_affichage_textChanged(const QString &arg1)  // SLOT appelé par KeyboardManager
{
    if (arg1 == "+")
    {
        ui->affichage->clear();
        on_touche_plus_clicked();
    }
    else if ( arg1 == "/")
    {
        ui->affichage->clear();
        on_touche_div_clicked();
    }
    else if ( arg1 == "*")
    {
        ui->affichage->clear();
        on_touche_fois_clicked();
    }
    else if ( arg1 == "-")
    {
        ui->affichage->clear();
        on_touche_moins_clicked();
    }
    else if ( arg1 == "<")
    {
        ui->affichage->clear();
        on_touche_inferieur_clicked();
    }
    else if ( arg1 == ">")
    {
        ui->affichage->clear();
        on_touche_sup_clicked();
    }
    else if ( arg1 == "=")
    {
        ui->affichage->clear();
        on_touche_egal_clicked();
    }
    else if ( arg1 == ">=")
    {
        ui->affichage->clear();
        on_touche_supegal_clicked();
    }
    else if ( arg1 == "<=")
    {
        ui->affichage->clear();
        on_touche_inferegal_clicked();
    }
    else if (getResultatAffiche())
    {
        setResultatAffiche(false);
    }
}

void MainWindow::on_touche_supegal_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS(">=");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_sup_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS(">");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_egal_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS("=");
    reset_affiche();
    affiche_resultat();
}

void MainWindow::on_touche_different_clicked()
{
    Pile& pile=Pile::donneInstancePile();
    pile.TESTS("!=");
    reset_affiche();
    affiche_resultat();
}
/**
 * @brief MainWindow::stringToitem
 * @param texte : un QString à analyser
 * @return un item
 */
Item* MainWindow::stringToitem(QString texte)
{
    if (texte.contains("."))  // c'est un réel
    {
        //On convertit les parties gauches et droites en entier
        //On crée notre objet Réel
        bool neg = false;
        Entier nul(0);
        QStringList liste=texte.split(".");
        Entier pe(liste[0].toInt());
        QString temp = liste[1];
        int nb_zero=0;
        int it = 0;
        while(temp.at(it)==QChar('0')){
            nb_zero++;
            it++;
        }
        Entier pm(liste[1].toInt());
        if((pe==nul) && (texte[0]=="-"))
            neg = true;

        Reel* r=new Reel(pe, pm, nb_zero, neg);
        return (dynamic_cast<Item*>(r));
    }
    else if (texte.contains("/"))  // c'est une fraction
    {
        //On convertit les parties gauches et droites en entier
        //On crée notre objet Fraction
        QStringList liste=texte.split("/");
        Entier pn(liste[0].toInt());
        Entier pd(liste[1].toInt());
        Fraction* f=new Fraction(pn,pd);
        return (dynamic_cast<Item*>(f));
    }
    else
    {
        int i(texte.toInt()); // c'est une entier
        Entier* e=new Entier(i);
        return (dynamic_cast<Item*>(e));
    }
}

/**
 * @brief MainWindow::on_listVariables_itemDoubleClicked
 * @param item
 * met le contenu de item.data(1) dans l'affichage et clique sur la touche entrée pour empiler
 */
void MainWindow::on_listVariables_itemDoubleClicked(QListWidgetItem *item)
{

    ui->affichage->setText(item->data(1).toString());

    on_touche_entree_clicked();

}
