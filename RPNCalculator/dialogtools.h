#ifndef DIALOGTOOLS_H
#define DIALOGTOOLS_H

#include <QDialog>

namespace Ui {
class DialogTools;
}

class DialogTools : public QDialog
{
    Q_OBJECT

public:
    Ui::DialogTools *ui; // l'UI est publique pour que MainWindow récupère les éléments
    explicit DialogTools(QWidget *parent = nullptr);
    DialogTools(int nbaffiche):nbAffiche(nbaffiche){};
    void setNbAffiche(int nb);
    int getNbAffiche() const {return nbAffiche;};
    ~DialogTools();

signals:
    void OkPressed();

private slots:
    void on_SpinNbAffiche_valueChanged(int arg1);

    void on_buttonBox_accepted();

private:

    int nbAffiche;
};

#endif // DIALOGTOOLS_H
