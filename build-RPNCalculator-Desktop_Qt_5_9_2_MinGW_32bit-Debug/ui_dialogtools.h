/********************************************************************************
** Form generated from reading UI file 'dialogtools.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGTOOLS_H
#define UI_DIALOGTOOLS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DialogTools
{
public:
    QDialogButtonBox *buttonBox;
    QWidget *formLayoutWidget;
    QFormLayout *formLayout;
    QLabel *label;
    QSpinBox *SpinNbAffiche;

    void setupUi(QDialog *DialogTools)
    {
        if (DialogTools->objectName().isEmpty())
            DialogTools->setObjectName(QStringLiteral("DialogTools"));
        DialogTools->resize(453, 359);
        buttonBox = new QDialogButtonBox(DialogTools);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setGeometry(QRect(90, 320, 341, 32));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);
        formLayoutWidget = new QWidget(DialogTools);
        formLayoutWidget->setObjectName(QStringLiteral("formLayoutWidget"));
        formLayoutWidget->setGeometry(QRect(9, 29, 421, 281));
        formLayout = new QFormLayout(formLayoutWidget);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(formLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        SpinNbAffiche = new QSpinBox(formLayoutWidget);
        SpinNbAffiche->setObjectName(QStringLiteral("SpinNbAffiche"));

        formLayout->setWidget(0, QFormLayout::FieldRole, SpinNbAffiche);


        retranslateUi(DialogTools);
        QObject::connect(buttonBox, SIGNAL(accepted()), DialogTools, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), DialogTools, SLOT(reject()));

        QMetaObject::connectSlotsByName(DialogTools);
    } // setupUi

    void retranslateUi(QDialog *DialogTools)
    {
        DialogTools->setWindowTitle(QApplication::translate("DialogTools", "Dialog", Q_NULLPTR));
        label->setText(QApplication::translate("DialogTools", "Nombre d'\303\251l\303\251ments \303\240 afficher dans la pile", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DialogTools: public Ui_DialogTools {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGTOOLS_H
