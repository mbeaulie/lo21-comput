/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionQuitter;
    QAction *actionEditeur;
    QAction *actionEditeur_2;
    QAction *actionParam_tres;
    QWidget *centralwidget;
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QPushButton *touche_inferieur;
    QListWidget *listPrgm;
    QLineEdit *affichage;
    QPushButton *touche_sup;
    QPushButton *cacher_var;
    QPushButton *touche_prgm;
    QPushButton *touche_mod;
    QPushButton *touche_entree;
    QPushButton *touche_6;
    QPushButton *touche_back;
    QPushButton *touche_2;
    QPushButton *touche_clear;
    QPushButton *touche_fois;
    QPushButton *cacher_prgm;
    QPushButton *touche_divent;
    QPushButton *touche_moins;
    QPushButton *touche_point;
    QPushButton *touche_egal;
    QPushButton *touche_9;
    QPushButton *touche_div;
    QPushButton *touche_plus;
    QPushButton *touche_3;
    QPushButton *touche_var;
    QPushButton *touche_4;
    QPushButton *touche_neg;
    QPushButton *touche_0;
    QPushButton *touche_inferegal;
    QPushButton *touche_8;
    QLabel *label;
    QPushButton *cacher_clavier;
    QPushButton *touche_eval;
    QListWidget *listVariables;
    QPushButton *touche_supegal;
    QPushButton *touche_7;
    QPushButton *touche_5;
    QPushButton *touche_1;
    QPushButton *touche_dup;
    QPushButton *touche_drop;
    QPushButton *touche_swap;
    QPushButton *touche_ift;
    QPushButton *touche_clear_pile;
    QTextEdit *EtatPile;
    QLineEdit *msg_util;
    QPushButton *touche_not;
    QPushButton *touche_or;
    QPushButton *touche_and;
    QPushButton *touche_different;
    QMenuBar *menubar;
    QMenu *menu_Fichier;
    QMenu *menu_Programmes;
    QMenu *menu_Variables;
    QMenu *menu_Outils;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->setEnabled(true);
        MainWindow->resize(1346, 600);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(11);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        MainWindow->setFont(font);
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QStringLiteral("actionQuitter"));
        actionEditeur = new QAction(MainWindow);
        actionEditeur->setObjectName(QStringLiteral("actionEditeur"));
        actionEditeur_2 = new QAction(MainWindow);
        actionEditeur_2->setObjectName(QStringLiteral("actionEditeur_2"));
        actionParam_tres = new QAction(MainWindow);
        actionParam_tres->setObjectName(QStringLiteral("actionParam_tres"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        gridLayoutWidget = new QWidget(centralwidget);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(10, 20, 1311, 481));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setHorizontalSpacing(4);
        gridLayout->setVerticalSpacing(6);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        touche_inferieur = new QPushButton(gridLayoutWidget);
        touche_inferieur->setObjectName(QStringLiteral("touche_inferieur"));

        gridLayout->addWidget(touche_inferieur, 6, 2, 1, 1);

        listPrgm = new QListWidget(gridLayoutWidget);
        listPrgm->setObjectName(QStringLiteral("listPrgm"));

        gridLayout->addWidget(listPrgm, 1, 12, 6, 2);

        affichage = new QLineEdit(gridLayoutWidget);
        affichage->setObjectName(QStringLiteral("affichage"));
        affichage->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        gridLayout->addWidget(affichage, 0, 1, 1, 4);

        touche_sup = new QPushButton(gridLayoutWidget);
        touche_sup->setObjectName(QStringLiteral("touche_sup"));

        gridLayout->addWidget(touche_sup, 6, 4, 1, 1);

        cacher_var = new QPushButton(gridLayoutWidget);
        cacher_var->setObjectName(QStringLiteral("cacher_var"));

        gridLayout->addWidget(cacher_var, 0, 15, 1, 1);

        touche_prgm = new QPushButton(gridLayoutWidget);
        touche_prgm->setObjectName(QStringLiteral("touche_prgm"));

        gridLayout->addWidget(touche_prgm, 0, 12, 1, 1);

        touche_mod = new QPushButton(gridLayoutWidget);
        touche_mod->setObjectName(QStringLiteral("touche_mod"));

        gridLayout->addWidget(touche_mod, 4, 5, 1, 1);

        touche_entree = new QPushButton(gridLayoutWidget);
        touche_entree->setObjectName(QStringLiteral("touche_entree"));

        gridLayout->addWidget(touche_entree, 5, 5, 1, 1);

        touche_6 = new QPushButton(gridLayoutWidget);
        touche_6->setObjectName(QStringLiteral("touche_6"));

        gridLayout->addWidget(touche_6, 3, 3, 1, 1);

        touche_back = new QPushButton(gridLayoutWidget);
        touche_back->setObjectName(QStringLiteral("touche_back"));

        gridLayout->addWidget(touche_back, 1, 1, 1, 2);

        touche_2 = new QPushButton(gridLayoutWidget);
        touche_2->setObjectName(QStringLiteral("touche_2"));

        gridLayout->addWidget(touche_2, 4, 2, 1, 1);

        touche_clear = new QPushButton(gridLayoutWidget);
        touche_clear->setObjectName(QStringLiteral("touche_clear"));

        gridLayout->addWidget(touche_clear, 1, 3, 1, 2);

        touche_fois = new QPushButton(gridLayoutWidget);
        touche_fois->setObjectName(QStringLiteral("touche_fois"));

        gridLayout->addWidget(touche_fois, 3, 4, 1, 1);

        cacher_prgm = new QPushButton(gridLayoutWidget);
        cacher_prgm->setObjectName(QStringLiteral("cacher_prgm"));

        gridLayout->addWidget(cacher_prgm, 0, 13, 1, 1);

        touche_divent = new QPushButton(gridLayoutWidget);
        touche_divent->setObjectName(QStringLiteral("touche_divent"));

        gridLayout->addWidget(touche_divent, 3, 5, 1, 1);

        touche_moins = new QPushButton(gridLayoutWidget);
        touche_moins->setObjectName(QStringLiteral("touche_moins"));

        gridLayout->addWidget(touche_moins, 4, 4, 1, 1);

        touche_point = new QPushButton(gridLayoutWidget);
        touche_point->setObjectName(QStringLiteral("touche_point"));

        gridLayout->addWidget(touche_point, 5, 3, 1, 1);

        touche_egal = new QPushButton(gridLayoutWidget);
        touche_egal->setObjectName(QStringLiteral("touche_egal"));

        gridLayout->addWidget(touche_egal, 6, 5, 1, 1);

        touche_9 = new QPushButton(gridLayoutWidget);
        touche_9->setObjectName(QStringLiteral("touche_9"));

        gridLayout->addWidget(touche_9, 2, 3, 1, 1);

        touche_div = new QPushButton(gridLayoutWidget);
        touche_div->setObjectName(QStringLiteral("touche_div"));

        gridLayout->addWidget(touche_div, 2, 4, 1, 1);

        touche_plus = new QPushButton(gridLayoutWidget);
        touche_plus->setObjectName(QStringLiteral("touche_plus"));

        gridLayout->addWidget(touche_plus, 5, 4, 1, 1);

        touche_3 = new QPushButton(gridLayoutWidget);
        touche_3->setObjectName(QStringLiteral("touche_3"));

        gridLayout->addWidget(touche_3, 4, 3, 1, 1);

        touche_var = new QPushButton(gridLayoutWidget);
        touche_var->setObjectName(QStringLiteral("touche_var"));

        gridLayout->addWidget(touche_var, 0, 14, 1, 1);

        touche_4 = new QPushButton(gridLayoutWidget);
        touche_4->setObjectName(QStringLiteral("touche_4"));

        gridLayout->addWidget(touche_4, 3, 1, 1, 1);

        touche_neg = new QPushButton(gridLayoutWidget);
        touche_neg->setObjectName(QStringLiteral("touche_neg"));

        gridLayout->addWidget(touche_neg, 5, 1, 1, 1);

        touche_0 = new QPushButton(gridLayoutWidget);
        touche_0->setObjectName(QStringLiteral("touche_0"));

        gridLayout->addWidget(touche_0, 5, 2, 1, 1);

        touche_inferegal = new QPushButton(gridLayoutWidget);
        touche_inferegal->setObjectName(QStringLiteral("touche_inferegal"));

        gridLayout->addWidget(touche_inferegal, 6, 1, 1, 1);

        touche_8 = new QPushButton(gridLayoutWidget);
        touche_8->setObjectName(QStringLiteral("touche_8"));

        gridLayout->addWidget(touche_8, 2, 2, 1, 1);

        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setAlignment(Qt::AlignCenter);

        gridLayout->addWidget(label, 0, 6, 1, 5);

        cacher_clavier = new QPushButton(gridLayoutWidget);
        cacher_clavier->setObjectName(QStringLiteral("cacher_clavier"));

        gridLayout->addWidget(cacher_clavier, 0, 5, 1, 1);

        touche_eval = new QPushButton(gridLayoutWidget);
        touche_eval->setObjectName(QStringLiteral("touche_eval"));

        gridLayout->addWidget(touche_eval, 2, 5, 1, 1);

        listVariables = new QListWidget(gridLayoutWidget);
        listVariables->setObjectName(QStringLiteral("listVariables"));

        gridLayout->addWidget(listVariables, 1, 14, 6, 2);

        touche_supegal = new QPushButton(gridLayoutWidget);
        touche_supegal->setObjectName(QStringLiteral("touche_supegal"));

        gridLayout->addWidget(touche_supegal, 6, 3, 1, 1);

        touche_7 = new QPushButton(gridLayoutWidget);
        touche_7->setObjectName(QStringLiteral("touche_7"));

        gridLayout->addWidget(touche_7, 2, 1, 1, 1);

        touche_5 = new QPushButton(gridLayoutWidget);
        touche_5->setObjectName(QStringLiteral("touche_5"));

        gridLayout->addWidget(touche_5, 3, 2, 1, 1);

        touche_1 = new QPushButton(gridLayoutWidget);
        touche_1->setObjectName(QStringLiteral("touche_1"));

        gridLayout->addWidget(touche_1, 4, 1, 1, 1);

        touche_dup = new QPushButton(gridLayoutWidget);
        touche_dup->setObjectName(QStringLiteral("touche_dup"));

        gridLayout->addWidget(touche_dup, 7, 6, 1, 1);

        touche_drop = new QPushButton(gridLayoutWidget);
        touche_drop->setObjectName(QStringLiteral("touche_drop"));

        gridLayout->addWidget(touche_drop, 7, 7, 1, 1);

        touche_swap = new QPushButton(gridLayoutWidget);
        touche_swap->setObjectName(QStringLiteral("touche_swap"));

        gridLayout->addWidget(touche_swap, 7, 8, 1, 1);

        touche_ift = new QPushButton(gridLayoutWidget);
        touche_ift->setObjectName(QStringLiteral("touche_ift"));

        gridLayout->addWidget(touche_ift, 7, 9, 1, 1);

        touche_clear_pile = new QPushButton(gridLayoutWidget);
        touche_clear_pile->setObjectName(QStringLiteral("touche_clear_pile"));

        gridLayout->addWidget(touche_clear_pile, 7, 11, 1, 1);

        EtatPile = new QTextEdit(gridLayoutWidget);
        EtatPile->setObjectName(QStringLiteral("EtatPile"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(EtatPile->sizePolicy().hasHeightForWidth());
        EtatPile->setSizePolicy(sizePolicy1);
        EtatPile->setReadOnly(true);

        gridLayout->addWidget(EtatPile, 1, 6, 6, 6);

        msg_util = new QLineEdit(gridLayoutWidget);
        msg_util->setObjectName(QStringLiteral("msg_util"));
        msg_util->setEnabled(true);
        msg_util->setReadOnly(true);

        gridLayout->addWidget(msg_util, 8, 1, 1, 5);

        touche_not = new QPushButton(gridLayoutWidget);
        touche_not->setObjectName(QStringLiteral("touche_not"));

        gridLayout->addWidget(touche_not, 7, 4, 1, 1);

        touche_or = new QPushButton(gridLayoutWidget);
        touche_or->setObjectName(QStringLiteral("touche_or"));

        gridLayout->addWidget(touche_or, 7, 3, 1, 1);

        touche_and = new QPushButton(gridLayoutWidget);
        touche_and->setObjectName(QStringLiteral("touche_and"));

        gridLayout->addWidget(touche_and, 7, 2, 1, 1);

        touche_different = new QPushButton(gridLayoutWidget);
        touche_different->setObjectName(QStringLiteral("touche_different"));

        gridLayout->addWidget(touche_different, 7, 1, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 1346, 27));
        menu_Fichier = new QMenu(menubar);
        menu_Fichier->setObjectName(QStringLiteral("menu_Fichier"));
        menu_Programmes = new QMenu(menubar);
        menu_Programmes->setObjectName(QStringLiteral("menu_Programmes"));
        menu_Variables = new QMenu(menubar);
        menu_Variables->setObjectName(QStringLiteral("menu_Variables"));
        menu_Outils = new QMenu(menubar);
        menu_Outils->setObjectName(QStringLiteral("menu_Outils"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menu_Fichier->menuAction());
        menubar->addAction(menu_Programmes->menuAction());
        menubar->addAction(menu_Variables->menuAction());
        menubar->addAction(menu_Outils->menuAction());
        menu_Fichier->addAction(actionQuitter);
        menu_Programmes->addAction(actionEditeur);
        menu_Variables->addAction(actionEditeur_2);
        menu_Outils->addAction(actionParam_tres);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        actionQuitter->setText(QApplication::translate("MainWindow", "Quitter", Q_NULLPTR));
        actionEditeur->setText(QApplication::translate("MainWindow", "Editeur", Q_NULLPTR));
        actionEditeur_2->setText(QApplication::translate("MainWindow", "Editeur", Q_NULLPTR));
        actionParam_tres->setText(QApplication::translate("MainWindow", "Param\303\250tres", Q_NULLPTR));
        touche_inferieur->setText(QApplication::translate("MainWindow", "<", Q_NULLPTR));
        touche_sup->setText(QApplication::translate("MainWindow", ">", Q_NULLPTR));
        cacher_var->setText(QApplication::translate("MainWindow", "Cacher", Q_NULLPTR));
        touche_prgm->setText(QApplication::translate("MainWindow", "Programmes", Q_NULLPTR));
        touche_mod->setText(QApplication::translate("MainWindow", "MOD", Q_NULLPTR));
        touche_entree->setText(QApplication::translate("MainWindow", "ENTREE", Q_NULLPTR));
        touche_6->setText(QApplication::translate("MainWindow", "6", Q_NULLPTR));
        touche_back->setText(QApplication::translate("MainWindow", "Backspace", Q_NULLPTR));
        touche_2->setText(QApplication::translate("MainWindow", "2", Q_NULLPTR));
        touche_clear->setText(QApplication::translate("MainWindow", "CLEAR", Q_NULLPTR));
        touche_fois->setText(QApplication::translate("MainWindow", "x", Q_NULLPTR));
        cacher_prgm->setText(QApplication::translate("MainWindow", "Cacher", Q_NULLPTR));
        touche_divent->setText(QApplication::translate("MainWindow", "DIV", Q_NULLPTR));
        touche_moins->setText(QApplication::translate("MainWindow", "-", Q_NULLPTR));
        touche_point->setText(QApplication::translate("MainWindow", ".", Q_NULLPTR));
        touche_egal->setText(QApplication::translate("MainWindow", "=", Q_NULLPTR));
        touche_9->setText(QApplication::translate("MainWindow", "9", Q_NULLPTR));
        touche_div->setText(QApplication::translate("MainWindow", "/", Q_NULLPTR));
        touche_plus->setText(QApplication::translate("MainWindow", "+", Q_NULLPTR));
        touche_3->setText(QApplication::translate("MainWindow", "3", Q_NULLPTR));
        touche_var->setText(QApplication::translate("MainWindow", "Variables", Q_NULLPTR));
        touche_4->setText(QApplication::translate("MainWindow", "4", Q_NULLPTR));
        touche_neg->setText(QApplication::translate("MainWindow", "+/-", Q_NULLPTR));
        touche_0->setText(QApplication::translate("MainWindow", "0", Q_NULLPTR));
        touche_inferegal->setText(QApplication::translate("MainWindow", "<=", Q_NULLPTR));
        touche_8->setText(QApplication::translate("MainWindow", "8", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "Etat de la pile", Q_NULLPTR));
        cacher_clavier->setText(QApplication::translate("MainWindow", "Cacher", Q_NULLPTR));
        touche_eval->setText(QApplication::translate("MainWindow", "EVAL", Q_NULLPTR));
        touche_supegal->setText(QApplication::translate("MainWindow", ">=", Q_NULLPTR));
        touche_7->setText(QApplication::translate("MainWindow", "7", Q_NULLPTR));
        touche_5->setText(QApplication::translate("MainWindow", "5", Q_NULLPTR));
        touche_1->setText(QApplication::translate("MainWindow", "1", Q_NULLPTR));
        touche_dup->setText(QApplication::translate("MainWindow", "DUP", Q_NULLPTR));
        touche_drop->setText(QApplication::translate("MainWindow", "DROP", Q_NULLPTR));
        touche_swap->setText(QApplication::translate("MainWindow", "SWAP", Q_NULLPTR));
        touche_ift->setText(QApplication::translate("MainWindow", "IFT", Q_NULLPTR));
        touche_clear_pile->setText(QApplication::translate("MainWindow", "CLEAR", Q_NULLPTR));
        touche_not->setText(QApplication::translate("MainWindow", "NOT", Q_NULLPTR));
        touche_or->setText(QApplication::translate("MainWindow", "OR", Q_NULLPTR));
        touche_and->setText(QApplication::translate("MainWindow", "AND", Q_NULLPTR));
        touche_different->setText(QApplication::translate("MainWindow", "!=", Q_NULLPTR));
        menu_Fichier->setTitle(QApplication::translate("MainWindow", "&Fichier", Q_NULLPTR));
        menu_Programmes->setTitle(QApplication::translate("MainWindow", "&Programmes", Q_NULLPTR));
        menu_Variables->setTitle(QApplication::translate("MainWindow", "&Variables", Q_NULLPTR));
        menu_Outils->setTitle(QApplication::translate("MainWindow", "&Outils", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
