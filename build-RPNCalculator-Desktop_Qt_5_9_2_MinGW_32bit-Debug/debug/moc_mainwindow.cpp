/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "C:/Users/mhdel/OneDrive/Bureau/LO21TP/mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[51];
    char stringdata0[1113];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "OptionsChanged"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 11), // "prgmChanged"
QT_MOC_LITERAL(4, 39, 25), // "on_cacher_clavier_clicked"
QT_MOC_LITERAL(5, 65, 22), // "on_touche_prgm_clicked"
QT_MOC_LITERAL(6, 88, 21), // "on_touche_var_clicked"
QT_MOC_LITERAL(7, 110, 23), // "on_touche_clear_clicked"
QT_MOC_LITERAL(8, 134, 22), // "on_touche_back_clicked"
QT_MOC_LITERAL(9, 157, 22), // "on_touche_drop_clicked"
QT_MOC_LITERAL(10, 180, 21), // "on_touche_dup_clicked"
QT_MOC_LITERAL(11, 202, 28), // "on_touche_clear_pile_clicked"
QT_MOC_LITERAL(12, 231, 22), // "on_cacher_prgm_clicked"
QT_MOC_LITERAL(13, 254, 19), // "on_touche_0_clicked"
QT_MOC_LITERAL(14, 274, 19), // "on_touche_1_clicked"
QT_MOC_LITERAL(15, 294, 19), // "on_touche_2_clicked"
QT_MOC_LITERAL(16, 314, 19), // "on_touche_3_clicked"
QT_MOC_LITERAL(17, 334, 19), // "on_touche_4_clicked"
QT_MOC_LITERAL(18, 354, 19), // "on_touche_5_clicked"
QT_MOC_LITERAL(19, 374, 19), // "on_touche_6_clicked"
QT_MOC_LITERAL(20, 394, 19), // "on_touche_7_clicked"
QT_MOC_LITERAL(21, 414, 19), // "on_touche_8_clicked"
QT_MOC_LITERAL(22, 434, 19), // "on_touche_9_clicked"
QT_MOC_LITERAL(23, 454, 23), // "on_touche_point_clicked"
QT_MOC_LITERAL(24, 478, 21), // "on_cacher_var_clicked"
QT_MOC_LITERAL(25, 500, 21), // "on_touche_neg_clicked"
QT_MOC_LITERAL(26, 522, 24), // "on_touche_entree_clicked"
QT_MOC_LITERAL(27, 547, 26), // "on_affichage_returnPressed"
QT_MOC_LITERAL(28, 574, 21), // "on_touche_div_clicked"
QT_MOC_LITERAL(29, 596, 22), // "on_touche_plus_clicked"
QT_MOC_LITERAL(30, 619, 23), // "on_touche_moins_clicked"
QT_MOC_LITERAL(31, 643, 22), // "on_touche_fois_clicked"
QT_MOC_LITERAL(32, 666, 24), // "on_touche_divent_clicked"
QT_MOC_LITERAL(33, 691, 21), // "on_touche_mod_clicked"
QT_MOC_LITERAL(34, 713, 26), // "on_actionQuitter_triggered"
QT_MOC_LITERAL(35, 740, 26), // "on_actionEditeur_triggered"
QT_MOC_LITERAL(36, 767, 28), // "on_actionEditeur_2_triggered"
QT_MOC_LITERAL(37, 796, 29), // "on_actionParam_tres_triggered"
QT_MOC_LITERAL(38, 826, 22), // "on_touche_swap_clicked"
QT_MOC_LITERAL(39, 849, 21), // "on_touche_ift_clicked"
QT_MOC_LITERAL(40, 871, 24), // "on_affichage_textChanged"
QT_MOC_LITERAL(41, 896, 4), // "arg1"
QT_MOC_LITERAL(42, 901, 27), // "on_touche_inferegal_clicked"
QT_MOC_LITERAL(43, 929, 27), // "on_touche_inferieur_clicked"
QT_MOC_LITERAL(44, 957, 25), // "on_touche_supegal_clicked"
QT_MOC_LITERAL(45, 983, 21), // "on_touche_sup_clicked"
QT_MOC_LITERAL(46, 1005, 22), // "on_touche_egal_clicked"
QT_MOC_LITERAL(47, 1028, 27), // "on_touche_different_clicked"
QT_MOC_LITERAL(48, 1056, 34), // "on_listVariables_itemDoubleCl..."
QT_MOC_LITERAL(49, 1091, 16), // "QListWidgetItem*"
QT_MOC_LITERAL(50, 1108, 4) // "item"

    },
    "MainWindow\0OptionsChanged\0\0prgmChanged\0"
    "on_cacher_clavier_clicked\0"
    "on_touche_prgm_clicked\0on_touche_var_clicked\0"
    "on_touche_clear_clicked\0on_touche_back_clicked\0"
    "on_touche_drop_clicked\0on_touche_dup_clicked\0"
    "on_touche_clear_pile_clicked\0"
    "on_cacher_prgm_clicked\0on_touche_0_clicked\0"
    "on_touche_1_clicked\0on_touche_2_clicked\0"
    "on_touche_3_clicked\0on_touche_4_clicked\0"
    "on_touche_5_clicked\0on_touche_6_clicked\0"
    "on_touche_7_clicked\0on_touche_8_clicked\0"
    "on_touche_9_clicked\0on_touche_point_clicked\0"
    "on_cacher_var_clicked\0on_touche_neg_clicked\0"
    "on_touche_entree_clicked\0"
    "on_affichage_returnPressed\0"
    "on_touche_div_clicked\0on_touche_plus_clicked\0"
    "on_touche_moins_clicked\0on_touche_fois_clicked\0"
    "on_touche_divent_clicked\0on_touche_mod_clicked\0"
    "on_actionQuitter_triggered\0"
    "on_actionEditeur_triggered\0"
    "on_actionEditeur_2_triggered\0"
    "on_actionParam_tres_triggered\0"
    "on_touche_swap_clicked\0on_touche_ift_clicked\0"
    "on_affichage_textChanged\0arg1\0"
    "on_touche_inferegal_clicked\0"
    "on_touche_inferieur_clicked\0"
    "on_touche_supegal_clicked\0"
    "on_touche_sup_clicked\0on_touche_egal_clicked\0"
    "on_touche_different_clicked\0"
    "on_listVariables_itemDoubleClicked\0"
    "QListWidgetItem*\0item"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      46,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  244,    2, 0x0a /* Public */,
       3,    0,  245,    2, 0x0a /* Public */,
       4,    0,  246,    2, 0x08 /* Private */,
       5,    0,  247,    2, 0x08 /* Private */,
       6,    0,  248,    2, 0x08 /* Private */,
       7,    0,  249,    2, 0x08 /* Private */,
       8,    0,  250,    2, 0x08 /* Private */,
       9,    0,  251,    2, 0x08 /* Private */,
      10,    0,  252,    2, 0x08 /* Private */,
      11,    0,  253,    2, 0x08 /* Private */,
      12,    0,  254,    2, 0x08 /* Private */,
      13,    0,  255,    2, 0x08 /* Private */,
      14,    0,  256,    2, 0x08 /* Private */,
      15,    0,  257,    2, 0x08 /* Private */,
      16,    0,  258,    2, 0x08 /* Private */,
      17,    0,  259,    2, 0x08 /* Private */,
      18,    0,  260,    2, 0x08 /* Private */,
      19,    0,  261,    2, 0x08 /* Private */,
      20,    0,  262,    2, 0x08 /* Private */,
      21,    0,  263,    2, 0x08 /* Private */,
      22,    0,  264,    2, 0x08 /* Private */,
      23,    0,  265,    2, 0x08 /* Private */,
      24,    0,  266,    2, 0x08 /* Private */,
      25,    0,  267,    2, 0x08 /* Private */,
      26,    0,  268,    2, 0x08 /* Private */,
      27,    0,  269,    2, 0x08 /* Private */,
      28,    0,  270,    2, 0x08 /* Private */,
      29,    0,  271,    2, 0x08 /* Private */,
      30,    0,  272,    2, 0x08 /* Private */,
      31,    0,  273,    2, 0x08 /* Private */,
      32,    0,  274,    2, 0x08 /* Private */,
      33,    0,  275,    2, 0x08 /* Private */,
      34,    0,  276,    2, 0x08 /* Private */,
      35,    0,  277,    2, 0x08 /* Private */,
      36,    0,  278,    2, 0x08 /* Private */,
      37,    0,  279,    2, 0x08 /* Private */,
      38,    0,  280,    2, 0x08 /* Private */,
      39,    0,  281,    2, 0x08 /* Private */,
      40,    1,  282,    2, 0x08 /* Private */,
      42,    0,  285,    2, 0x08 /* Private */,
      43,    0,  286,    2, 0x08 /* Private */,
      44,    0,  287,    2, 0x08 /* Private */,
      45,    0,  288,    2, 0x08 /* Private */,
      46,    0,  289,    2, 0x08 /* Private */,
      47,    0,  290,    2, 0x08 /* Private */,
      48,    1,  291,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,   41,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, 0x80000000 | 49,   50,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        MainWindow *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->OptionsChanged(); break;
        case 1: _t->prgmChanged(); break;
        case 2: _t->on_cacher_clavier_clicked(); break;
        case 3: _t->on_touche_prgm_clicked(); break;
        case 4: _t->on_touche_var_clicked(); break;
        case 5: _t->on_touche_clear_clicked(); break;
        case 6: _t->on_touche_back_clicked(); break;
        case 7: _t->on_touche_drop_clicked(); break;
        case 8: _t->on_touche_dup_clicked(); break;
        case 9: _t->on_touche_clear_pile_clicked(); break;
        case 10: _t->on_cacher_prgm_clicked(); break;
        case 11: _t->on_touche_0_clicked(); break;
        case 12: _t->on_touche_1_clicked(); break;
        case 13: _t->on_touche_2_clicked(); break;
        case 14: _t->on_touche_3_clicked(); break;
        case 15: _t->on_touche_4_clicked(); break;
        case 16: _t->on_touche_5_clicked(); break;
        case 17: _t->on_touche_6_clicked(); break;
        case 18: _t->on_touche_7_clicked(); break;
        case 19: _t->on_touche_8_clicked(); break;
        case 20: _t->on_touche_9_clicked(); break;
        case 21: _t->on_touche_point_clicked(); break;
        case 22: _t->on_cacher_var_clicked(); break;
        case 23: _t->on_touche_neg_clicked(); break;
        case 24: _t->on_touche_entree_clicked(); break;
        case 25: _t->on_affichage_returnPressed(); break;
        case 26: _t->on_touche_div_clicked(); break;
        case 27: _t->on_touche_plus_clicked(); break;
        case 28: _t->on_touche_moins_clicked(); break;
        case 29: _t->on_touche_fois_clicked(); break;
        case 30: _t->on_touche_divent_clicked(); break;
        case 31: _t->on_touche_mod_clicked(); break;
        case 32: _t->on_actionQuitter_triggered(); break;
        case 33: _t->on_actionEditeur_triggered(); break;
        case 34: _t->on_actionEditeur_2_triggered(); break;
        case 35: _t->on_actionParam_tres_triggered(); break;
        case 36: _t->on_touche_swap_clicked(); break;
        case 37: _t->on_touche_ift_clicked(); break;
        case 38: _t->on_affichage_textChanged((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 39: _t->on_touche_inferegal_clicked(); break;
        case 40: _t->on_touche_inferieur_clicked(); break;
        case 41: _t->on_touche_supegal_clicked(); break;
        case 42: _t->on_touche_sup_clicked(); break;
        case 43: _t->on_touche_egal_clicked(); break;
        case 44: _t->on_touche_different_clicked(); break;
        case 45: _t->on_listVariables_itemDoubleClicked((*reinterpret_cast< QListWidgetItem*(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow.data,
      qt_meta_data_MainWindow,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 46)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 46;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 46)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 46;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
