#ifndef COMPUTER_H_INCLUDED
#define COMPUTER_H_INCLUDED

#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string.h>


using namespace std;

class ComputException{
string info;
public:
    ComputException(const string& s): info(s){}
    string GetInfo() const {return info;}
};


class Entier{
private:
    int ent;
    string intitule= "Entier";
public:
    Entier(int i=0):ent(i){}
    const int getEnt() const {return ent;}
    Entier operator+(const Entier& e) {Entier i(ent+e.ent); return i;}
    Entier operator-(const Entier& e) {Entier i(ent-e.ent); return i;}
    Entier operator*(const Entier& e) {Entier i(ent*e.ent); return i;}
//    Fraction operator/(const Entier& e) {Fraction f(this, e); return f;}
    Entier DIV(const Entier& e){Entier i(ent/e.ent); return i;} //Attention, pas des operator -> e.DIV(e1)
    Entier MOD(const Entier& e) {Entier i(ent%e.ent); return i;}
    Entier NEG(){Entier i(-ent); return i;}
    bool operator<(const Entier& e){return ent<e.ent;}
    bool operator>(const Entier& e){return ent>e.ent;}
    bool operator==(const Entier& e){return ent==e.ent;}
    bool operator!=(const Entier& e){return ent!=e.ent;}
    void operator++(){ent++;}
    void operator--(){ent--;}


};

ostream & operator<<(ostream &f, Entier &e);


class Fraction
{
    public:
        Fraction(Entier n, Entier d): numerateur(n),denominateur(d){simplification();};
        Entier const getNum(){return numerateur;}
        Entier const getDeno(){return denominateur;}
        void setFraction(Entier n, Entier d);
        Fraction operator+(Fraction& f1) {Fraction f(numerateur*f1.denominateur+f1.numerateur*denominateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator-(Fraction& f1) {Fraction f(numerateur*f1.denominateur-f1.numerateur*denominateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator*(Fraction& f1) {Fraction f(numerateur*f1.numerateur,denominateur*f1.denominateur);simplification();return f;};
        Fraction operator/(Fraction& f1) {Fraction f(numerateur*f1.denominateur,denominateur*f1.numerateur);simplification();return f;};
        Fraction NEG() {Fraction f(numerateur.NEG(),denominateur);return f;};
        void simplification();
    private:
        Entier numerateur;
        Entier denominateur;
        string intitule= "Fraction";
};

ostream & operator<<(ostream& f, Fraction& frac);


class Reel
{
    public:
        Reel(Entier e=0, Entier m=0): ent(e), mant(m){simplification();}
        Entier const getEnt(){return ent;}
        Entier const getMant(){return mant;}
        Reel const operator+(Reel& r1);
        Reel const operator-(Reel& r1);
        Reel const operator*(Reel& r1);
        Reel const operator/(Reel& r1);
        Reel NEG(){Reel r(ent.NEG(),mant);return r;}
        Entier simplification();
    private:
        Entier ent;
        Entier mant;
        string intitule= "Reel";
};

ostream & operator<<(ostream& f, Reel& r);
int taille(int n);
int puissance_dix(int n);


class Item{
public:
    Item()= default;
    void STO(const Item& it, Item& itexp); //si l'operateur STO contient une litterale num, il appelle addVar, sinon addPrg
    const string& GetIntitule() const {return intitule;}
    enum class Type{Variable,Programme};
    Type getType() const {return _type;}
private:
    string intitule;
    Type _type;
};

class ItemManager {
    Item** it = nullptr;
    size_t nb = 0;
    size_t nbMax = 0;
    void agrandissementCapacite();
public:
    ItemManager() = default;
    ~ItemManager();
    ItemManager(const ItemManager & m);
    //Ajout accesseur taille tableau
    const size_t getNb() const {return nb;}
    //ajout accesseur item
    const Item* getItem() const {return *it;}
    ItemManager & operator=(const ItemManager & m);
    Item& addItem();
    void removeItem(Item& e);

};

class Atome: public Item{
string nom;
public:
    Atome(const string& n): nom(n){
    char firstcar = n.at(0);
    if (firstcar<'A'|| firstcar>'Z')
        throw ComputException("la première lettre doit etre une majuscule ");
    } //verif de la validite du nom de l'atome
    const string& getNom() const {return nom;}
};


class Variable: public Atome
{
    Item value;
    string intitule= "Variable";
public:
    Variable(const string& n,Item val): Atome(n), value(val){} //ici il doit manquer un attribut

};

class Programme: public Atome {

string instructions;
string intitule= "Programme";
public:
    Programme(const string& n, const string& i):Atome(n), instructions(i){
    if (instructions.at(0) != '[' || instructions.at(instructions.size()) !=']')
        throw ComputException("mauvaise syntaxe, instructions entre crochets");
    }
    const string& GetInstructions() const {return instructions;}
    void executer();
};

/*
L’opérateurEVALpermet d’évaluer numériquement une littérale expression.
Si la littérale atome associéeà l’expression correspond à l’identificateur d’une variable,
l’évaluation provoque l’empilement de lalittérale associée cette variable.
Si la littérale atome associé à l’expression correspond à l’identificateurd’un programme,
l’évaluation provoque l’évaluation du programme associé (i.e.l’exécution de la suited’opérandes du programme).
Si la littérale expression est un atome qui ne correspond pas au nom d’unevariable ou d’un programme,
l’évaluation n’a aucune effet et un message en informe l’utilisateur.
*/

class Expression: public Item
{
    string nom;
    string intitule="Expression";
public:
    Expression(const string& n):nom(n){};
    const string& getNom() const {return nom;}
    int operatorEVAL(ItemManager& itMan);

};

/*
class ExpressionManager {
    Expression** exps = nullptr;
    size_t nb = 0;
    size_t nbMax = 0;
    void agrandissementCapacite();
public:
    ExpressionManager() = default;
    ~ExpressionManager();
    ExpressionManager(const ExpressionManager & m);
    ExpressionManager & operator=(const ExpressionManager & m);
    Expression& addExpression(int v);
    void removeExpression(Expression& e);
};*/
#endif // COMPUTER_H_INCLUDED
