#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include<math.h>
#include "computer.h"

using namespace std;

ostream & operator<<(ostream &f, Entier &e){
    f<<e.getEnt()<<endl;
    return f;
}

void Fraction::setFraction(Entier n, Entier d)
{
    numerateur=n;
    if (d!=0)
        denominateur=d;
    else
    {
        denominateur=1;
        throw"Erreur, dénominateur nul.";
    }
    simplification();

}

void Fraction::simplification()
{
    // si le numerateur est 0, le denominateur prend la valeur 1
    if(numerateur==0)
    {
        denominateur=1;
        return;
    }
    /*un denominateur ne devrait pas être 0;si c’est le cas, on sort de la méthode*/
    if(denominateur==0)
        return;
    /*utilisation de l’algorithme d’Euclide pour trouver le Plus Grand CommunDenominateur (PGCD) entre le numerateur et le denominateur*/
    Entier a=numerateur, b=denominateur;
    // on ne travaille qu’avec des valeurs positives...
    if(a<0)
        a=numerateur.NEG();
    if(b<0) b=denominateur.NEG();
    while(a!=b)
    {
        if(a>b)
            a=a-b;
        else
            b=b-a;
    }
    // on divise le numerateur et le denominateur par le PGCD=a
    numerateur=numerateur.DIV(a);
    denominateur=denominateur.DIV(a);// si le denominateur est négatif, on fait passer le signe - au denominateur
    if(denominateur<0)
    {
    denominateur=denominateur.NEG();
    numerateur=numerateur.NEG();
    }
}

ostream & operator <<(ostream& f, Fraction& frac)
{
    if (frac.getDeno().getEnt()!=1)
        f<<frac.getNum().getEnt()<<"/"<<frac.getDeno().getEnt()<<endl;
    else
        f<<frac.getNum().getEnt()<<endl;
    return f;
}


/*
_______________________________________________________________________

Fonction de service qui renvoie la taille d'un int
Au dessus de 10, on est en overflow de l'int donc ça ne fonctionne plus
________________________________________________________________________
*/

int taille(int n) {
    int res = 0;
    if(n == 0) {
        return 1;
    }
    else if(n < 0)  { //si tu compte le moins dans la longueur
        ++res;
    }
    while(n != 0) {
        n /= 10.0;
        ++res;
    }
    return res;
}




/*
_________________________________________________________________________________________________

Si on utilise pow(int,int) avec une variable en deuxième paramètre, ça renvoie parfois des 99.
(avec des chiffres négatifs c'est bon par contre).
Donc implémentation de fonction de service pour donner les puissances de 10.
Jusque'a 10 maximum, parce qu'au dessus on est en overflow des int.
__________________________________________________________________________________________________
*/
int puissance_dix(int n){
if (n>=10)
    cout << "Nombre trop grand";
else
    switch(n){
        case(1):
            return 10;
            break;
        case(2):
            return 100;
            break;
        case(3):
            return 1000;
            break;
        case(4):
            return 10000;
            break;
        case(5):
            return 100000;
            break;
        case(6):
            return 1000000;
            break;
        case(7):
            return 10000000;
            break;
        case(8):
            return 100000000;
            break;
        case(9):
            return 1000000000;
            break;
        default:
            return 0;
    }
        return 0;
}


Reel const Reel::operator+(Reel& r)
{
    double re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -taille(mant.getEnt()));
    double re2 = r.ent.getEnt()+((double) r.mant.getEnt())*pow(10, -taille(r.mant.getEnt()));
    double re3 = re1+re2;

    int sizetot;
    if(taille(mant.getEnt())>taille(r.mant.getEnt()))
        sizetot = taille(mant.getEnt());
    else
        sizetot = taille(r.mant.getEnt());

    re3 = re3 * puissance_dix(sizetot);
    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re(entfin,mantfin);
    return re;
}

Reel const Reel::operator-(Reel& r)
{
    double re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -taille(mant.getEnt()));
    double re2 = r.ent.getEnt()+((double) r.mant.getEnt())*pow(10, -taille(r.mant.getEnt()));
    double re3 = re1-re2;

    int sizetot;
    if(taille(mant.getEnt())>taille(r.mant.getEnt()))
        sizetot = taille(mant.getEnt());
    else
        sizetot = taille(r.mant.getEnt());

    re3 = re3 * puissance_dix(sizetot);
    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re(entfin,mantfin);
    return re;
}

Reel const Reel::operator*(Reel& r)
{
    double re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -taille(mant.getEnt()));
    double re2 = r.ent.getEnt()+((double) r.mant.getEnt())*pow(10, -taille(r.mant.getEnt()));
    double re3 = re1*re2;

    int sizetot;
    if(taille(mant.getEnt())>taille(r.mant.getEnt()))
        sizetot = taille(mant.getEnt());
    else
        sizetot = taille(r.mant.getEnt());

    re3 = re3 * puissance_dix(sizetot);
    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re(entfin,mantfin);
    return re;
}


Reel const Reel::operator/(Reel& r)
{   double re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -taille(mant.getEnt()));
    double re2 = r.ent.getEnt()+((double) r.mant.getEnt())*pow(10, -taille(r.mant.getEnt()));
    double re3 = re1/re2;

    int sizetot;
    if(taille(mant.getEnt())>taille(r.mant.getEnt()))
        sizetot = taille(mant.getEnt());
    else
        sizetot = taille(r.mant.getEnt());

    re3 = re3 * puissance_dix(sizetot);
    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re(entfin,mantfin);
    return re;

}




//Une littérale réelle dont la matisse est nulle est simplifiée en une littérale entière
Entier Reel::simplification()
{
    if (mant.getEnt()==0)
    {
        Entier e(ent);
        return e;
    }
}

ostream & operator<<(ostream& f, Reel& r)
{
    if (r.getMant().getEnt()!=0)
        f<<r.getEnt().getEnt()<<"."<<r.getMant().getEnt()<<endl;
    else
        f<<r.getEnt().getEnt()<<endl;
    return f;
}




//----------------------------------------------------------------------------------------------------------------------
//ItemManager

void ItemManager::agrandissementCapacite() {

    // Création d'un nouveau tableau plus grand
    size_t newSize = (nbMax+1)*2;
    Item ** newtab = new Item*[newSize];
    // Copie du contenu de l'ancien tableau vers le nouveau
    for(size_t i=0; i<nb; i++) {
        newtab[i] = it[i];
    }
    //std::memcpy(newtab, exps, nb*sizeof(Expression*));
    delete[] it;
    it = newtab;
    nbMax = newSize;
}


Item& ItemManager::addItem() {
    // Agrandit le tableau si nécessaire
    if(nb==nbMax) {
        agrandissementCapacite();
    }
    it[nb] = new Item();//pas sur
    return *it[nb++];
}

void ItemManager::removeItem(Item & v) {
    bool isFound = false;
    for(size_t i=0; i<nb; i++) {
        if(isFound) {
            // Décale
            it[i-1]=it[i];
        } else if(it[i]==&v) {
            // Supprime
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Item non trouve");
    }
    nb--;
}


ItemManager::~ItemManager() {
    // Libère la mémoire allouée à chaque objet
    for(size_t i=0; i<nb; i++) {
        delete it[i];
    }
    // Libère la mémoire allouée au tableau de pointeurs
    delete[] it;
}

ItemManager::ItemManager(const ItemManager & v) : it(new Item*[v.nbMax]), nb(v.nb), nbMax(v.nbMax) {
    for(size_t i=0; i<nb; i++) { // apres avoir alloué de la mémoire pour un nouveau tableau distinct, on cree un copie de l'expression et on l'enregistre dans le tableau crée
        it[i]= new Item(*v.it[i]);
    }

}

ItemManager & ItemManager::operator=(const ItemManager & v) {
    // Gère l'opération d'autoaffectation
    if(this==&v)
        return *this;

    // Création d'un nouveau tableau
    Item ** newtab = new Item*[v.nbMax];
    for(size_t i=0; i<v.nb; i++) {
        newtab[i] = new Item(*v.it[i]);// ici on veut deux éléments distincts donc on fait une duplication
    }
    // Suppression de l'ancien tableau
    for(size_t i =0; i<nb; i++) {
        delete it[i];
    }
    delete[] it;
    it = newtab;
    nb = v.nb;
    nbMax = v.nbMax;
    return *this;
}
//-----------------------------------------------------------------------------------------------------------------------

int Expression::operatorEVAL(ItemManager& itMan)
{
    //On parcourt le tableau
    //Si on n'a aucune correspondance, alors on affiche simplement le message
    //Si on a une correspondance avec un id de variable alors on récupère la valeur littérale de la variable et on l'empile
    //Si on a une correspondance avec un id de prgm alors on évalue le prgm et on l'exécute
    for (size_t i=0;i<itMan.getNb();i++)
    {
        //Tests
        if (itMan.getItem()->GetIntitule()==nom) //On cherche une correspondance
        {
            if (itMan.getItem()->getType()==Item::Type::Variable)//C'est un atome prgm ou variable
            {
                //C'est une variable
                cout<<"Correspondance trouvée : atome variable"<<endl;
                //On récupère la valeur de la variable



            }
            else
            {
                //C'est un atome
                cout<<"Correspondance trouvée : atome programme"<<endl;
            }
        return 1;
        }
    }
    cout<<"Aucune correspondance trouvée"<<endl;
    return 0;


}


/*
void Programme::executer()
{

    char firstcar;
    for (unsigned int i=1; i<instructions.size(); i++)
    {
        firstcar = instructions.at(i);
        if (firstcar != ']')
            controleur.commande(firstcar);
    }
}
*/
