#include <iostream>
#include <math.h>
#include "computer.h"

using namespace std;

ostream & operator<<(ostream &f, Entier &e){
    f<<e.getEnt()<<endl;
    return f;
}

ostream & operator<<(ostream &f, Item &i){
    f<<i.GetId()<<endl;
    return f;
}

/*---------------Fraction------------------*/

void Fraction::setFraction(Entier n, Entier d){
    numerateur=n;
    Entier nul(0);
    if (d!=nul)
        denominateur=d;
    else
    {
        denominateur=1;
        throw"Erreur, dénominateur nul.";
    }
    simplification();

}

void Fraction::simplification(){
    Entier nul(0);
    // si le numerateur est 0, le denominateur prend la valeur 1
    if(numerateur==nul)
    {
        denominateur=1;
        return;
    }
    /*un denominateur ne devrait pas être 0;si c’est le cas, on sort de la méthode*/
    if(denominateur==nul)
        return;
    /*utilisation de l’algorithme d’Euclide pour trouver le Plus Grand CommunDenominateur (PGCD) entre le numerateur et le denominateur*/
    Entier a=numerateur, b=denominateur;
    // on ne travaille qu’avec des valeurs positives...
    if(a<nul)
        a=numerateur.NEG();
    if(b<nul) b=denominateur.NEG();
    while(a!=b)
    {
        if(a>b)
            a=a-b;
        else
            b=b-a;
    }
    // on divise le numerateur et le denominateur par le PGCD=a
    numerateur=numerateur.DIV(a);
    denominateur=denominateur.DIV(a);// si le denominateur est négatif, on fait passer le signe - au denominateur
    if(denominateur<nul)
    {
    denominateur=denominateur.NEG();
    numerateur=numerateur.NEG();
    }
}

ostream & operator <<(ostream& f, Fraction& frac) {
    if (frac.getDeno().getEnt()!=1)
        f<<frac.getNum().getEnt()<<"/"<<frac.getDeno().getEnt()<<endl;
    else
        f<<frac.getNum().getEnt()<<endl;
    return f;
}



/*
_______________________________________________________________________

Fonction de service qui renvoie la taille d'un int
Au dessus de 10, on est en overflow de l'int donc ça ne fonctionne plus
________________________________________________________________________
*/

int taille(int n) {
    int res = 0;
    if(n == 0) {
        return 1;
    }
    else if(n < 0)  { //si tu compte le moins dans la longueur
        ++res;
    }
    while(n != 0) {
        n /= 10.0;
        ++res;
    }
    return res;
}




/*
_________________________________________________________________________________________________

Si on utilise pow(int,int) avec une variable en deuxième paramètre, ça renvoie parfois des 99.
(avec des chiffres négatifs c'est bon par contre).
Donc implémentation de fonction de service pour donner les puissances de 10.
Jusque'a 10 maximum, parce qu'au dessus on est en overflow des int.
__________________________________________________________________________________________________
*/
int puissance_dix(int n){
if (n>=10)
    cout << "Nombre trop grand";
else
    switch(n){
        case(1):
            return 10;
            break;
        case(2):
            return 100;
            break;
        case(3):
            return 1000;
            break;
        case(4):
            return 10000;
            break;
        case(5):
            return 100000;
            break;
        case(6):
            return 1000000;
            break;
        case(7):
            return 10000000;
            break;
        case(8):
            return 100000000;
            break;
        case(9):
            return 1000000000;
            break;
        default:
            return 0;
    }
        return 0;
}


const Reel& Reel::operator+(const LitNumerique& r) const{
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);
    int re1_t = taille(mant.getEnt()) + nb_zero;
    int re2_t = taille(pt->mant.getEnt()) + pt->nb_zero;
    int re1 = 0;
    int re2 = 0;
    int sizediff = re1_t - re2_t;
    int sizemax = re1_t;

    if(sizediff > 0){
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = (pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt()) * puissance_dix(sizediff);
    }
    else if(sizediff<0){
        sizediff = -sizediff;
        sizemax = re2_t;
        re1 = (ent.getEnt() * puissance_dix(re1_t) + mant.getEnt()) * puissance_dix(sizediff);
        re2 = pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt();
    }
    else{
        re1 = ent.getEnt() * puissance_dix(re1_t) + mant.getEnt();
        re2 = pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt();
    }

    int re3 = re1+re2;
    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizemax; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }
    int entfin = re3/puissance_dix(sizemax);
    int mantfin = re3%puissance_dix(sizemax);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin, mantfin, zerofin);
    return (*pt1);
}

const Reel& Reel::operator-(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+pt->nb_zero;
    int re1 = 0;
    int re2 = 0;
    int sizediff = re1_t - re2_t;
    int sizemax = re1_t;

    if(sizediff > 0){
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = (pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt()) * puissance_dix(sizediff);
    }
    else if(sizediff<0){
        sizediff = -sizediff;
        sizemax = re2_t;
        re1 = (ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt()) * puissance_dix(sizediff);
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    }
    else{
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    }

    int re3 = re1-re2;
    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizemax; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }


    int entfin = re3/puissance_dix(sizemax);
    int mantfin = re3%puissance_dix(sizemax);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin,zerofin);
    return (*pt1);
}

const Reel& Reel::operator*(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+pt->nb_zero;
    int re1;
    int re2;

    if(ent.getEnt()>= 0)
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
    else
        re1 = ent.getEnt() * puissance_dix(re1_t)- mant.getEnt();
    if(pt->ent.getEnt()>= 0)
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    else
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)- pt->mant.getEnt();

    int re3 = re1*re2;
    int sizetot = re1_t+re2_t;

    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizetot; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }

    int entfin = re3/puissance_dix(sizetot);
    int mantfin = re3%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin,zerofin);
    return (*pt1);
}


const Reel& Reel::operator/(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+ pt->nb_zero;
    double re1;
    double re2;

    // on cast les deux reels en double pour pouvoir faire les calculs
    if(ent.getEnt() >= 0)
        re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -re1_t);
    else
        re1 = ent.getEnt()-((double) mant.getEnt())*pow(10, -re1_t);

    if(pt->ent.getEnt() >= 0)
        re2 = pt->ent.getEnt()+((double) pt->mant.getEnt())*pow(10, -re2_t);
    else
        re2 = pt->ent.getEnt()-((double) pt->mant.getEnt())*pow(10, -re2_t);


    double re3 = re1/re2;

    //On définit la taille de la mantisse
    int sizetot=4;
    if(taille(mant.getEnt()) > sizetot)
        sizetot = taille(mant.getEnt());
    if(taille(pt->mant.getEnt())>sizetot)
        sizetot = taille(pt->mant.getEnt());

    double d_temp = re3;
    int zerofin = 0;
    int p_ent = ((int) floor(d_temp));

    //si la partie entiere est nulle
    if(p_ent==0){
        zerofin = -1;
        while(p_ent == 0){
            zerofin ++;
            d_temp = d_temp*10;
            p_ent = ((int) floor(d_temp));

        }
        re3 = re3 * puissance_dix(sizetot);
        int re = ((int) re3);
    }
    //si la partie entiere n'est pas nulle
    else {
        re3 = re3 * puissance_dix(sizetot);
        int re = ((int) re3);
        int i_temp = re;

        for(int i = 0; i<sizetot; i++){
            if(i_temp%10 == 0)
                zerofin ++;
            else
                zerofin = 0;
            i_temp = i_temp/10;
        }
    }

    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin, zerofin);
    return (*pt1);

}





//Une littérale réelle dont la matisse est nulle est simplifiée en une littérale entière
//Fonction uniquement appelée par le constructeur quand la mantisse est nulle
Entier Reel::simplification()
{
    Entier e(ent);
    return e;
}

ostream & operator<<(ostream& f, const Reel& r){

    if (r.getMant().getEnt()!=0)
        f<<r.getEnt().getEnt()<<"."<<r.getMant().getEnt()<<endl;
    else
        f<<r.getEnt().getEnt()<<endl;
    return f;
}


/*------------------------------------------------------

Fonction de service : Cast de Fraction vers Reel.
Prends une fraction en paramètre et retourne un reel.

-----------------------------------------------------------*/

Reel r_cast(const Fraction& f){
    double re = ((double) f.getNum().getEnt())/f.getDeno().getEnt();

    int t_mant = 4; // on met une mantisse à 4 chiffres

    double d_temp = re;
    int zerofin = 0;
    int p_ent = ((int) floor(d_temp));

    //si la partie entiere est nulle
    if(p_ent==0){
        zerofin = -1;
        while(p_ent == 0){
            zerofin ++;
            d_temp = d_temp*10;
            p_ent = ((int) floor(d_temp));

        }
        re = re * puissance_dix(t_mant);
        int re_temp = ((int) re);
    }
    //si la partie entiere n'est pas nulle
    else {
        re = re * puissance_dix(t_mant);
        int re_temp = ((int) re);
        int i_temp = re_temp;

        for(int i = 0; i<t_mant; i++){
            if(i_temp%10 == 0)
                zerofin ++;
            else
                zerofin = 0;
            i_temp = i_temp/10;
        }
    }

    int entfin = re/puissance_dix(t_mant);
    int mantfin = ((int) re)%puissance_dix(t_mant);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re1(entfin,mantfin,zerofin);
    return re1;
}

/*----------------Surcharge Opérateurs-----------------------------*/

const Fraction operator/(const Entier& n, const Entier& d){
    Fraction f(n, d);
    return f;
}

/*---------------------Opérateur+----------------------------------*/

const Fraction operator+(const Entier& e, const Fraction& f){
    Fraction s(e*f.getDeno()+f.getNum(), f.getDeno());
    return s;
}

const Reel operator+(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp + r;
    return s;
}

const Reel operator+(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = r + re;
    return somme;
}

const Fraction operator+(const Fraction& f, const Entier& e){return e+f;}
const Reel operator+(const Reel& r, const Entier& e){return e+r;}
const Reel operator+(const Reel& r, const Fraction& f){return f+r;}

/*---------------------Opérateur(-)----------------------------------*/


const Fraction operator-(const Entier& e, const Fraction& f){
    Fraction s(e*f.getDeno()-f.getNum(), f.getDeno());
    return s;
}

const Fraction operator-(const Fraction& f, const Entier& e){
    Fraction s(f.getNum()-e*f.getDeno(), f.getDeno());
    return s;
}

const Reel operator-(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp - r;
    return s;
}

const Reel operator-(const Reel& r, const Entier& e){
    Reel r_temp(e);
    Reel s = r - r_temp;
    return s;
}

const Reel operator-(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel sous = re - r;
    return sous;
}

const Reel operator-(const Reel& r, const Fraction& f){
    Reel re = r_cast(f);
    Reel sous = r - re;
    return sous;
}
//--------------------------Operateur*---------------------------

const Fraction operator*(const Entier& e, const Fraction& f){
    Fraction s(e*f.getNum(), f.getDeno());
    return s;
}

const Reel operator*(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp * r;
    return s;
}

const Reel operator*(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = re * r;
    return somme;
}

const Fraction operator*(const Fraction& f, const Entier& e){return e*f;}
const Reel operator*(const Reel& r, const Entier& e){return e*r;}
const Reel operator*(const Reel& r, const Fraction& f){return f*r;}



//----------------------------Operateur/--------------------------------

const Fraction operator/(const Entier& e, const Fraction& f){
    Fraction f_temp(f.getDeno(), f.getNum());
    Fraction s = e*f_temp;
    return s;
}

const Fraction operator/(const Fraction& f, const Entier& e){
    Entier e_temp(1);
    Fraction f_temp(e_temp, e);
    Fraction s = f*f_temp;
    return s;
}

const Reel operator/(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp / r;
    return s;
}

const Reel operator/(const Reel& r, const Entier& e){
    Reel r_temp(e);
    Reel s = r / r_temp;
    return s;
}

const Reel operator/(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = re / r;
    return somme;
}

const Reel operator/(const Reel& r, const Fraction& f){
    Reel re = r_cast(f);
    Reel somme = r / re;
    return somme;
}

/*-----------------------------Operateurs logiques-------------------------------------*/

/*---------------------------------Operateur<------------------------------------------*/

const bool operator<(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp<f;
}

const bool operator<(const Fraction& f, const Entier& e){
    Fraction f_temp(e, 1);
    return f<f_temp;
}

const bool operator<(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp<r;
}

const bool operator<(const Reel& r, const Entier& e){
    Reel r_temp(e);
    return r<r_temp;
}

const bool operator<(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp <r;
}

const bool operator<(const Reel& r, const Fraction& f){
    Reel r_temp = r_cast(f);
    return r<r_temp;
}

/*---------------------------------Operateur>------------------------------------------*/

const bool operator>(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp>f;
}

const bool operator>(const Fraction& f, const Entier& e){
    Fraction f_temp(e, 1);
    return f>f_temp;
}

const bool operator>(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp>r;
}

const bool operator>(const Reel& r, const Entier& e){
    Reel r_temp(e);
    return r>r_temp;
}

const bool operator>(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp >r;
}

const bool operator>(const Reel& r, const Fraction& f){
    Reel r_temp = r_cast(f);
    return r>r_temp;
}

/*---------------------------------Operateur==------------------------------------------*/

const bool operator==(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp==f;
}

const bool operator==(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp==r;
}

const bool operator==(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp==r;
}

const bool operator==(const Fraction& f, const Entier& e){return e==f;}
const bool operator==(const Reel& r, const Entier& e){return e==r;}
const bool operator==(const Reel& r, const Fraction&f){return f==r;}

/*---------------------------------Operateur!=------------------------------------------*/

const bool operator!=(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp!=f;
}

const bool operator!=(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp!=r;
}

const bool operator!=(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp!=r;
}

const bool operator!=(const Fraction& f, const Entier& e){return e!=f;}
const bool operator!=(const Reel& r, const Entier& e){return e!=r;}
const bool operator!=(const Reel& r, const Fraction& f){return f!=r;}

/*---------------------------------Operateur<=------------------------------------------*/

const bool operator<=(const Entier& e, const Fraction& f){return !(e>f);}
const bool operator<=(const Fraction& f, const Entier& e){return !(f>e);}
const bool operator<=(const Entier& e, const Reel& r){return !(e>r);}
const bool operator<=(const Reel& r, const Entier& e){return !(r>e);}
const bool operator<=(const Fraction& f, const Reel& r){return !(f>r);}
const bool operator<=(const Reel& r, const Fraction& f){return !(r>f);}

/*---------------------------------Operateur>=------------------------------------------*/

const bool operator>=(const Entier& e, const Fraction& f){return !(e<f);}
const bool operator>=(const Fraction& f, const Entier& e){return !(f<e);}
const bool operator>=(const Entier& e, const Reel& r){return !(e<r);}
const bool operator>=(const Reel& r, const Entier& e){return !(r<e);}
const bool operator>=(const Fraction& f, const Reel& r){return !(f<r);}
const bool operator>=(const Reel& r, const Fraction& f){return !(r<f);}

//----------------------------------------------------------------------------------------------------------------------
//ItemManager

void ItemManager::agrandissementCapacite() {

    // Création d'un nouveau tableau plus grand
    size_t newSize = (nbMax+1)*2;
    Item ** newtab = new Item*[newSize];
    // Copie du contenu de l'ancien tableau vers le nouveau
    for(size_t i=0; i<nb; i++) {
        newtab[i] = it[i];
    }
    //std::memcpy(newtab, exps, nb*sizeof(Expression*));
    delete[] it;
    it = newtab;
    nbMax = newSize;
}

Item& ItemManager::addItem(Item* item) {
// lors de l'appel de STO: on stocke une var ou un prog
// lors de la création d'un operateur, on le stocke

    if(nb==nbMax) {
        agrandissementCapacite();
    }
    it[nb] = item;
    //cout<<"additem"<<endl;
    //cout<<it[nb]->GetId();
    return *it[nb++];
}

void ItemManager::removeItem(Item & v) {
    bool isFound = false;
    for(size_t i=0; i<nb; i++) {
        if(isFound) {
            // Décale
            it[i-1]=it[i];
        } else if(it[i]==&v) {
            // Supprime
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Item non trouve");
    }
    nb--;
}


ItemManager::~ItemManager() {
    // Libère la mémoire allouée à chaque objet
    for(size_t i=0; i<nb; i++) {
        delete it[i];
    }
    // Libère la mémoire allouée au tableau de pointeurs
    delete[] it;
}

ItemManager::ItemManager(const ItemManager & v) : it(new Item*[v.nbMax]), nb(v.nb), nbMax(v.nbMax) {
    for(size_t i=0; i<nb; i++) { // apres avoir alloué de la mémoire pour un nouveau tableau distinct, on cree un copie de l'expression et on l'enregistre dans le tableau crée
        it[i]= new Item(*v.it[i]);
    }

}

ItemManager & ItemManager::operator=(const ItemManager & v) {
    // Gère l'opération d'autoaffectation
    if(this==&v)
        return *this;

    // Création d'un nouveau tableau
    Item ** newtab = new Item*[v.nbMax];
    for(size_t i=0; i<v.nb; i++) {
        newtab[i] = new Item(*v.it[i]);// ici on veut deux éléments distincts donc on fait une duplication
    }
    // Suppression de l'ancien tableau
    for(size_t i =0; i<nb; i++) {
        delete it[i];
    }
    delete[] it;
    it = newtab;
    nb = v.nb;
    nbMax = v.nbMax;
    return *this;
}


ItemManager& ItemManager::donneInstance()
{
    static ItemManager uniqueinstance;
    uniqueinstance.intialise_tableau();
    return uniqueinstance;
}

void ItemManager::intialise_tableau()
{   bool done = false;

    if(done==false){

    Operateur* opSwap = new Operateur(2,"SWAP");
    Operateur* opDUP= new Operateur(1,"DUP");
    Operateur* opDROP= new Operateur(1,"DROP");
    Operateur* opCLEAR= new Operateur(0,"CLEAR");
    Operateur* opIFT= new Operateur(2,"IFT");
    Operateur* opSTO= new Operateur(2,"STO");
    Operateur* opFORGET= new Operateur(1,"FORGET");
    Operateur* opEVAL= new Operateur(1,"EVAL");
    Operateur* opAND= new Operateur(2,"AND");
    Operateur* opOR= new Operateur(2,"OR");
    Operateur* opNOT= new Operateur(1,"NOT");
    Operateur* opDIV= new Operateur(2,"DIV");
    Operateur* opMOD= new Operateur(2,"MOD");
    Operateur* opNEG= new Operateur(1,"NEG");


    addItem(opSwap);

    addItem(opDUP);

    addItem(opDROP);

    addItem(opCLEAR);

    addItem(opIFT);

    addItem(opSTO);

    addItem(opFORGET);

    addItem(opEVAL);

    addItem(opAND);

    addItem(opOR);

    addItem(opNOT);

    addItem(opDIV);

    addItem(opMOD);

    addItem(opNEG);

    done = true;

    }
}


void ItemManager::STO(Item& item, Expression& exp)
{
    // On parcourt le tab pour savoir si l'exp est deja utilisée
    //si l'expression correspond a un id d'operateur deja utilise, erreur
   for (unsigned int i=0; i<nb; i++)
    {
        if(it[i]->GetId()== Op){
            if(dynamic_cast<Operateur*>(it[i])->getNom() == exp.getNom())
            throw ComputException("id utilise pour un operateur");}

    // si l'expression correspond a un id de var ou programme, ecrase la valeur de celle-ci
        if (it[i]->GetId()== At){
            if(dynamic_cast<Atome*>(it[i])->GetSs_Id()==Var && dynamic_cast<Atome*>(it[i])->getNom() == exp.getNom())
            {
            //on cree une nv var avec le même nom
            Variable* var= new Variable(exp.getNom(),item);
            //on erase la premiere du tableau
            it[i]=var;
            return;
            }
            if(dynamic_cast<Atome*>(it[i])->GetSs_Id()==Prog && dynamic_cast<Atome*>(it[i])->getNom() == exp.getNom())
                {
                //on cree une nv var avec le même nom
                Programme* prg= new Programme(exp.getNom(),dynamic_cast<Programme*>(it[i])->GetInstructions());
                //on erase la premiere du tableau
                it[i]=prg;
                return;
                }
        }
    }
    //si item est un numerique: creation variable qu'on ajoute a la fin du tab itemManager
    if (item.GetId() == Ent || item.GetId()==Frac ||item.GetId()==Re)
        {
            Variable* var= new Variable(exp.getNom(),item);
            cout<<"on met une var dans le tableau d'item"<<endl;
            addItem(var);
            return;
        }

    //si item est un prg, stock programme
    if (item.GetId()== At && dynamic_cast<Atome&>(item).GetSs_Id()==Prog)
    {
        Programme* prg= new Programme(exp.getNom(),dynamic_cast<Programme&>(item).GetInstructions());
        cout<<"programme dans le tab d'item"<<endl;
        addItem(prg);
        return;

    }
    cout<<"ouloulou pb"<<endl;
    throw ComputException("STO ne s'applique pas a ce type d'Item");
}



void ItemManager::FORGET(Atome& a)
{
    bool isFound = false;
    for (unsigned int i=0; i<nb; i++)
    {
            if(isFound) {
            // Décale
            it[i-1]=it[i];
        } else if(dynamic_cast<Atome*>(it[i])->getNom() == a.getNom()) {
            // Supprime
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Atome non trouve");
    }
    nb--;
}


void EVAL(ItemManager& ItMan, Expression& e)
{
    for (unsigned int i=0; i<ItMan.nb; i++)
    {
        if(dynamic_cast<Atome*>(ItMan.it[i])->getNom() == e.getNom())
        {
            if(dynamic_cast<Atome*>(ItMan.it[i])->GetSs_Id()==Var){
                cout<<"cas 1: l'expression est une variable"<<endl;

                Pile::donneInstancePile().PUSH(ItMan.it[i]);
                return;
            }
            else if (dynamic_cast<Atome*>(ItMan.it[i])->GetSs_Id()==Prog){
                cout<<"cas 2: l'expression est un programme"<<endl;
                //dynamic_cast<Programme*>(it[i])->executer();
                return;}
        }
    }

    cout<<"la litterale expression ne correspond ni a un atome ni a un prog, EVAL ne fait rien"<<endl;

}


//-----------------------------------------------------------------------------------------------------------------------

Pile& Pile::donneInstancePile()
{
    static Pile uniqueinstance;
    return uniqueinstance;
}



size_t Pile::nbAffiche=4;

void Pile::affiche() const {
    system("cls"); // system("cls") sous windows
    cout << message << endl;
    for(size_t i = nbAffiche; i>0; i--) {
        cout << i << " : ";
        if(i <= items.size()) //il y en a moins que nbAffiche, on affiche tout
        {
          cout<<items[items.size()-i]->GetId();
        }
        cout << endl;
    }
}

void Pile::PUSH(Item * it)
{
    items.push_back(it);
}

void Pile::DROP()
{
    if(estVide()) {
        throw ComputException("Pile vide");

    }
    items.pop_back();// le popback va supprimé le pointeur mais pas l'element pointé qui sera elemine avec le destructeur en fin de prg
}

Item* Pile::TOP() {
    if(estVide()) {
        throw ComputException("Pile vide");
    }
    //enum_id id_temp =items[items.size()-1]->GetId();
    //Item* it = new Item(id_temp);
    //it = items[items.size()-1];
    Item* top = items.back();
    return top;
}

void Pile::CLEAR()
{
    items.clear();// appel popback qui supprime tous les pointeurs mais paes les objets
}

void Pile::SWAP()
{
    if (items.size()<2)
    {
        throw ComputException("Pile trop petite");
    }

    swap(items[getTaille()-1],items[getTaille()-2]);

    /*it1 = TOP();
    DROP();
    it2 = TOP();
    DROP();
    PUSH(it1);
    PUSH(it2);*/

}

void Pile::DUP()
{
    if(estVide()) {
        throw ComputException("Pile vide");
    }
    Item* copie = new Item(*TOP());
    PUSH(copie);
    /*it = TOP();
    PUSH(it);*/
}


void Pile::IFT(){

//— L’opérateur binaire IFT dépile 2 arguments. Le 1er (i.e. le dernier dépilé) est un test logique.
//Si la valeur de ce test est vrai, le 2e argument est évalué sinon il est abandonné.
    Item* it1 = TOP();
    DROP();
    Item* it2 = TOP();
    DROP();
    if (it1->GetId() == Ent && it1->getValue()!=0 || it1->GetId() != Ent )
    {
            cout<<"test logique positif"<<endl;
            Entier* e1= new Entier(1);
            PUSH(e1);

          //evaluer arg2
          if(it2->GetId() == Ent ||it2->GetId() == Frac || it2->GetId() == Re)
            PUSH(it2);
          else if(it2->GetId() == At)
          {
              if(dynamic_cast<Atome*>(it2)->GetSs_Id() == Var)
              { cout<<"var"<<endl;
                PUSH(it2);}
              else if(dynamic_cast<Atome*>(it2)->GetSs_Id() == Prog)
                //dynamic_cast<Programme&>(it2).executer();
                cout<<"prog"<<endl;
          }
          else if(it2->GetId() == Exp)
            EVAL(ItemManager::donneInstance(), dynamic_cast<Expression&>(*it2));
    }

    else cout<<"test logique faux"<<endl;

}
//--------------------------------------------------------
//operateurs

void OpAND(){
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    Item* it2 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    int res1;
    int res2;
    if (it1->GetId() != Ent)
    res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (it2->GetId() != Ent)
        res2=1;
    else
    {
        if (it2->getValue() !=0)
            res2=1;
        else
            res2=0;
    }

    if (res1 == 1 && res2==1){
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);
    }


}

void OpOR(){
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    Item* it2 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    int res1;
    int res2;
    if (it1->GetId() != Ent)
    res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (it2->GetId() != Ent)
        res2=1;
    else
    {
        if (it2->getValue() !=0)
            res2=1;
        else
            res2=0;
    }
    if (res1 == 1 || res2==1 ){
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);

    }

}


void OpNON()
{
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    int res1;
    if (it1->GetId() != Ent)
    res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (res1 == 1){
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
    }
}
//-----------------------------------------------------------------------------------------------------------------------

/*double const Fraction::getValue()const {
    double num= numerateur.getValue();
    double den=denominateur.getValue();
    double res= num/den;
    return res;
    }
*/

/*
void Programme::executer()
{

    char firstcar;
    for (unsigned int i=1; i<instructions.size(); i++)
    {
        firstcar = instructions.at(i);
        if (firstcar != ']')
            controleur.commande(firstcar);
    }
}
*/
