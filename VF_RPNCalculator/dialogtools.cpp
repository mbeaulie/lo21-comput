#include <iostream>
#include "dialogtools.h"
#include "ui_dialogtools.h"

DialogTools::DialogTools(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DialogTools)
{
    ui->setupUi(this);
    ui->SpinNbAffiche->setValue(nbAffiche);
}

DialogTools::~DialogTools()
{
    delete ui;
}

void DialogTools::on_SpinNbAffiche_valueChanged(int arg1)
{
    nbAffiche=arg1;
    //std::cout<<"nbaffiche= "<<nbAffiche;
    fflush(stdout);
}

void DialogTools::on_buttonBox_accepted()
{
    emit OkPressed();
}

void DialogTools::setNbAffiche(int nb)
{
    nbAffiche=nb;
    ui->SpinNbAffiche->setValue(nb);
}
