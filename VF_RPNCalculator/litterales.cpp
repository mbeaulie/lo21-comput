#include "litterales.h"
#include "controller.h"

using namespace std;

void OpAND(){
    if (Pile::donneInstancePile().getTaille()<2)
    {
        Pile::donneInstancePile().setMessage("Pas assez d'elements dans la pile !");
        return;
    }
    std::cout<<"opAnd : debut fonction"<<std::endl; fflush(stdout);
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    Item* it2 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    std::cout<<"opAnd : debut des tests"<<std::endl; fflush(stdout);
    int res1;
    int res2;
    if (it1->GetId() != Ent)
    res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (it2->GetId() != Ent)
        res2=1;
    else
    {
        if (it2->getValue() !=0)
            res2=1;
        else
            res2=0;
    }
    std::cout<<"opAnd : res1="<<res1<<",res2="<<res2<<std::endl; fflush(stdout);
    if (res1 == 1 && res2==1){
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);
    }
}

void OpOR(){
    if (Pile::donneInstancePile().getTaille()<2)
    {
        Pile::donneInstancePile().setMessage("Pas assez d'elements dans la pile !");
        return;
    }
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    Item* it2 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    int res1;
    int res2;
    if (it1->GetId() != Ent)
        res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (it2->GetId() != Ent)
        res2=1;
    else
    {
        if (it2->getValue() !=0)
            res2=1;
        else
            res2=0;
    }
    if (res1 == 1 || res2==1 ){
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);
    }
}


void OpNON()
{
    if (Pile::donneInstancePile().getTaille()<1)
    {
        Pile::donneInstancePile().setMessage("Pas assez d'elements dans la pile !");
        return;
    }
    Item* it1 = Pile::donneInstancePile().TOP();
    Pile::donneInstancePile().DROP();
    int res1;
    if (it1->GetId() != Ent)
        res1=1;
    else
    {
        if (it1->getValue() !=0)
            res1=1;
        else
            res1=0;
    }
    if (res1 == 1){
        Entier* e= new Entier(0);
        Pile::donneInstancePile().PUSH(e);
        }
    else {
        Entier* e= new Entier(1);
        Pile::donneInstancePile().PUSH(e);
    }
}

//----------------------------------------------------------------------------------------------------------------------
//ItemManager

void ItemManager::agrandissementCapacite() {

    // Creation d'un nouveau tableau plus grand
    size_t newSize = (nbMax+1)*2;
    Item ** newtab = new Item*[newSize];
    // Copie du contenu de l'ancien tableau vers le nouveau
    for(size_t i=0; i<nb; i++) {
        newtab[i] = it[i];
    }
    //std::memcpy(newtab, exps, nb*sizeof(Expression*));
    delete[] it;
    it = newtab;
    nbMax = newSize;
}

Item& ItemManager::addItem(Item* item) {
// lors de l'appel de STO: on stocke une var ou un prog
// lors de la creation d'un operateur, on le stocke

    if(nb==nbMax) {
        agrandissementCapacite();
    }
    it[nb] = item;
    cout<<"additem"<<endl;
    cout<<it[nb]->GetId();
    return *it[nb++];
}

void ItemManager::removeItem(Item& v) {
    bool isFound = false;
    for(size_t i=0; i<nb; i++) {
        std::cout << "ItemManager::removeItem, item=" << v.GetId() << ", it[" << i << "]=" << it[i]->GetId() << std::endl;
        if(isFound) {
            // Decale
            it[i-1]=it[i];
        } else if(it[i]==&v) {
            // Supprime
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Item non trouve");
    }
    nb--;
}

void ItemManager::removeItemByName(string name, int id) {

    bool isFound = false;

    for(size_t i=0; i<nb; i++) {

        //cout << "ItemManager::removeItemByName() name=" << name << ",id=" << id
        //     << ",dynamic_cast<Atome*>(it[i])->getNom()=" << dynamic_cast<Atome*>(it[i])->getNom()
        //     << ",dynamic_cast<Atome*>(it[i])->GetssId()=" << dynamic_cast<Atome*>(it[i])->GetSs_Id() << endl ; fflush(stdout);

        if(isFound) {
            // Decale
         //   cout << "ItemManager::removeItemByName() on décale " << dynamic_cast<Atome*>(it[i])->getNom() << ",i=" << i << endl ; fflush(stdout);
            it[i-1]=it[i];

        } else if (dynamic_cast<Atome*>(it[i])->getNom() == name && dynamic_cast<Atome*>(it[i])->GetSs_Id() == id ) {
            // Supprime
         //   cout << "ItemManager::removeItemByName() on supprime " << name << ",i=" << i << endl ; fflush(stdout);
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Item non trouve");
    }
    nb--;
    // cout << "ItemManager::removeItemByName() nb=" << nb << endl ; fflush(stdout);
}


ItemManager::~ItemManager() {
    // Libere la memoire allouee a chaque objet
    for(size_t i=0; i<nb; i++) {
        delete it[i];
    }
    // Libere la memoire allouee au tableau de pointeurs
    delete[] it;
}

ItemManager::ItemManager(const ItemManager & v) : it(new Item*[v.nbMax]), nb(v.nb), nbMax(v.nbMax) {
    for(size_t i=0; i<nb; i++) { // apres avoir alloue de la memoire pour un nouveau tableau distinct, on cree un copie de l'expression et on l'enregistre dans le tableau cree
        it[i]= new Item(*v.it[i]);
    }

}

ItemManager & ItemManager::operator=(const ItemManager & v) {
    // Gere l'operation d'autoaffectation
    if(this==&v)
        return *this;

    // Creation d'un nouveau tableau
    Item ** newtab = new Item*[v.nbMax];
    for(size_t i=0; i<v.nb; i++) {
        newtab[i] = new Item(*v.it[i]);// ici on veut deux elements distincts donc on fait une duplication
    }
    // Suppression de l'ancien tableau
    for(size_t i =0; i<nb; i++) {
        delete it[i];
    }
    delete[] it;
    it = newtab;
    nb = v.nb;
    nbMax = v.nbMax;
    return *this;
}


ItemManager& ItemManager::donneInstance()
{
    static ItemManager uniqueinstance;
    return uniqueinstance;
}

void ItemManager::STO(Item& item, Expression& exp)
{
    // On parcourt le tab pour savoir si l'exp est deja utilisee
    //si l'expression correspond a un id d'operateur deja utilise, erreur
   for (unsigned int i=0; i<nb; i++)
    {
        if(it[i]->GetId()== Op){
            if(dynamic_cast<Operateur*>(it[i])->getNom() == exp.getNom())
            throw ComputException("id utilise pour un operateur");}

    // si l'expression correspond a un id de var ou programme, ecrase la valeur de celle-ci
        if (it[i]->GetId()== At){
            if(dynamic_cast<Atome*>(it[i])->GetSs_Id()==Var && dynamic_cast<Atome*>(it[i])->getNom() == exp.getNom())
            {
                //on cree une nv var avec le meme nom
                Variable* var= new Variable(exp.getNom(),item);
                //on erase la premiere du tableau
                it[i]=var;
                return;
            }
            if(dynamic_cast<Atome*>(it[i])->GetSs_Id()==Prog && dynamic_cast<Atome*>(it[i])->getNom() == exp.getNom())
            {
                //on cree un nv prg avec le meme nom
                //std::cout << "ItemManager::STO, on remplace it[" << i << "]" << std::endl; fflush (stdout);

                //std::cout << "ItemManager::STO, item=" << dynamic_cast<Programme&>(item).GetInstructions() << std::endl; fflush (stdout);

                Programme* prg= new Programme(exp.getNom(),dynamic_cast<Programme&>(item).GetInstructions());
                //on erase la premiere du tableau
                //std::cout << "ItemManager::STO, on a ecrase it[" << i << "] avec " << dynamic_cast<Programme&>(item).GetInstructions() << std::endl; fflush (stdout);
                it[i]=dynamic_cast<Item*>(prg);
                return;
             }
        }
    }
    //si item est un numerique: creation variable qu'on ajoute a la fin du tab itemManager
    if (item.GetId() == Ent || item.GetId()==Frac ||item.GetId()==Re)
        {
            Variable* var= new Variable(exp.getNom(),item);
            cout<<"on met une var dans le tableau d'item"<<endl;
            addItem(var);
            return;
        }

    //si item est un prg, stock programme
    if (item.GetId()== At && dynamic_cast<Atome&>(item).GetSs_Id()==Prog)
    {
        Programme* prg= new Programme(exp.getNom(),dynamic_cast<Programme&>(item).GetInstructions());
        cout<<"programme dans le tab d'item"<<endl;
        addItem(prg);
        return;

    }
    cout<<"ouloulou pb"<<endl;
    throw ComputException("STO ne s'applique pas a ce type d'Item");
}



void ItemManager::FORGET(Atome& a)
{
    bool isFound = false;
    for (unsigned int i=0; i<nb; i++)
    {
            if(isFound) {
            // Decale
            it[i-1]=it[i];
        } else if(dynamic_cast<Atome*>(it[i])->getNom() == a.getNom()) {
            // Supprime
            delete it[i];
            isFound = true;
        }
    }
    if(!isFound) {
        throw ComputException("Atome non trouve");
    }
    nb--;
}

void EVAL(ItemManager& ItMan, Expression& e)
{
    for (unsigned int i=0; i<ItMan.nb; i++)
    {
        if(dynamic_cast<Atome*>(ItMan.it[i])->getNom() == e.getNom())
        {
            if(dynamic_cast<Atome*>(ItMan.it[i])->GetSs_Id()==Var){
                cout<<"cas 1: l'expression est une variable"<<endl;
                //cout<<"EVAL : ItMan.it[i]->GetId() : "<<ItMan.it[i]->GetId()<<endl;
                Item* newItem=dynamic_cast<Variable*>(ItMan.it[i])->getItem();
                Pile::donneInstancePile().PUSH(newItem);
                return;
            }
            else if (dynamic_cast<Atome*>(ItMan.it[i])->GetSs_Id()==Prog){
                cout<<"cas 2: l'expression est un programme"<<endl;
                Pile& pile=Pile::donneInstancePile();
                Controller cnt(ItMan, pile);
                cnt.execute(dynamic_cast<Programme*>(ItMan.it[i])->GetInstructions());
                return;
            }
        }
    }

    cout<<"la litterale expression ne correspond ni a un atome ni a un prog, EVAL ne fait rien"<<endl;

}

void EVAL_it(ItemManager& ItMan, Expression& e)
{
    EVAL(ItMan, e);
}
