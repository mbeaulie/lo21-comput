#include "litnum.h"


/*---------------LitNumerique--------------*/
/**
 * @brief toString
 * @param
 * @return une chaine qui contient l'item mis en forme
 */
string LitNumerique::toString ()
{

    stringstream res;

    int id=this->GetId();

    // std::cout << "toString, id=" << id << std::endl ; fflush(stdout);

    switch ( id )
    {
        // Ent,Frac,Re
        case Ent:
        {
            Entier* e=dynamic_cast<Entier*>(this);
            res << e->getEnt();
            break;
        }
        case Frac:
        {
            Fraction* f=dynamic_cast<Fraction*>(this);
            if (f->getDeno().getEnt()==1) //fraction sur 1
            {
               res << f->getNum();
               break;
            }
            res << f->getNum() << "/" << f->getDeno();
            break;
        }
        case Re:
        {
            Reel* r=dynamic_cast<Reel*>(this);
            if (r->getEnt().getEnt()==0 && (r->getNeg()))
                res<<"-";
            res << r->getEnt() << "." ;
            for(int i =0; i < r->getNbZero(); i++)
               res << "0";
            res << r->getMant();
            //cout<<"toString() : "<<r->getEnt()<<endl;
            //cout<<"toString() : "<<r->getNeg()<<endl;
            break;
        }
        default:
        {
            res << "";
            break;
        }
    }

    return res.str();

}

/*---------------Entier------------------*/

ostream & operator<<(ostream &f, Entier &e){
    f<<e.getEnt();
    return f;
}

/*---------------Fraction------------------*/

void Fraction::setFraction(Entier n, Entier d){
    numerateur=n;
    Entier nul(0);
    if (d!=nul)
        denominateur=d;
    else
    {
        denominateur=1;
        throw"Erreur, denominateur nul.";
    }
    simplification();

}

void Fraction::simplification(){
    Entier nul(0);
    // si le numerateur est 0, le denominateur prend la valeur 1
    if(numerateur==nul)
    {
        denominateur=1;
        return;
    }
    /*un denominateur ne devrait pas etre 0;si c�est le cas, on sort de la methode*/
    if(denominateur==nul)
        return;
    /*utilisation de l�algorithme d�Euclide pour trouver le Plus Grand CommunDenominateur (PGCD) entre le numerateur et le denominateur*/
    Entier a=numerateur, b=denominateur;
    // on ne travaille qu�avec des valeurs positives...
    if(a<nul)
        a=numerateur.NEG();
    if(b<nul) b=denominateur.NEG();
    while(a!=b)
    {
        if(a > b)
            a=a.getValue()-b.getValue();
        else
            b=b.getValue()-a.getValue();
    }
    // on divise le numerateur et le denominateur par le PGCD=a
    numerateur=numerateur.DIV(a);
    denominateur=denominateur.DIV(a);// si le denominateur est negatif, on fait passer le signe - au denominateur
    if(denominateur<nul)
    {
    denominateur=denominateur.NEG();
    numerateur=numerateur.NEG();
    }
}

ostream & operator <<(ostream& f, Fraction& frac) {
    if (frac.getDeno().getEnt()!=1)
        f<<frac.getNum().getEnt()<<"/"<<frac.getDeno().getEnt();
    else
        f<<frac.getNum().getEnt();
    return f;
}



/*---------------------------------Reel--------------------------------------*/


/*
_______________________________________________________________________

Fonction de service qui renvoie la taille d'un int
Au dessus de 10, on est en overflow de l'int donc �a ne fonctionne plus
________________________________________________________________________
*/

int taille(int n) {
    int res = 0;
    if(n == 0) {
        return 1;
    }
    else if(n < 0)  { //si tu compte le moins dans la longueur
        ++res;
    }
    while(n != 0) {
        n /= 10.0;
        ++res;
    }
    return res;
}




/*
_________________________________________________________________________________________________

Si on utilise pow(int,int) avec une variable en deuxieme parametre, �a renvoie parfois des 99.
(avec des chiffres negatifs c'est bon par contre).
Donc implementation de fonction de service pour donner les puissances de 10.
Jusque'a 10 maximum, parce qu'au dessus on est en overflow des int.
__________________________________________________________________________________________________
*/
int puissance_dix(int n){
if (n>=10)
    cout << "Nombre trop grand";
else
    switch(n){
        case(1):
            return 10;
            break;
        case(2):
            return 100;
            break;
        case(3):
            return 1000;
            break;
        case(4):
            return 10000;
            break;
        case(5):
            return 100000;
            break;
        case(6):
            return 1000000;
            break;
        case(7):
            return 10000000;
            break;
        case(8):
            return 100000000;
            break;
        case(9):
            return 1000000000;
            break;
        default:
            return 0;
    }
        return 0;
}


const Reel& Reel::operator+(const LitNumerique& r) const{
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);
    int re1_t = taille(mant.getEnt()) + nb_zero;
    int re2_t = taille(pt->mant.getEnt()) + pt->nb_zero;
    int re1 = 0;
    int re2 = 0;
    int sizediff = re1_t - re2_t;
    int sizemax = re1_t;


    if(sizediff > 0){
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = (pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt()) * puissance_dix(sizediff);
    }
    else if(sizediff<0){
        sizediff = -sizediff;
        sizemax = re2_t;
        re1 = (ent.getEnt() * puissance_dix(re1_t) + mant.getEnt()) * puissance_dix(sizediff);
        re2 = pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt();
    }
    else{
        re1 = ent.getEnt() * puissance_dix(re1_t) + mant.getEnt();
        re2 = pt->ent.getEnt() * puissance_dix(re2_t) + pt->mant.getEnt();
    }

    if((ent.getEnt()==0) &&(getNeg()==true))
        re1 = - re1;
    if((pt->ent.getEnt()==0)&&(pt->getNeg()==true))
        re2 = - re2;

    int re3 = re1+re2;
    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizemax; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }
    int entfin = re3/puissance_dix(sizemax);
    int mantfin = re3%puissance_dix(sizemax);

    bool neg = false;

    if((entfin == 0)&& (re3 <0))
        neg =true;

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin, mantfin, zerofin, neg);
    return (*pt1);
}

const Reel& Reel::operator-(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+pt->nb_zero;
    int re1 = 0;
    int re2 = 0;
    int sizediff = re1_t - re2_t;
    int sizemax = re1_t;

    if(sizediff > 0){
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = (pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt()) * puissance_dix(sizediff);
    }
    else if(sizediff<0){
        sizediff = -sizediff;
        sizemax = re2_t;
        re1 = (ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt()) * puissance_dix(sizediff);
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    }
    else{
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    }

    if((ent.getEnt()==0) &&(getNeg()==true))
        re1 = - re1;
    if((pt->ent.getEnt()==0)&&(pt->getNeg()==true))
        re2 = - re2;

    int re3 = re1-re2;
    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizemax; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }


    int entfin = re3/puissance_dix(sizemax);
    int mantfin = re3%puissance_dix(sizemax);

    bool neg = false;

    if((entfin == 0) && (re3 <0))
        neg =true;

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin,zerofin, neg);
    return (*pt1);
}

const Reel& Reel::operator*(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+pt->nb_zero;
    int re1;
    int re2;

    if(ent.getEnt()>= 0)
        re1 = ent.getEnt() * puissance_dix(re1_t)+ mant.getEnt();
    else
        re1 = ent.getEnt() * puissance_dix(re1_t)- mant.getEnt();
    if(pt->ent.getEnt()>= 0)
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)+ pt->mant.getEnt();
    else
        re2 = pt->ent.getEnt() * puissance_dix(re2_t)- pt->mant.getEnt();

    if((ent.getEnt()==0) &&(getNeg()==true))
        re1 = - re1;
    if((pt->ent.getEnt()==0)&&(pt->getNeg()==true))
        re2 = - re2;

    int re3 = re1*re2;
    int sizetot = re1_t+re2_t;

    int temp = re3;
    int zerofin = 0;

    for(int i = 0; i<sizetot; i++){
        if(temp%10 == 0)
            zerofin ++;
        else
            zerofin = 0;
        temp = temp/10;
    }

    int entfin = re3/puissance_dix(sizetot);
    int mantfin = re3%puissance_dix(sizetot);
    bool neg = false;

    if((entfin == 0)&& (re3 <0))
        neg =true;

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin,zerofin, neg);
    return (*pt1);
}


const Reel& Reel::operator/(const LitNumerique& r) const {
    const Reel* pt = new Reel(dynamic_cast<const Reel&>(r).ent,dynamic_cast<const Reel&>(r).mant,dynamic_cast<const Reel&>(r).nb_zero,dynamic_cast<const Reel&>(r).negatif);

    int re1_t = taille(mant.getEnt())+nb_zero;
    int re2_t = taille(pt->mant.getEnt())+ pt->nb_zero;
    double re1;
    double re2;

    // on cast les deux reels en double pour pouvoir faire les calculs
    if(ent.getEnt() >= 0)
        re1 = ent.getEnt()+((double) mant.getEnt())*pow(10, -re1_t);
    else
        re1 = ent.getEnt()-((double) mant.getEnt())*pow(10, -re1_t);

    if(pt->ent.getEnt() >= 0)
        re2 = pt->ent.getEnt()+((double) pt->mant.getEnt())*pow(10, -re2_t);
    else
        re2 = pt->ent.getEnt()-((double) pt->mant.getEnt())*pow(10, -re2_t);

    if((ent.getEnt()==0) &&(getNeg()==true))
        re1 = - re1;
    if((pt->ent.getEnt()==0)&&(pt->getNeg()==true))
        re2 = - re2;

    double re3 = re1/re2;

    //On definit la taille de la mantisse
    int sizetot=4;
    if(taille(mant.getEnt()) > sizetot)
        sizetot = taille(mant.getEnt());
    if(taille(pt->mant.getEnt())>sizetot)
        sizetot = taille(pt->mant.getEnt());

    double d_temp = re3;
    int zerofin = 0;
    int p_ent = ((int) floor(d_temp));
    bool neg=false;

    //si la partie entiere est nulle
    if(p_ent==0){
        if(re3 < 0)
            neg = true;
        zerofin = -1;
        while(p_ent == 0){
            zerofin ++;
            d_temp = d_temp*10;
            p_ent = ((int) floor(d_temp));

        }
        re3 = re3 * puissance_dix(sizetot);
    }
    //si la partie entiere n'est pas nulle
    else {
        re3 = re3 * puissance_dix(sizetot);
        int re = ((int) re3);
        int i_temp = re;

        for(int i = 0; i<sizetot; i++){
            if(i_temp%10 == 0)
                zerofin ++;
            else
                zerofin = 0;
            i_temp = i_temp/10;
        }
    }

    int entfin = re3/puissance_dix(sizetot);
    int mantfin = ((int) re3)%puissance_dix(sizetot);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel* pt1 =new  Reel(entfin,mantfin, zerofin, neg);
    return (*pt1);

}





//Une litterale reelle dont la matisse est nulle est simplifiee en une litterale entiere
//Fonction uniquement appelee par le constructeur quand la mantisse est nulle
Entier Reel::simplification()
{
    Entier e(ent);
    return e;
}

ostream & operator<<(ostream& f, const Reel& r){

    if (r.getMant().getEnt()!=0){
        Entier nul(0);
          if((r.getEnt()==nul)&&(r.getNeg()))
              f<<"-";
          f<<r.getEnt().getEnt()<<".";
          for(int i=0; i<r.getNbZero();i++)
              cout <<"0";
          cout <<r.getMant().getEnt();
      }
    else
        f<<r.getEnt().getEnt();
    return f;
}


/*------------------------------------------------------

Fonction de service : Cast de Fraction vers Reel.
Prends une fraction en parametre et retourne un reel.

-----------------------------------------------------------*/

Reel r_cast(const Fraction& f){
    double re = ((double) f.getNum().getEnt())/f.getDeno().getEnt();

    int t_mant = 4; // on met une mantisse a 4 chiffres

    double d_temp = re;
    int zerofin = 0;
    int p_ent = ((int) floor(d_temp));
    bool neg = false;

    if((re<0)&&(p_ent == -1))
            neg = true;
    //si la partie entiere est nulle
    if(p_ent==0){
        zerofin = -1;
        while(p_ent == 0){
            zerofin ++;
            d_temp = d_temp*10;
            p_ent = ((int) floor(d_temp));

        }
        re = re * puissance_dix(t_mant);
    }
    //si la partie entiere n'est pas nulle
    else {
        re = re * puissance_dix(t_mant);
        int re_temp = ((int) re);
        int i_temp = re_temp;

        for(int i = 0; i<t_mant; i++){
            if(i_temp%10 == 0)
                zerofin ++;
            else
                zerofin = 0;
            i_temp = i_temp/10;
        }
    }

    int entfin = re/puissance_dix(t_mant);
    int mantfin = ((int) re)%puissance_dix(t_mant);

    if(mantfin < 0)
        mantfin = - mantfin;

    Reel re1(entfin,mantfin,zerofin,neg);
    return re1;
}

/*----------------Surcharge Operateurs-----------------------------*/

const Fraction operator/(const Entier& n, const Entier& d){
    Fraction f(n, d);
    return f;
}

/*---------------------Operateur+----------------------------------*/

const Fraction operator+(const Entier& e, const Fraction& f){
    Fraction s(e  * f.getDeno() + f.getNum(), f.getDeno());
    return s;
}

const Reel operator+(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp + r;
    return s;
}

const Reel operator+(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = r + re;
    return somme;
}

const Fraction operator+(const Fraction& f, const Entier& e){return e+f;}
const Reel operator+(const Reel& r, const Entier& e){return e+r;}
const Reel operator+(const Reel& r, const Fraction& f){return f+r;}

/*---------------------Operateur(-)----------------------------------*/


const Fraction operator-(const Entier& e, const Fraction& f){
    Fraction s(e*f.getDeno()-f.getNum(), f.getDeno());
    return s;
}

const Fraction operator-(const Fraction& f, const Entier& e){
    Fraction s(f.getNum()-e*f.getDeno(), f.getDeno());
    return s;
}

const Reel operator-(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp - r;
    return s;
}

const Reel operator-(const Reel& r, const Entier& e){
    Reel r_temp(e);
    Reel s = r - r_temp;
    return s;
}

const Reel operator-(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel sous = re - r;
    return sous;
}

const Reel operator-(const Reel& r, const Fraction& f){
    Reel re = r_cast(f);
    Reel sous = r - re;
    return sous;
}
//--------------------------Operateur*---------------------------

const Fraction operator*(const Entier& e, const Fraction& f){
    Fraction s(e*f.getNum(), f.getDeno());
    return s;
}

const Reel operator*(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp * r;
    return s;
}

const Reel operator*(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = re * r;
    return somme;
}

const Fraction operator*(const Fraction& f, const Entier& e){return e*f;}
const Reel operator*(const Reel& r, const Entier& e){return e*r;}
const Reel operator*(const Reel& r, const Fraction& f){return f*r;}



//----------------------------Operateur/--------------------------------

const Fraction operator/(const Entier& e, const Fraction& f){
    Fraction f_temp(f.getDeno(), f.getNum());
    Fraction s = e*f_temp;
    return s;
}

const Fraction operator/(const Fraction& f, const Entier& e){
    Entier e_temp(1);
    Fraction f_temp(e_temp, e);
    Fraction s = f*f_temp;
    return s;
}

const Reel operator/(const Entier& e, const Reel& r){
    Reel r_temp(e);
    Reel s = r_temp / r;
    return s;
}

const Reel operator/(const Reel& r, const Entier& e){
    Reel r_temp(e);
    Reel s = r / r_temp;
    return s;
}

const Reel operator/(const Fraction& f, const Reel& r){
    Reel re = r_cast(f);
    Reel somme = re / r;
    return somme;
}

const Reel operator/(const Reel& r, const Fraction& f){
    Reel re = r_cast(f);
    Reel somme = r / re;
    return somme;
}

/*-----------------------------Operateurs logiques-------------------------------------*/

/*---------------------------------Operateur<------------------------------------------*/

bool operator<(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp<f;
}

bool operator<(const Fraction& f, const Entier& e){
    Fraction f_temp(e, 1);
    return f<f_temp;
}

bool operator<(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp<r;
}

bool operator<(const Reel& r, const Entier& e){
    Reel r_temp(e);
    return r<r_temp;
}

bool operator<(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp <r;
}

bool operator<(const Reel& r, const Fraction& f){
    Reel r_temp = r_cast(f);
    return r<r_temp;
}

/*---------------------------------Operateur>------------------------------------------*/

bool operator>(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp>f;
}

bool operator>(const Fraction& f, const Entier& e){
    Fraction f_temp(e, 1);
    return f>f_temp;
}

bool operator>(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp>r;
}

bool operator>(const Reel& r, const Entier& e){
    Reel r_temp(e);
    return r>r_temp;
}

bool operator>(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp >r;
}

bool operator>(const Reel& r, const Fraction& f){
    Reel r_temp = r_cast(f);
    return r>r_temp;
}

/*---------------------------------Operateur==------------------------------------------*/

bool operator==(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp==f;
}

bool operator==(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp==r;
}

bool operator==(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp==r;
}

bool operator==(const Fraction& f, const Entier& e){return e==f;}
bool operator==(const Reel& r, const Entier& e){return e==r;}
bool operator==(const Reel& r, const Fraction&f){return f==r;}

/*---------------------------------Operateur!=------------------------------------------*/

bool operator!=(const Entier& e, const Fraction& f){
    Fraction f_temp(e, 1);
    return f_temp!=f;
}

bool operator!=(const Entier& e, const Reel& r){
    Reel r_temp(e);
    return r_temp!=r;
}

bool operator!=(const Fraction& f, const Reel& r){
    Reel r_temp = r_cast(f);
    return r_temp!=r;
}

bool operator!=(const Fraction& f, const Entier& e){return e!=f;}
bool operator!=(const Reel& r, const Entier& e){return e!=r;}
bool operator!=(const Reel& r, const Fraction& f){return f!=r;}

/*---------------------------------Operateur<=------------------------------------------*/

bool operator<=(const Entier& e, const Fraction& f){return !(e>f);}
bool operator<=(const Fraction& f, const Entier& e){return !(f>e);}
bool operator<=(const Entier& e, const Reel& r){return !(e>r);}
bool operator<=(const Reel& r, const Entier& e){return !(r>e);}
bool operator<=(const Fraction& f, const Reel& r){return !(f>r);}
bool operator<=(const Reel& r, const Fraction& f){return !(r>f);}

/*---------------------------------Operateur>=------------------------------------------*/

bool operator>=(const Entier& e, const Fraction& f){return !(e<f);}
bool operator>=(const Fraction& f, const Entier& e){return !(f<e);}
bool operator>=(const Entier& e, const Reel& r){return !(e<r);}
bool operator>=(const Reel& r, const Entier& e){return !(r<e);}
bool operator>=(const Fraction& f, const Reel& r){return !(f<r);}
bool operator>=(const Reel& r, const Fraction& f){return !(r<f);}
